import { expect, it } from '@jest/globals';
import { AttachmentsService, createTransformer } from '../../src/services/atttachments';
import { AxiosHeaders } from 'axios';
import mockAxios from 'jest-mock-axios';
import FormData from '../../__mocks__/form-data';
import { ReadStream } from 'fs';
import { Client } from '../../src';
import OpenAPIClientAxios from 'openapi-client-axios';
import { getDefinition } from '../../src/container';

jest.mock('form-data');

const content = 'content';

describe('Service Attachments', () => {
  it('Transformer', async () => {
    const transformer = createTransformer('test.txt');
    const headers = new AxiosHeaders();
    const formData = transformer({ file: __filename, minorEdit: 'false', comment: 'content' }, headers) as unknown as FormData;
    expect(formData.data).toHaveProperty('file');
    expect(formData.data.file.value).toBeInstanceOf(ReadStream)
    expect(formData.data.file.options?.filename).toEqual('test.txt');
    expect(formData.data).toHaveProperty('minorEdit');
    expect(formData.data.minorEdit.value).toEqual('false');
    expect(formData.data).toHaveProperty('comment');
    expect(formData.data.comment.value).toEqual('content');
    expect(headers.get('X-Atlassian-Token')).toEqual('nocheck');
  });
  it('uploadAttachment', async () => {
    const api = new OpenAPIClientAxios({ definition: await getDefinition() });
    const client = await api.init<Client>();
    const service = new AttachmentsService(client);
    const promise = service.uploadAttachment('pageId', __filename, 'test.txt');
    expect(mockAxios.request).toHaveBeenCalled();
    const response = { data: content };
    mockAxios.mockResponse(response);
    const data = await promise;
    expect(data).toEqual(content);
  });
});
