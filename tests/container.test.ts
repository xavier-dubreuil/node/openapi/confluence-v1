import { expect, describe, it } from '@jest/globals';
import mockAxios from 'jest-mock-axios';
import { Container } from '../src';
import { paramsSerializer } from '../src/container';

const content = 'content';
const axiosRequest = {
  method: 'get',
  url: '/wiki/rest/api/settings/systemInfo',
  data: undefined,
  headers: {},
  params: {}
};

describe('OpenAPI', () => {
  it('getSystemInfo', async () => {
    const container = await Container.new('http://localhost', 'username', 'password');
    const promise = container.client.getSystemInfo();
    expect(mockAxios.request).toHaveBeenCalledWith(axiosRequest);
    const response = { data: content };
    mockAxios.mockResponse(response);
    const { data } = await promise;
    expect(data).toEqual(content);
  });
  it('ParamSerializer', async () => {
    const check = paramsSerializer({ key: ['val1', 'val2'] });
    expect(check).toEqual('key=val1%2Cval2');
  });
});
