import mockAxios from 'jest-mock-axios';
export default mockAxios;

export class AxiosHeaders {
  data: Record<string, string | null> = {};

  set(headerName: string, value?: string | null): void {
    this.data[headerName] = value ?? null;
  }

  get(headerName: string): string | null {
    return this.data[headerName] ?? null;
  }

  has(header: string): boolean {
    return this.data[header] !== undefined;
  }
}
