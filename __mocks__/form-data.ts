import stream from 'stream';
import { ReadStream } from 'fs';

export = FormData;

type FormDataValue = string | number | ReadStream;

class FormData extends stream.Readable {
  public data: Record<string, { value: FormDataValue, options: FormDataAppendOptions | null }> = {}

  append (key: string, value: FormDataValue, options?: FormDataAppendOptions): void {
    this.data[key] = { value, options: options ?? null };
  }
}

interface FormDataAppendOptions {
  header?: string | Headers;
  knownLength?: number;
  filename?: string;
  filepath?: string;
  contentType?: string;
}
