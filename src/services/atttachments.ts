import { ClientService } from '@openapi/container';
import { Client, Components, Paths } from '../openapi.d';
import { AxiosRequestHeaders } from 'axios';
import { createReadStream } from 'fs';
import FormData from 'form-data';

type AttachmentTransformer = (data: Paths.CreateAttachment.RequestBody, headers: AxiosRequestHeaders) => FormData;

export function createTransformer (filename: string): AttachmentTransformer {
  return (data, headers) => {
    const formData = new FormData();
    formData.append('file', createReadStream(data.file), {
      filename
    });
    formData.append('minorEdit', data.minorEdit);
    if (data.comment !== undefined) {
      formData.append('comment', data.comment);
    }
    headers.set('X-Atlassian-Token', 'nocheck');
    return formData;
  };
}

export class AttachmentsService extends ClientService<Client> {
  public async uploadAttachment (page: string, path: string, filename: string): Promise<Components.Schemas.ContentArray> {
    const { data: response } = await this.client.createAttachment({ id: page }, {
      file: path,
      minorEdit: 'true',
    }, {
      transformRequest: createTransformer(filename)
    });
    return response
  }
}
