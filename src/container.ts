import { AxiosRequestConfig, CustomParamsSerializer } from 'axios';
import { OpenAPIV3 } from 'openapi-client-axios';
import { ClientServices, Container as OpenAPIContainer } from '@openapi/container';
import { AttachmentsService } from './services';
import { Client } from './openapi';
import { dirname } from 'path';
import Qs from 'qs';

interface Services extends ClientServices<Client> {
  attachments: AttachmentsService
}

export const paramsSerializer: CustomParamsSerializer = (params) => Qs.stringify(params, { arrayFormat: 'comma' });

export const openapiPath = `${dirname(__dirname)}/assets/openapi.json`;

export async function getDefinition(): Promise<OpenAPIV3.Document> {
  return Container.readDefinition(openapiPath);
}

export class Container extends OpenAPIContainer<Client, Services> {
  public static async new (domain: string, username: string, password: string): Promise<Container> {
    return await super.instantiate<Container, Client, Services>(
      Container,
      await getDefinition(),
      {
        baseURL: `${domain}`,
        auth: { username, password },
        paramsSerializer,
      } as AxiosRequestConfig,
      {
        attachments: AttachmentsService
      }
    );
  }
}
