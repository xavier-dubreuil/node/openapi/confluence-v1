import type {
  OpenAPIClient,
  Parameters,
  UnknownParamsObject,
  OperationResponse,
  AxiosRequestConfig,
} from 'openapi-client-axios';

export declare namespace Components {
    namespace Parameters {
        export type BodyConversionExpand = string[];
        export type ContentExpand = string[];
        export type ContentExpandWithSubExpandLimit = string[];
        export type SpaceExpand = ("settings" | "metadata" | "metadata.labels" | "operations" | "lookAndFeel" | "permissions" | "icon" | "description" | "description.plain" | "description.view" | "theme" | "homepage" | "history")[];
        export type UserLookupAccountId = string;
        export type UserLookupKey = string;
        export type UserLookupUsername = string;
    }
    export interface QueryParameters {
        contentExpand?: Parameters.ContentExpand;
        bodyConversionExpand?: Parameters.BodyConversionExpand;
        contentExpandWithSubExpandLimit?: Parameters.ContentExpandWithSubExpandLimit;
        spaceExpand?: Parameters.SpaceExpand;
        userLookupKey?: Parameters.UserLookupKey;
        userLookupUsername?: Parameters.UserLookupUsername;
        userLookupAccountId?: Parameters.UserLookupAccountId;
    }
    namespace Schemas {
        export interface AccountId {
            accountId: string;
        }
        export interface AccountIdEmailRecord {
            accountId: string;
            email: string;
        }
        export type AccountIdEmailRecordArray = AccountIdEmailRecord[];
        export interface AddContentRestriction {
            /**
             * The restriction operation applied to content.
             */
            operation: "read" | "update";
            /**
             * The users/groups that the restrictions will be applied to. At least one of
             * `user` or `group` must be specified for this object.
             */
            restrictions: {
                /**
                 * The users that the restrictions will be applied to. This array must
                 * have at least one item, otherwise it should be omitted.
                 */
                user?: {
                    /**
                     * Set to 'known'.
                     */
                    type: "known" | "unknown" | "anonymous" | "user";
                    username?: /**
                     * This property is no longer available and will be removed from the documentation soon.
                     * Use `accountId` instead.
                     * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
                     */
                    GenericUserName;
                    userKey?: /**
                     * This property is no longer available and will be removed from the documentation soon.
                     * Use `accountId` instead.
                     * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
                     */
                    GenericUserKey;
                    accountId: /**
                     * The account ID of the user, which uniquely identifies the user across all Atlassian products.
                     * For example, `384093:32b4d9w0-f6a5-3535-11a3-9c8c88d10192`.
                     */
                    GenericAccountId;
                }[];
                /**
                 * The groups that the restrictions will be applied to. This array must
                 * have at least one item, otherwise it should be omitted.
                 */
                group?: {
                    /**
                     * Set to 'group'.
                     */
                    type: "group";
                    /**
                     * The name of the group.
                     */
                    name: string;
                }[];
            };
        }
        export type AddContentRestrictionUpdateArray = AddContentRestriction[];
        export interface AffectedObject {
            name: string;
            objectType: string;
        }
        export interface AsyncContentBody {
            value?: string;
            representation?: "view" | "export_view" | "styled_view" | "storage" | "editor" | "editor2" | "anonymous_export_view" | "wiki" | "atlas_doc_format";
            renderTaskId?: string;
            error?: string;
            /**
             * Rerunning is reserved for when the job is working, but there is a previous run's value in the cache. You may choose to continue polling, or use the cached value.
             */
            status?: "WORKING" | "QUEUED" | "FAILED" | "COMPLETED" | "RERUNNING";
            embeddedContent?: EmbeddedContent[];
            webresource?: WebResourceDependencies;
            mediaToken?: {
                collectionIds?: string[];
                contentId?: string;
                expiryDateTime?: string;
                fileIds?: string[];
                token?: string;
            };
            _expandable?: {
                content?: string;
                embeddedContent?: string;
                webresource?: string;
                mediaToken?: string;
            };
            _links?: GenericLinks;
        }
        export interface AsyncId {
            asyncId: string;
        }
        export interface AttachmentPropertiesUpdateBody {
            [name: string]: any;
            id: string;
            /**
             * Set this to "attachment"
             */
            type: string;
            status?: string;
            title?: string;
            container?: /**
             * Container for content. This can be either a space (containing a page or blogpost)
             * or a page/blog post (containing an attachment or comment)
             */
            Container;
            metadata?: {
                mediaType?: string;
            };
            extensions?: {
                [key: string]: any;
            };
            version: Version;
        }
        export interface AttachmentUpdate {
            /**
             * The attachment version. Set this to the current version number of the
             * attachment. Note, the version number only needs to be incremented when
             * updating the actual attachment, not its properties.
             */
            version: {
                /**
                 * The version number.
                 */
                number: number; // int32
            };
            /**
             * The ID of the attachment to be updated.
             */
            id: string;
            /**
             * Set this to `attachment`.
             */
            type: "attachment";
            /**
             * The updated name of the attachment.
             */
            title?: string;
            metadata?: {
                /**
                 * The media type of the attachment, e.g. 'img/jpg'.
                 */
                mediaType?: string;
                /**
                 * The comment for this update.
                 */
                comment?: string;
            };
            /**
             * The new content to attach the attachment to.
             */
            container?: {
                /**
                 * The `id` of the parent content.
                 */
                id: string;
                /**
                 * The content type. You can only attach attachments to content
                 * of type: `page`, `blogpost`.
                 */
                type: string;
            };
        }
        export interface AuditRecord {
            author: {
                type: "user";
                displayName: string;
                operations: {
                    [key: string]: any;
                } | null;
                username?: /**
                 * This property is no longer available and will be removed from the documentation soon.
                 * Use `accountId` instead.
                 * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
                 */
                GenericUserName;
                userKey?: /**
                 * This property is no longer available and will be removed from the documentation soon.
                 * Use `accountId` instead.
                 * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
                 */
                GenericUserKey;
                accountId?: /**
                 * The account ID of the user, which uniquely identifies the user across all Atlassian products.
                 * For example, `384093:32b4d9w0-f6a5-3535-11a3-9c8c88d10192`.
                 */
                GenericAccountId;
                accountType?: string;
                externalCollaborator?: boolean;
                /**
                 * Whether the user is an external collaborator user
                 */
                isExternalCollaborator?: boolean;
                /**
                 * The public name or nickname of the user. Will always contain a value.
                 */
                publicName?: string;
            };
            remoteAddress: string;
            /**
             * The creation date-time of the audit record, as a timestamp.
             */
            creationDate: number; // int64
            summary: string;
            description: string;
            category: string;
            sysAdmin: boolean;
            superAdmin?: boolean;
            affectedObject: AffectedObject;
            changedValues: ChangedValue[];
            associatedObjects: AffectedObject[];
        }
        export interface AuditRecordArray {
            results: AuditRecord[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        export interface AuditRecordCreate {
            /**
             * The user that actioned the event. If `author` is not specified, then all
             * `author` properties will be set to null/empty, except for `type` which
             * will be set to 'user'.
             */
            author?: {
                /**
                 * Set to 'user'.
                 */
                type: "user";
                /**
                 * The name that is displayed on the audit log in the Confluence UI.
                 */
                displayName?: string;
                /**
                 * Always defaults to null.
                 */
                operations?: {
                    [key: string]: any;
                };
                username?: /**
                 * This property is no longer available and will be removed from the documentation soon.
                 * Use `accountId` instead.
                 * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
                 */
                GenericUserName;
                userKey?: /**
                 * This property is no longer available and will be removed from the documentation soon.
                 * Use `accountId` instead.
                 * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
                 */
                GenericUserKey;
            };
            /**
             * The IP address of the computer where the event was initiated from.
             */
            remoteAddress: string;
            /**
             * The creation date-time of the audit record, as a timestamp. This is converted
             * to a date-time display in the Confluence UI. If the `creationDate` is not
             * specified, then it will be set to the timestamp for the current date-time.
             */
            creationDate?: number; // int64
            /**
             * The summary of the event, which is displayed in the 'Change' column on
             * the audit log in the Confluence UI.
             */
            summary?: string;
            /**
             * A long description of the event, which is displayed in the 'Description'
             * field on the audit log in the Confluence UI.
             */
            description?: string;
            /**
             * The category of the event, which is displayed in the 'Event type' column
             * on the audit log in the Confluence UI.
             */
            category?: string;
            /**
             * Indicates whether the event was actioned by a system administrator.
             */
            sysAdmin?: boolean;
            affectedObject?: AffectedObject;
            /**
             * The values that were changed in the event.
             */
            changedValues?: ChangedValue[];
            /**
             * Objects that were associated with the event. For example, if the event
             * was a space permission change then the associated object would be the
             * space.
             */
            associatedObjects?: AffectedObject[];
        }
        export interface AvailableContentStates {
            /**
             * Space suggested content states that can be used in the space. This can be null if space content states are disabled in the space.
             * This list can be empty if there are no space content states defined in the space.
             * All spaces start with 3 default space content states, and this can be modified in the UI under space settings.
             */
            spaceContentStates: ContentState[];
            /**
             * Custom content states that can be used by the user on the content of this call.
             * This can be null if custom content states are disabled in the space of the content.
             * This list can be empty if there are no custom content states defined by the user.
             * This will at most have 3 of the most recently published content states.
             * Only the calling user has access to place these states on content, but all users can see these states once they are placed.
             */
            customContentStates: ContentState[];
        }
        export interface BlueprintTemplate {
            templateId: string;
            originalTemplate: {
                pluginKey: string;
                moduleKey: string;
            };
            referencingBlueprint: string;
            name: string;
            description: string;
            space?: {
                [name: string]: any;
            };
            labels: Label[];
            templateType: string;
            editorVersion?: string;
            body?: /**
             * The body of the new content. Does not apply to attachments.
             * Only one body format should be specified as the property for
             * this object, e.g. `storage`.
             *
             * Note, `editor2` format is used by Atlassian only. `anonymous_export_view` is
             * the same as `export_view` format but only content viewable by an anonymous
             * user is included.
             */
            ContentTemplateBody;
            _expandable?: {
                body?: string;
            };
            _links: GenericLinks;
        }
        export interface BlueprintTemplateArray {
            results: BlueprintTemplate[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        export interface Breadcrumb {
            label: string;
            url: string;
            separator: string;
        }
        export interface BulkContentStateSetInput {
            /**
             * maximum number of ids you can pass in is 300
             */
            ids: ContentId[];
            contentState: ContentStateInput;
        }
        export interface BulkUserLookup {
            type: "known" | "unknown" | "anonymous" | "user";
            username?: /**
             * This property is no longer available and will be removed from the documentation soon.
             * Use `accountId` instead.
             * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
             */
            GenericUserName;
            userKey?: /**
             * This property is no longer available and will be removed from the documentation soon.
             * Use `accountId` instead.
             * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
             */
            GenericUserKey;
            accountId: /**
             * The account ID of the user, which uniquely identifies the user across all Atlassian products.
             * For example, `384093:32b4d9w0-f6a5-3535-11a3-9c8c88d10192`.
             */
            GenericAccountId;
            /**
             * The account type of the user, may return empty string if unavailable.
             */
            accountType: string;
            /**
             * The email address of the user. Depending on the user's privacy setting, this may return an empty string.
             */
            email: string;
            /**
             * The public name or nickname of the user. Will always contain a value.
             */
            publicName: string;
            profilePicture: /* This object represents an icon. If used as a profilePicture, this may be returned as null, depending on the user's privacy setting. */ Icon;
            /**
             * The displays name of the user. Depending on the user's privacy setting, this may be the same as publicName.
             */
            displayName: string;
            /**
             * This displays user time zone. Depending on the user's privacy setting, this may return null.
             */
            timeZone?: string | null;
            /**
             * Whether the user is an external collaborator user
             */
            isExternalCollaborator?: boolean;
            operations?: /* An operation and the target entity that it applies to, e.g. create page. */ OperationCheckResult[];
            details?: UserDetails;
            personalSpace?: Space;
            _expandable: {
                operations?: string;
                details?: string;
                personalSpace?: string;
            };
            _links: GenericLinks;
        }
        export interface BulkUserLookupArray {
            results: BulkUserLookup[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        export type ButtonLookAndFeel = {
            backgroundColor: string;
            color: string;
        } | null;
        /**
         * The converted CQL queries.
         */
        export interface CQLPersonalDataConvertedQueries {
            /**
             * The list of converted CQL query strings with account IDs in
             * place of user identifiers.
             */
            queryStrings: string[];
        }
        /**
         * The CQL queries to be converted.
         */
        export interface CQLPersonalDataMigrationRequest {
            /**
             * A list of queries with user identifiers. Maximum of 100 queries.
             * example:
             * [
             *   "type = page and creator != admin and space = DEV"
             * ]
             */
            queryStrings: string[];
        }
        export interface ChangedValue {
            name: string;
            oldValue: string;
            hiddenOldValue?: string;
            newValue: string;
            hiddenNewValue?: string;
        }
        /**
         * A [Connect module](https://developer.atlassian.com/cloud/confluence/modules/admin-page/) in the same format as in the
         * [app descriptor](https://developer.atlassian.com/cloud/confluence/app-descriptor/).
         * example:
         * {
         *   "key": "my-webitem",
         *   "location": "system.admin/globalsettings",
         *   "url": "/my-web-item",
         *   "name": {
         *     "value": "My Web Item"
         *   }
         * }
         */
        export interface ConnectModule {
        }
        /**
         * example:
         * {
         *   "webItems": [
         *     {
         *       "key": "my-webitem",
         *       "location": "system.admin/globalsettings",
         *       "url": "/my-web-item",
         *       "name": {
         *         "value": "My Web Item"
         *       }
         *     }
         *   ],
         *   "adminPages": [
         *     {
         *       "key": "my-admin-page",
         *       "name": {
         *         "value": "My Admin Page"
         *       },
         *       "url": "/my-admin-page"
         *     }
         *   ]
         * }
         */
        export interface ConnectModules {
            /**
             * A list of app modules in the same format as the `modules` property in the
             * [app descriptor](https://developer.atlassian.com/cloud/confluence/app-descriptor/).
             */
            modules: /**
             * A [Connect module](https://developer.atlassian.com/cloud/confluence/modules/admin-page/) in the same format as in the
             * [app descriptor](https://developer.atlassian.com/cloud/confluence/app-descriptor/).
             * example:
             * {
             *   "key": "my-webitem",
             *   "location": "system.admin/globalsettings",
             *   "url": "/my-web-item",
             *   "name": {
             *     "value": "My Web Item"
             *   }
             * }
             */
            ConnectModule[];
        }
        /**
         * Container for content. This can be either a space (containing a page or blogpost)
         * or a page/blog post (containing an attachment or comment)
         */
        export type Container = {
            [name: string]: any;
        } | null;
        export type ContainerLookAndFeel = {
            background: string;
            backgroundAttachment?: string | null;
            backgroundBlendMode?: string | null;
            backgroundClip?: string | null;
            backgroundColor: string | null;
            backgroundImage: string | null;
            backgroundOrigin?: string | null;
            backgroundPosition?: string | null;
            backgroundRepeat?: string | null;
            backgroundSize: string | null;
            padding: string;
            borderRadius: string;
        } | null;
        export interface ContainerSummary {
            title: string;
            displayUrl: string;
        }
        /**
         * Base object for all content types.
         */
        export type Content = {
            [name: string]: any;
            id?: string;
            /**
             * Can be "page", "blogpost", "attachment" or "content"
             */
            type: string;
            status: string;
            title?: string;
            space?: Space;
            history?: ContentHistory;
            version?: Version;
            ancestors?: /* Base object for all content types. */ Content[] | null;
            operations?: /* An operation and the target entity that it applies to, e.g. create page. */ OperationCheckResult[];
            children?: ContentChildren;
            childTypes?: /**
             * Shows whether a piece of content has attachments, comments, or child pages/whiteboards.
             * Note, this doesn't actually contain the child objects.
             */
            ContentChildType;
            descendants?: ContentChildren;
            container?: /**
             * Container for content. This can be either a space (containing a page or blogpost)
             * or a page/blog post (containing an attachment or comment)
             */
            Container;
            body?: {
                view?: ContentBody;
                export_view?: ContentBody;
                styled_view?: ContentBody;
                storage?: ContentBody;
                wiki?: ContentBody;
                editor?: ContentBody;
                editor2?: ContentBody;
                anonymous_export_view?: ContentBody;
                atlas_doc_format?: ContentBody;
                dynamic?: ContentBody;
                raw?: ContentBody;
                _expandable?: {
                    editor?: string;
                    view?: string;
                    export_view?: string;
                    styled_view?: string;
                    storage?: string;
                    editor2?: string;
                    anonymous_export_view?: string;
                    atlas_doc_format?: string;
                    wiki?: string;
                    dynamic?: string;
                    raw?: string;
                };
            };
            restrictions?: {
                read?: ContentRestriction;
                update?: ContentRestriction;
                _expandable?: {
                    read?: string;
                    update?: string;
                };
                _links?: GenericLinks;
            };
            metadata?: /* Metadata object for page, blogpost, comment content */ ContentMetadata;
            macroRenderedOutput?: {
                [name: string]: {
                    [key: string]: any;
                };
            };
            extensions?: {
                [key: string]: any;
            };
            _expandable?: {
                childTypes?: string;
                container?: string;
                metadata?: string;
                operations?: string;
                children?: string;
                restrictions?: string;
                history?: string;
                ancestors?: string;
                body?: string;
                version?: string;
                descendants?: string;
                space?: string;
                extensions?: string;
                schedulePublishDate?: string;
                schedulePublishInfo?: string;
                macroRenderedOutput?: string;
            };
            _links?: GenericLinks;
        } | null;
        export interface ContentArray {
            results: /* Base object for all content types. */ Content[];
            start?: number; // int32
            limit?: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        /**
         * Representation of a blogpost (content)
         */
        export type ContentBlogpost = {
            [name: string]: any;
            id?: string;
            /**
             * Can be "page", "blogpost", "attachment" or "content"
             */
            type: string;
            status: string;
            title?: string;
            space?: Space;
            history?: ContentHistory;
            version?: Version;
            ancestors?: /* Base object for all content types. */ Content[] | null;
            operations?: /* An operation and the target entity that it applies to, e.g. create page. */ OperationCheckResult[];
            children?: ContentChildren;
            childTypes?: /**
             * Shows whether a piece of content has attachments, comments, or child pages/whiteboards.
             * Note, this doesn't actually contain the child objects.
             */
            ContentChildType;
            descendants?: ContentChildren;
            container?: /**
             * Container for content. This can be either a space (containing a page or blogpost)
             * or a page/blog post (containing an attachment or comment)
             */
            Container;
            body?: {
                view?: ContentBody;
                export_view?: ContentBody;
                styled_view?: ContentBody;
                storage?: ContentBody;
                wiki?: ContentBody;
                editor?: ContentBody;
                editor2?: ContentBody;
                anonymous_export_view?: ContentBody;
                atlas_doc_format?: ContentBody;
                dynamic?: ContentBody;
                raw?: ContentBody;
                _expandable?: {
                    editor?: string;
                    view?: string;
                    export_view?: string;
                    styled_view?: string;
                    storage?: string;
                    editor2?: string;
                    anonymous_export_view?: string;
                    atlas_doc_format?: string;
                    wiki?: string;
                    dynamic?: string;
                    raw?: string;
                };
            };
            restrictions?: {
                read?: ContentRestriction;
                update?: ContentRestriction;
                _expandable?: {
                    read?: string;
                    update?: string;
                };
                _links?: GenericLinks;
            };
            metadata: /* Metadata object for page, blogpost, comment content */ ContentMetadata;
            macroRenderedOutput?: {
                [name: string]: {
                    [key: string]: any;
                };
            };
            extensions?: {
                [key: string]: any;
            };
            _expandable?: {
                childTypes?: string;
                container?: string;
                metadata?: string;
                operations?: string;
                children?: string;
                restrictions?: string;
                history?: string;
                ancestors?: string;
                body?: string;
                version?: string;
                descendants?: string;
                space?: string;
                extensions?: string;
                schedulePublishDate?: string;
                schedulePublishInfo?: string;
                macroRenderedOutput?: string;
            };
            _links: GenericLinks;
        } | null;
        export interface ContentBlueprintDraft {
            [name: string]: any;
            /**
             * The version for the new content.
             */
            version: {
                [name: string]: any;
                /**
                 * The version number. Set this to `1`.
                 */
                number: number; // int32
            };
            /**
             * The title of the content. If you don't want to change the title,
             * set this to the current title of the draft.
             */
            title: string;
            /**
             * The type of content. Set this to `page`.
             */
            type: "page";
            /**
             * The status of the content. Set this to `current` or omit it altogether.
             */
            status?: "current";
            /**
             * The space for the content.
             */
            space?: {
                [name: string]: any;
                /**
                 * The key of the space
                 */
                key: string; // int32
            };
            /**
             * The new ancestor (i.e. parent page) for the content. If you have
             * specified an ancestor, you must also specify a `space` property
             * in the request body for the space that the ancestor is in.
             *
             * Note, if you specify more than one ancestor, the last ID in the array
             * will be selected as the parent page for the content.
             */
            ancestors?: {
                /**
                 * The content ID of the ancestor.
                 */
                id: string;
            }[] | null;
        }
        export interface ContentBody {
            value: string;
            representation: "view" | "export_view" | "styled_view" | "storage" | "editor" | "editor2" | "anonymous_export_view" | "wiki" | "atlas_doc_format" | "raw";
            embeddedContent?: EmbeddedContent[];
            webresource?: WebResourceDependencies;
            mediaToken?: {
                collectionIds?: string[];
                contentId?: string;
                expiryDateTime?: string;
                fileIds?: string[];
                token?: string;
            };
            _expandable?: {
                content?: string;
                embeddedContent?: string;
                webresource?: string;
                mediaToken?: string;
            };
            _links?: GenericLinks;
        }
        /**
         * This object is used when creating or updating content.
         */
        export interface ContentBodyCreate {
            [name: string]: any;
            /**
             * The body of the content in the relevant format.
             */
            value: string;
            /**
             * The content format type. Set the value of this property to
             * the name of the format being used, e.g. 'storage'.
             */
            representation: "view" | "export_view" | "styled_view" | "storage" | "editor" | "editor2" | "anonymous_export_view" | "wiki" | "atlas_doc_format" | "plain" | "raw";
        }
        /**
         * This object is used when creating or updating content.
         */
        export interface ContentBodyCreateStorage {
            [name: string]: any;
            /**
             * The body of the content in the relevant format.
             */
            value: string;
            /**
             * The content format type. Set the value of this property to
             * the name of the format being used, e.g. 'storage'.
             */
            representation: "storage" | "view" | "export_view" | "styled_view" | "editor" | "editor2" | "anonymous_export_view" | "wiki" | "atlas_doc_format";
        }
        /**
         * Shows whether a piece of content has attachments, comments, or child pages/whiteboards.
         * Note, this doesn't actually contain the child objects.
         */
        export interface ContentChildType {
            [name: string]: any;
            attachment?: {
                value: boolean;
                _links: GenericLinks;
            };
            comment?: {
                value: boolean;
                _links: GenericLinks;
            };
            page?: {
                value: boolean;
                _links: GenericLinks;
            };
            _expandable?: {
                all?: string;
                attachment?: string;
                comment?: string;
                page?: string;
                whiteboard?: string;
            };
        }
        export interface ContentChildren {
            [name: string]: any;
            attachment?: ContentArray;
            comment?: ContentArray;
            page?: ContentArray;
            _expandable?: {
                [name: string]: any;
                attachment?: string;
                comment?: string;
                page?: string;
            };
            _links?: GenericLinks;
        }
        export type ContentCreate = {
            [name: string]: any;
            /**
             * The ID of the draft content. Required when publishing a draft.
             */
            id?: string | null;
            title?: string | null;
            /**
             * The type of the new content. Custom content types defined by apps are also supported. eg. 'page', 'blogpost', 'comment' etc.
             */
            type: string;
            /**
             * The space that the content is being created in.
             */
            space?: {
                id?: number | null; // int64
                name?: string | null;
                icon?: /* This object represents an icon. If used as a profilePicture, this may be returned as null, depending on the user's privacy setting. */ Icon;
                identifiers?: GlobalSpaceIdentifier;
                description?: {
                    plain?: SpaceDescription;
                    view?: SpaceDescription;
                    _expandable?: {
                        view?: string;
                        plain?: string;
                    };
                } | null;
                homepage?: /* Base object for all content types. */ Content;
                type?: string | null;
                metadata?: {
                    labels?: LabelArray;
                    _expandable?: {
                        [key: string]: any;
                    };
                } | null;
                operations?: /* An operation and the target entity that it applies to, e.g. create page. */ OperationCheckResult[] | null;
                permissions?: /**
                 * This object represents a permission for given space. Permissions consist of
                 * at least one operation object with an accompanying subjects object.
                 *
                 * The following combinations of `operation` and `targetType` values are
                 * valid for the `operation` object:
                 *
                 *   - 'create': 'page', 'blogpost', 'comment', 'attachment'
                 *   - 'read': 'space'
                 *   - 'delete': 'page', 'blogpost', 'comment', 'attachment'
                 *   - 'export': 'space'
                 *   - 'administer': 'space'
                 */
                SpacePermission[] | null;
                status?: string | null;
                settings?: SpaceSettings;
                theme?: Theme;
                lookAndFeel?: LookAndFeel;
                history?: {
                    createdDate: string; // date-time
                    createdBy?: User;
                };
                /**
                 * The key of the space.
                 */
                key: string;
                links?: {
                    [name: string]: any;
                } | null;
            } | null;
            /**
             * The status of the new content.
             */
            status?: "current" | "deleted" | "historical" | "draft";
            /**
             * The container of the content. Required if type is `comment` or certain types of
             * custom content. If you are trying to create a comment that is a child of another comment,
             * specify the parent comment in the ancestors field, not in this field.
             */
            container?: {
                [name: string]: any;
                /**
                 * The `id` of the container.
                 */
                id: /* The `id` of the container. */ number | string;
                /**
                 * The `type` of the container.
                 */
                type: string;
            } | null;
            /**
             * The parent content of the new content.  If you are creating a top-level `page` or `comment`,
             * this can be left blank. If you are creating a child page, this is where the parent page id goes.
             * If you are creating a child comment, this is where the parent comment id goes. Only one parent
             * content id can be specified.
             */
            ancestors?: {
                [name: string]: any;
                /**
                 * The `id` of the parent content.
                 */
                id: string;
            }[] | null;
            /**
             * The body of the new content. Does not apply to attachments.
             * Only one body format should be specified as the property for
             * this object, e.g. `storage`.
             *
             * Note, `editor2` format is used by Atlassian only. `anonymous_export_view` is
             * the same as `export_view` format but only content viewable by an anonymous
             * user is included.
             */
            body?: {
                view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                export_view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                styled_view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                storage?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                editor?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                editor2?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                wiki?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                anonymous_export_view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                plain?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                atlas_doc_format?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                raw?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            };
        } | null;
        export type ContentHistory = {
            latest: boolean;
            createdBy?: User;
            ownedBy?: User;
            lastOwnedBy?: User;
            createdDate?: string; // date-time
            lastUpdated?: Version;
            previousVersion?: Version;
            contributors?: {
                publishers?: UsersUserKeys;
            };
            nextVersion?: Version;
            _expandable?: {
                lastUpdated?: string;
                previousVersion?: string;
                contributors?: string;
                nextVersion?: string;
                ownedBy?: string;
                lastOwnedBy?: string;
            };
            _links?: GenericLinks;
        } | null;
        export type ContentId = string;
        export interface ContentLookAndFeel {
            screen?: ScreenLookAndFeel;
            container?: ContainerLookAndFeel;
            header?: ContainerLookAndFeel;
            body?: ContainerLookAndFeel;
        }
        /**
         * Metadata object for page, blogpost, comment content
         */
        export interface ContentMetadata {
            [name: string]: any;
            currentuser?: {
                favourited?: {
                    isFavourite?: boolean;
                    favouritedDate?: string; // date-time
                };
                lastmodified?: {
                    version?: Version;
                    friendlyLastModified?: string;
                };
                lastcontributed?: {
                    status?: string;
                    when?: string; // date-time
                };
                viewed?: {
                    lastSeen?: string; // date-time
                    friendlyLastSeen?: string;
                };
                scheduled?: {
                    [key: string]: any;
                };
                _expandable?: {
                    favourited?: string;
                    lastmodified?: string;
                    lastcontributed?: string;
                    viewed?: string;
                    scheduled?: string;
                };
            };
            properties?: GenericLinks;
            frontend?: {
                [name: string]: any;
            };
            labels?: LabelArray | Label[];
        }
        export interface ContentPageResponse {
            results: /* Base object for all content types. */ Content[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
        }
        /**
         * This object represents the request for the content permission check API.
         */
        export interface ContentPermissionRequest {
            subject: /* The user or group that the permission applies to. */ PermissionSubjectWithGroupId;
            /**
             * The content permission operation to check.
             */
            operation: "read" | "update" | "delete";
        }
        export interface ContentProperty {
            [name: string]: any;
            id: string;
            key: string;
            /**
             * The value of the content property. This can be empty or a complex object.
             */
            value: /* The value of the content property. This can be empty or a complex object. */ string[] | boolean | {
                [name: string]: any;
            } | string;
            version?: {
                [name: string]: any;
                when: string; // date-time
                message: string;
                number: number; // int32
                minorEdit: boolean;
                /**
                 * True if content type is modifed in this version (e.g. page to blog)
                 */
                contentTypeModified?: boolean;
            };
            _links: GenericLinks;
            _expandable?: {
                content?: string;
                additionalProperties?: string;
            };
        }
        export interface ContentPropertyArray {
            results: ContentProperty[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        export interface ContentPropertyCreate {
            [name: string]: any;
            /**
             * The key of the new property.
             */
            key: string;
            /**
             * The value of the content property. This can be empty or a complex object.
             */
            value: /* The value of the content property. This can be empty or a complex object. */ string[] | boolean | {
                [name: string]: any;
            } | string;
        }
        export interface ContentPropertyCreateNoKey {
            [name: string]: any;
            /**
             * The value of the content property. This can be empty or a complex object.
             */
            value: /* The value of the content property. This can be empty or a complex object. */ string[] | boolean | {
                [name: string]: any;
            } | string;
        }
        export interface ContentPropertyUpdate {
            [name: string]: any;
            /**
             * The value of the content property. This can be empty or a complex object.
             */
            value: /* The value of the content property. This can be empty or a complex object. */ string[] | boolean | {
                [name: string]: any;
            } | string;
            /**
             * The version number of the property.
             */
            version: {
                [name: string]: any;
                /**
                 * The new version for the updated content property. Set this to the
                 * current version number incremented by one. To get the current
                 * version number, use 'Get content property' and retrieve
                 * `version.number`.
                 */
                number: /**
                 * The new version for the updated content property. Set this to the
                 * current version number incremented by one. To get the current
                 * version number, use 'Get content property' and retrieve
                 * `version.number`.
                 */
                number /* int32 */ | string /* int32 */ // int32; // int32
                /**
                 * If `minorEdit` is set to 'true', no notification email or activity
                 * stream will be generated for the change.
                 */
                minorEdit?: boolean;
            } | null;
        }
        export interface ContentRestriction {
            operation: "administer" | "copy" | "create" | "delete" | "export" | "move" | "purge" | "purge_version" | "read" | "restore" | "update" | "use";
            restrictions?: {
                user?: UserArray;
                group?: GroupArray;
                _expandable?: {
                    user?: string;
                    group?: string;
                };
            };
            content?: /* Base object for all content types. */ Content;
            _expandable: {
                restrictions?: string;
                content?: string;
            };
            _links: GenericLinks;
        }
        export type ContentRestrictionAddOrUpdateArray = {
            results: ContentRestrictionUpdate[];
            start?: number; // int32
            limit?: number; // int32
            size?: number; // int32
            /**
             * This property is used by the UI to figure out whether a set of restrictions
             * has changed.
             */
            restrictionsHash?: string;
            _links?: GenericLinks;
        } | ContentRestrictionUpdate[];
        export interface ContentRestrictionArray {
            results: ContentRestriction[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            /**
             * This property is used by the UI to figure out whether a set of restrictions
             * has changed.
             */
            restrictionsHash: string;
            _links: GenericLinks;
        }
        export interface ContentRestrictionUpdate {
            /**
             * The restriction operation applied to content.
             */
            operation: "administer" | "copy" | "create" | "delete" | "export" | "move" | "purge" | "purge_version" | "read" | "restore" | "update" | "use";
            /**
             * The users/groups that the restrictions will be applied to. At least one of
             * `user` or `group` must be specified for this object.
             */
            restrictions: {
                /**
                 * The groups that the restrictions will be applied to. This array must
                 * have at least one item, otherwise it should be omitted. At least one of `name` or `id` is required,
                 * and `id` should be used where possible in advance of the `name` deprecation.
                 */
                group?: {
                    /**
                     * Set to 'group'.
                     */
                    type: "group";
                    /**
                     * The name of the group.
                     */
                    name?: string;
                    /**
                     * The id of the group.
                     */
                    id?: string;
                }[];
                user?: User[] | UserArray;
            };
            content?: /* Base object for all content types. */ Content;
        }
        export interface ContentState {
            /**
             * identifier of content state. If 0, 1, or 2, this is a default space state
             */
            id: number; // int64
            /**
             * name of content state.
             */
            name: string;
            /**
             * hex string representing color of state
             */
            color: string;
        }
        export interface ContentStateBulkSetTaskUpdate {
            set: ContentId[];
            failed: /* Object describing why a content state set failed */ ContentStateFailure[];
            percentage: number; // int64
            message?: string;
            state?: ContentState;
            success: boolean;
        }
        /**
         * Object describing why a content state set failed
         */
        export interface ContentStateFailure {
            contentId: ContentId;
            failureReason: string;
        }
        export interface ContentStateInput {
            name?: string;
            /**
             * Color of state. Must be in 6 digit hex form (#FFFFFF). The default colors offered in the UI are:
             *  #ff7452 (red),
             *  #2684ff (blue),
             *  #ffc400 (yellow),
             *  #57d9a3 (green), and
             *  #8777d9 (purple)
             */
            color?: string;
            id?: number; // int32
            spaceKey?: string;
        }
        export interface ContentStateResponse {
            /**
             * Null or content state
             */
            contentState?: ContentState;
            /**
             * Timestamp of last publish event where content state changed
             */
            lastUpdated?: string;
        }
        export interface ContentStateRestInput {
            /**
             * Name of content state. Maximum 20 characters.
             */
            name?: string;
            /**
             * Color of state. Must be in 6 digit hex form (#FFFFFF). The default colors offered in the UI are:
             *  #ff7452 (red),
             *  #2684ff (blue),
             *  #ffc400 (yellow),
             *  #57d9a3 (green), and
             *  #8777d9 (purple)
             */
            color?: string;
            /**
             * id of state. This can be 0,1, or 2 if you wish to specify a default space state.
             */
            id?: number; // int64
        }
        export interface ContentStateSettings {
            /**
             * Whether users can place any content states on content
             */
            contentStatesAllowed: boolean;
            /**
             * Whether users can place their custom states on content
             */
            customContentStatesAllowed: boolean;
            /**
             * Whether users can place space suggested states on content
             */
            spaceContentStatesAllowed: boolean;
            /**
             * space suggested content states that users can choose from
             */
            spaceContentStates?: ContentState[];
        }
        export interface ContentTemplate {
            templateId: string;
            originalTemplate?: {
                pluginKey?: string;
                moduleKey?: string;
            };
            referencingBlueprint?: string;
            name: string;
            description: string;
            space?: {
                [name: string]: any;
            };
            labels: Label[];
            templateType: string;
            editorVersion?: string;
            body?: /**
             * The body of the new content. Does not apply to attachments.
             * Only one body format should be specified as the property for
             * this object, e.g. `storage`.
             *
             * Note, `editor2` format is used by Atlassian only. `anonymous_export_view` is
             * the same as `export_view` format but only content viewable by an anonymous
             * user is included.
             */
            ContentTemplateBody;
            _expandable?: {
                body?: string;
            };
            _links: GenericLinks;
        }
        export interface ContentTemplateArray {
            results: ContentTemplate[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        /**
         * The body of the new content. Does not apply to attachments.
         * Only one body format should be specified as the property for
         * this object, e.g. `storage`.
         *
         * Note, `editor2` format is used by Atlassian only. `anonymous_export_view` is
         * the same as `export_view` format but only content viewable by an anonymous
         * user is included.
         */
        export interface ContentTemplateBody {
            view?: ContentBody;
            export_view?: ContentBody;
            styled_view?: ContentBody;
            storage?: ContentBody;
            editor?: ContentBody;
            editor2?: ContentBody;
            wiki?: ContentBody;
            atlas_doc_format?: ContentBody;
            anonymous_export_view?: ContentBody;
        }
        /**
         * The body of the new content. Does not apply to attachments.
         * Only one body format should be specified as the property for
         * this object, e.g. `storage`.
         *
         * Note, `editor2` format is used by Atlassian only. `anonymous_export_view` is
         * the same as `export_view` format but only content viewable by an anonymous
         * user is included.
         */
        export interface ContentTemplateBodyCreate {
            view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            export_view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            styled_view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            storage?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            editor?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            editor2?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            wiki?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            atlas_doc_format?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            anonymous_export_view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
        }
        /**
         * This object is used to create content templates.
         */
        export interface ContentTemplateCreate {
            [name: string]: any;
            /**
             * The name of the new template.
             */
            name: string;
            /**
             * The type of the new template. Set to `page`.
             */
            templateType: string;
            body: /**
             * The body of the new content. Does not apply to attachments.
             * Only one body format should be specified as the property for
             * this object, e.g. `storage`.
             *
             * Note, `editor2` format is used by Atlassian only. `anonymous_export_view` is
             * the same as `export_view` format but only content viewable by an anonymous
             * user is included.
             */
            ContentTemplateBodyCreate;
            /**
             * A description of the new template.
             */
            description?: string;
            /**
             * Labels for the new template.
             */
            labels?: Label[];
            /**
             * The key for the space of the new template. Only applies to space templates.
             * If the spaceKey is not specified, the template will be created as a global
             * template.
             */
            space?: {
                [name: string]: any;
                key: string;
            } | null;
        }
        /**
         * This object is used to update content templates.
         */
        export interface ContentTemplateUpdate {
            [name: string]: any;
            /**
             * The ID of the template being updated.
             */
            templateId: string;
            /**
             * The name of the template. Set to the current `name` if this field is
             * not being updated.
             */
            name: string;
            /**
             * The type of the template. Set to `page`.
             */
            templateType: "page";
            body: /**
             * The body of the new content. Does not apply to attachments.
             * Only one body format should be specified as the property for
             * this object, e.g. `storage`.
             *
             * Note, `editor2` format is used by Atlassian only. `anonymous_export_view` is
             * the same as `export_view` format but only content viewable by an anonymous
             * user is included.
             */
            ContentTemplateBodyCreate;
            /**
             * A description of the template.
             */
            description?: string;
            /**
             * Labels for the template.
             */
            labels?: Label[];
            /**
             * The key for the space of the template. Required if the template is a
             * space template. Set this to the current `space.key`.
             */
            space?: {
                [name: string]: any;
                key: string;
            } | null;
        }
        export interface ContentUpdate {
            [name: string]: any;
            /**
             * The new version for the updated content. Set this to the current version number incremented by one, unless you are changing the status to 'draft' which must have a version number of 1.
             *
             * To get the current version number, use [Get content by ID](#api-content-id-get) and retrieve `version.number`.
             */
            version: {
                [name: string]: any;
                /**
                 * The version number.
                 */
                number: number; // int32
                /**
                 * An optional message to be stored with the corresponding version.
                 */
                message?: string | null;
            } | null;
            /**
             * The updated title of the content. If you are updating a non-draft `page` or `blogpost`, title is required. If you are not changing the title, set this field to the the current title.
             */
            title?: string | null;
            /**
             * The type of content. Set this to the current type of the content. For example, - page - blogpost - comment - attachment
             */
            type: string | null;
            /**
             * The updated status of the content. Note, if you change the status of a page from
             * 'current' to 'draft' and it has an existing draft, the existing draft will be deleted
             * in favor of the updated page.
             */
            status?: "current" | "trashed" | "deleted" | "historical" | "draft";
            /**
             * The new parent for the content. Only one parent content 'id' can be specified.
             */
            ancestors?: {
                [name: string]: any;
                /**
                 * The `id` of the parent content.
                 */
                id: /* The `id` of the parent content. */ number | string;
            }[] | null;
            /**
             * The updated body of the content. Does not apply to attachments.
             * If you are not sure how to generate these formats, you can create a page in the
             * Confluence application, retrieve the content using [Get content](#api-content-get),
             * and expand the desired content format, e.g. `expand=body.storage`.
             */
            body?: {
                view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                export_view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                styled_view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                storage?: /* This object is used when creating or updating content. */ ContentBodyCreateStorage;
                editor?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                editor2?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                wiki?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                atlas_doc_format?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                anonymous_export_view?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            };
        }
        export interface CopyPageHierarchyRequest {
            /**
             * If set to `true`, attachments are copied to the destination page.
             */
            copyAttachments?: boolean;
            /**
             * If set to `true`, page permissions are copied to the destination page.
             */
            copyPermissions?: boolean;
            /**
             * If set to `true`, content properties are copied to the destination page.
             */
            copyProperties?: boolean;
            /**
             * If set to `true`, labels are copied to the destination page.
             */
            copyLabels?: boolean;
            /**
             * If set to `true`, custom contents are copied to the destination page.
             */
            copyCustomContents?: boolean;
            /**
             * If set to `true`, descendants are copied to the destination page.
             */
            copyDescendants?: boolean;
            destinationPageId: ContentId;
            titleOptions?: /* Required for copying page in the same space. */ CopyPageHierarchyTitleOptions;
        }
        /**
         * Required for copying page in the same space.
         */
        export interface CopyPageHierarchyTitleOptions {
            prefix?: string;
            replace?: string;
            search?: string;
        }
        export interface CopyPageRequest {
            /**
             * If set to `true`, attachments are copied to the destination page.
             */
            copyAttachments?: boolean;
            /**
             * If set to `true`, page permissions are copied to the destination page.
             */
            copyPermissions?: boolean;
            /**
             * If set to `true`, content properties are copied to the destination page.
             */
            copyProperties?: boolean;
            /**
             * If set to `true`, labels are copied to the destination page.
             */
            copyLabels?: boolean;
            /**
             * If set to `true`, custom contents are copied to the destination page.
             */
            copyCustomContents?: boolean;
            destination: /**
             * Defines where the page will be copied to, and can be one of the following types.
             *
             *   - `parent_page`: page will be copied as a child of the specified parent page
             *   - `space`: page will be copied to the specified space as a root page on the space
             *   - `existing_page`: page will be copied and replace the specified page
             */
            CopyPageRequestDestination;
            /**
             * If defined, this will replace the title of the destination page.
             */
            pageTitle?: string;
            /**
             * If defined, this will replace the body of the destination page.
             */
            body?: {
                storage?: /* This object is used when creating or updating content. */ ContentBodyCreate;
                editor2?: /* This object is used when creating or updating content. */ ContentBodyCreate;
            };
        }
        /**
         * Defines where the page will be copied to, and can be one of the following types.
         *
         *   - `parent_page`: page will be copied as a child of the specified parent page
         *   - `space`: page will be copied to the specified space as a root page on the space
         *   - `existing_page`: page will be copied and replace the specified page
         */
        export interface CopyPageRequestDestination {
            type: "space" | "existing_page" | "parent_page";
            /**
             * The space key for `space` type, and content id for `parent_page` and `existing_page`
             */
            value: string;
        }
        /**
         * example:
         * {
         *   "message": "The request is not from a Connect app."
         * }
         */
        export interface DynamicModulesErrorMessage {
            /**
             * The error message.
             */
            message: string;
        }
        export interface Embeddable {
            [name: string]: any;
        }
        export interface EmbeddedContent {
            [name: string]: any;
            entityId?: number; // int64
            entityType?: string;
            entity?: Embeddable;
        }
        /**
         * The account ID of the user, which uniquely identifies the user across all Atlassian products.
         * For example, `384093:32b4d9w0-f6a5-3535-11a3-9c8c88d10192`.
         */
        export type GenericAccountId = string | null;
        export interface GenericLinks {
            [name: string]: {
                [name: string]: any;
            } | string;
        }
        /**
         * This property is no longer available and will be removed from the documentation soon.
         * Use `accountId` instead.
         * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
         */
        export type GenericUserKey = string | null;
        /**
         * This property is no longer available and will be removed from the documentation soon.
         * Use `accountId` instead.
         * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
         */
        export type GenericUserName = string | null;
        export type GlobalSpaceIdentifier = {
            spaceIdentifier: string;
        } | null;
        export interface Group {
            type: "group";
            name: string;
            id: string;
            _links?: GenericLinks;
        }
        export interface GroupArray {
            results: Group[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
        }
        /**
         * Same as GroupArray but with `_links` property.
         */
        export interface GroupArrayWithLinks {
            results: Group[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            /**
             * This property will return total count of the objects before pagination is applied.
             * This value is returned if `shouldReturnTotalSize` is set to `true`.
             */
            totalSize?: number; // int64
            _links: GenericLinks;
        }
        /**
         * The name property will soon be deprecated in favor of using id.
         */
        export interface GroupCreate {
            [name: string]: any;
            type: "group";
            name?: string;
            id?: string;
        }
        export interface GroupName {
            name: string;
        }
        export interface HeaderLookAndFeel {
            backgroundColor: string;
            button: ButtonLookAndFeel;
            primaryNavigation: NavigationLookAndFeel;
            secondaryNavigation: NavigationLookAndFeel;
            search: SearchFieldLookAndFeel;
        }
        export interface HorizontalHeaderLookAndFeel {
            backgroundColor: string;
            button?: ButtonLookAndFeel;
            primaryNavigation: TopNavigationLookAndFeel;
            secondaryNavigation?: NavigationLookAndFeel;
            search?: SearchFieldLookAndFeel;
        }
        /**
         * This object represents an icon. If used as a profilePicture, this may be returned as null, depending on the user's privacy setting.
         */
        export type Icon = {
            path: string;
            width: number; // int32
            height: number; // int32
            isDefault: boolean;
        } | null;
        export interface Label {
            prefix: string;
            name: string;
            id: string;
            label: string;
        }
        export interface LabelArray {
            results: Label[];
            start?: number; // int32
            limit?: number; // int32
            size: number; // int32
            _links?: GenericLinks;
        }
        export interface LabelCreate {
            [name: string]: any;
            /**
             * The prefix for the label. `global`, `my` `team`, etc.
             */
            prefix: string;
            /**
             * The name of the label, which will be shown in the UI.
             */
            name: string;
        }
        export type LabelCreateArray = LabelCreate[];
        export interface LabelDetails {
            label: Label;
            associatedContents?: LabeledContentPageResponse;
        }
        export interface LabeledContent {
            contentType: LabeledContentType;
            contentId: number; // int64
            /**
             * Title of the content.
             */
            title: string;
        }
        export interface LabeledContentPageResponse {
            results: LabeledContent[];
            start?: number; // int32
            limit?: number; // int32
            size: number; // int32
        }
        export type LabeledContentType = "page" | "blogpost" | "attachment" | "page_template";
        export interface LongTask {
            /**
             * the ARI for the long task, based on its ID
             */
            ari?: string;
            /**
             * a unique identifier for the long task
             */
            id: string;
            links: {
                [name: string]: any;
                /**
                 * The URL to retrive status of long task.
                 */
                status?: string;
            };
        }
        /**
         * Current status of a long running task
         *
         * Status keys:
         *
         * - `ERROR_UNKNOWN` - Generic error
         * - `ERROR_LOCK_FAILED` - Could not get the lock on destination space
         * - `ERROR_RELINK` - Error when relink pages/attachments
         * - `ERROR_COPY_PAGE` - Error while copying 1 page
         * - `WARN_RENAME_PAGE` - Warning page is rename during copy
         * - `WARN_IGNORE_COPY_PERMISSION` - Warning could not copy permission
         * - `WARN_IGNORE_COPY_ATTACHMENT` - Warning could not copy attachment
         * - `WARN_IGNORE_DELETE_PAGE` - Warning ignoring delete of a non agreed on page
         * - `STATUS_COPIED_PAGES` - Message total pages are copied
         * - `STATUS_COPYING_PAGES` - Message copy pages
         * - `STATUS_RELINK_PAGES` - Message relink pages/attachments
         * - `STATUS_DELETING_PAGES` - Message delete pages
         * - `STATUS_DELETED_PAGES` - Message total pages are deleted
         * - `STATUS_MOVING_PAGES` - Message move pages
         * - `WARN_IGNORE_VIEW_RESTRICTED` - Permission changed - view restricted
         * - `WARN_IGNORE_EDIT_RESTRICTED` - Permission changed - edit restricted
         * - `INITIALIZING_TASK` - Message when initializing task
         * - `UNKNOWN_STATUS` - Message when status is unknown
         */
        export interface LongTaskStatus {
            /**
             * the ARI for the long task, based on its ID
             */
            ari?: string;
            id: string;
            name: {
                key: string;
                args: {
                    [key: string]: any;
                }[];
            };
            elapsedTime: number; // int64
            percentageComplete: number; // int32
            successful: boolean;
            finished: boolean;
            messages: Message[];
            status?: string;
            errors?: Message[];
            additionalDetails?: {
                destinationId?: string;
                destinationUrl?: string;
                totalPageNeedToCopy?: number;
                additionalProperties?: string;
            };
        }
        export interface LongTaskStatusArray {
            results: /**
             * Current status of a long running task
             *
             * Status keys:
             *
             * - `ERROR_UNKNOWN` - Generic error
             * - `ERROR_LOCK_FAILED` - Could not get the lock on destination space
             * - `ERROR_RELINK` - Error when relink pages/attachments
             * - `ERROR_COPY_PAGE` - Error while copying 1 page
             * - `WARN_RENAME_PAGE` - Warning page is rename during copy
             * - `WARN_IGNORE_COPY_PERMISSION` - Warning could not copy permission
             * - `WARN_IGNORE_COPY_ATTACHMENT` - Warning could not copy attachment
             * - `WARN_IGNORE_DELETE_PAGE` - Warning ignoring delete of a non agreed on page
             * - `STATUS_COPIED_PAGES` - Message total pages are copied
             * - `STATUS_COPYING_PAGES` - Message copy pages
             * - `STATUS_RELINK_PAGES` - Message relink pages/attachments
             * - `STATUS_DELETING_PAGES` - Message delete pages
             * - `STATUS_DELETED_PAGES` - Message total pages are deleted
             * - `STATUS_MOVING_PAGES` - Message move pages
             * - `WARN_IGNORE_VIEW_RESTRICTED` - Permission changed - view restricted
             * - `WARN_IGNORE_EDIT_RESTRICTED` - Permission changed - edit restricted
             * - `INITIALIZING_TASK` - Message when initializing task
             * - `UNKNOWN_STATUS` - Message when status is unknown
             */
            LongTaskStatus[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        /**
         * Same as LongTaskStatus but with `_links` property.
         *
         * Status keys:
         *
         * - `ERROR_UNKNOWN` - Generic error
         * - `ERROR_LOCK_FAILED` - Could not get the lock on destination space
         * - `ERROR_RELINK` - Error when relink pages/attachments
         * - `ERROR_COPY_PAGE` - Error while copying 1 page
         * - `WARN_RENAME_PAGE` - Warning page is rename during copy
         * - `WARN_IGNORE_COPY_PERMISSION` - Warning could not copy permission
         * - `WARN_IGNORE_COPY_ATTACHMENT` - Warning could not copy attachment
         * - `WARN_IGNORE_DELETE_PAGE` - Warning ignoring delete of a non agreed on page
         * - `STATUS_COPIED_PAGES` - Message total pages are copied
         * - `STATUS_COPYING_PAGES` - Message copy pages
         * - `STATUS_RELINK_PAGES` - Message relink pages/attachments
         * - `STATUS_DELETING_PAGES` - Message delete pages
         * - `STATUS_DELETED_PAGES` - Message total pages are deleted
         * - `STATUS_MOVING_PAGES` - Message move pages
         * - `WARN_IGNORE_VIEW_RESTRICTED` - Permission changed - view restricted
         * - `WARN_IGNORE_EDIT_RESTRICTED` - Permission changed - edit restricted
         * - `INITIALIZING_TASK` - Message when initializing task
         * - `UNKNOWN_STATUS` - Message when status is unknown
         */
        export interface LongTaskStatusWithLinks {
            /**
             * the ARI for the long task, based on its ID
             */
            ari?: string;
            id: string;
            name: {
                key: string;
                args: {
                    [key: string]: any;
                }[];
            };
            elapsedTime: number; // int64
            percentageComplete: number; // int32
            successful: boolean;
            finished: boolean;
            messages: Message[];
            _links: GenericLinks;
            status?: string;
            errors?: Message[];
            additionalDetails?: {
                destinationId?: string | null;
                destinationUrl?: string;
                totalPageNeedToCopy?: number;
                additionalProperties?: string;
            };
        }
        export interface LookAndFeel {
            headings: {
                color: string;
            };
            links: {
                color: string;
            };
            menus: MenusLookAndFeel;
            header: HeaderLookAndFeel;
            horizontalHeader?: HorizontalHeaderLookAndFeel;
            content: ContentLookAndFeel;
            bordersAndDividers: {
                color: string;
            };
            spaceReference?: {
                [key: string]: any;
            } | null;
        }
        /**
         * Look and feel selection
         */
        export interface LookAndFeelSelection {
            /**
             * The key of the space for which the look and feel settings will be
             * set.
             */
            spaceKey: string;
            lookAndFeelType: "global" | "custom" | "theme";
        }
        export interface LookAndFeelSettings {
            selected: "global" | "custom";
            global: LookAndFeel;
            theme?: LookAndFeel;
            custom: LookAndFeel;
        }
        /**
         * Look and feel settings returned after an update.
         */
        export interface LookAndFeelWithLinks {
            headings: {
                color: string;
            };
            links: {
                color: string;
            };
            menus: MenusLookAndFeel;
            header: HeaderLookAndFeel;
            horizontalHeader?: HorizontalHeaderLookAndFeel;
            content: ContentLookAndFeel;
            bordersAndDividers: {
                color: string;
            };
            spaceReference?: {
                [key: string]: any;
            } | null;
            _links?: GenericLinks;
        }
        export interface MacroInstance {
            name?: string;
            body?: string;
            parameters?: {
                [key: string]: any;
            };
            _links?: GenericLinks;
        }
        export interface MenusLookAndFeel {
            hoverOrFocus: {
                backgroundColor: string;
            };
            color: string;
        }
        export interface Message {
            [name: string]: any;
            translation?: string;
            args: (string | {
                [name: string]: any;
            })[];
        }
        export type NavigationLookAndFeel = {
            color: string;
            highlightColor?: string | null;
            hoverOrFocus: {
                backgroundColor: string;
                color: string;
            };
        } | null;
        /**
         * An operation and the target entity that it applies to, e.g. create page.
         */
        export interface OperationCheckResult {
            /**
             * The operation itself.
             */
            operation: "administer" | "archive" | "clear_permissions" | "copy" | "create" | "create_space" | "delete" | "export" | "move" | "purge" | "purge_version" | "read" | "restore" | "restrict_content" | "update" | "use";
            /**
             * The space or content type that the operation applies to. Could be one of- - application - page - blogpost - comment - attachment - space
             */
            targetType: string;
        }
        /**
         * This object represents the response for the content permission check API. If the user or group does not have
         * permissions, the following errors may be returned:
         *
         * - Group does not have permission to the space
         * - Group does not have permission to the content
         * - User is not allowed to use Confluence
         * - User does not have permission to the space
         * - User does not have permission to the content
         * - Anonymous users are not allowed to use Confluence
         * - Anonymous user does not have permission to the space
         * - Anonymous user does not have permission to the content
         */
        export interface PermissionCheckResponse {
            hasPermission: boolean;
            errors?: Message[];
            _links?: GenericLinks;
        }
        /**
         * The user or group that the permission applies to.
         */
        export interface PermissionSubject {
            type: "user" | "group";
            /**
             * for `type=user`, identifier should be user's accountId or `anonymous` for anonymous users
             *
             * for `type=group`, identifier should be the groupId. We are deprecating groupName support in mid-2024
             * for this field but still accept it in the interim.
             */
            identifier: string;
        }
        /**
         * The user or group that the permission applies to.
         */
        export interface PermissionSubjectWithGroupId {
            type: "user" | "group";
            /**
             * for `type=user`, identifier should be user's accountId or `anonymous` for anonymous users
             *
             * for `type=group`, identifier should be ID of the group
             */
            identifier: string;
        }
        /**
         * The value of the property. This can be empty or a complex object. 64KB Size Limit
         * For example,
         * ```
         * "value": {
         *   "example1": "value",
         *   "example2": true,
         *   "example3": 123,
         *   "example4": ["value1", "value2"],
         * }
         * ```
         */
        export type PropertyValue = /**
         * The value of the property. This can be empty or a complex object. 64KB Size Limit
         * For example,
         * ```
         * "value": {
         *   "example1": "value",
         *   "example2": true,
         *   "example3": 123,
         *   "example4": ["value1", "value2"],
         * }
         * ```
         */
        string[] | boolean | {
            [name: string]: any;
        } | string;
        export interface Relation {
            name: string;
            relationData?: RelationData;
            source?: /* Base object for all content types. */ Content | User | Space;
            target?: /* Base object for all content types. */ Content | User | Space;
            _expandable?: {
                relationData?: string;
                source?: string;
                target?: string;
            };
            _links: GenericLinks;
        }
        export interface RelationArray {
            results: Relation[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        export interface RelationData {
            createdBy?: User;
            createdDate?: string; // date-time
            friendlyCreatedDate?: string;
        }
        export interface RetentionPeriod {
            /**
             * The number of units for the retention period.
             */
            number: number; // int32
            /**
             * The unit of time that the retention period is measured in.
             */
            units: "NANOS" | "MICROS" | "MILLIS" | "SECONDS" | "MINUTES" | "HOURS" | "HALF_DAYS" | "DAYS" | "WEEKS" | "MONTHS" | "YEARS" | "DECADES" | "CENTURIES" | "MILLENNIA" | "ERAS" | "FOREVER";
        }
        export interface ScreenLookAndFeel {
            background: string;
            backgroundAttachment?: string | null;
            backgroundBlendMode?: string | null;
            backgroundClip?: string | null;
            backgroundColor?: string | null;
            backgroundImage?: string | null;
            backgroundOrigin?: string | null;
            backgroundPosition?: string | null;
            backgroundRepeat?: string | null;
            backgroundSize?: string | null;
            layer?: {
                width?: string;
                height?: string;
            } | null;
            gutterTop?: string | null;
            gutterRight?: string | null;
            gutterBottom?: string | null;
            gutterLeft?: string | null;
        }
        export type SearchFieldLookAndFeel = {
            backgroundColor: string;
            color: string;
        } | null;
        export interface SearchPageResponseSearchResult {
            results: SearchResult[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            totalSize: number; // int32
            cqlQuery: string;
            searchDuration: number; // int32
            archivedResultCount?: number; // int32
            _links: GenericLinks;
        }
        export interface SearchResult {
            content?: /* Base object for all content types. */ Content;
            user?: User;
            space?: Space;
            title: string;
            excerpt: string;
            url: string;
            resultParentContainer?: ContainerSummary;
            resultGlobalContainer?: ContainerSummary;
            breadcrumbs: Breadcrumb[];
            entityType: string;
            iconCssClass: string;
            lastModified: string; // date-time
            friendlyLastModified?: string;
            score?: number;
        }
        export type Space = {
            id?: number; // int64
            key: string;
            name: string;
            icon?: /* This object represents an icon. If used as a profilePicture, this may be returned as null, depending on the user's privacy setting. */ Icon;
            description?: {
                plain?: SpaceDescription;
                view?: SpaceDescription;
                _expandable?: {
                    view?: string;
                    plain?: string;
                };
            };
            homepage?: /* Base object for all content types. */ Content;
            type: string;
            metadata?: {
                labels?: LabelArray;
                _expandable?: {
                    [key: string]: any;
                };
            };
            operations?: /* An operation and the target entity that it applies to, e.g. create page. */ OperationCheckResult[];
            permissions?: /**
             * This object represents a permission for given space. Permissions consist of
             * at least one operation object with an accompanying subjects object.
             *
             * The following combinations of `operation` and `targetType` values are
             * valid for the `operation` object:
             *
             *   - 'create': 'page', 'blogpost', 'comment', 'attachment'
             *   - 'read': 'space'
             *   - 'delete': 'page', 'blogpost', 'comment', 'attachment'
             *   - 'export': 'space'
             *   - 'administer': 'space'
             */
            SpacePermission[];
            status: string;
            settings?: SpaceSettings;
            theme?: Theme;
            lookAndFeel?: LookAndFeel;
            history?: {
                createdDate: string; // date-time
                createdBy?: User;
            };
            _expandable: {
                settings?: string;
                metadata?: string;
                operations?: string;
                lookAndFeel?: string;
                permissions?: string;
                icon?: string;
                description?: string;
                theme?: string;
                history?: string;
                homepage?: string;
                identifiers?: string;
            };
            _links: GenericLinks;
        } | null;
        export interface SpaceArray {
            results: Space[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        /**
         * This is the request object used when creating a new space.
         */
        export interface SpaceCreate {
            [name: string]: any;
            /**
             * The key for the new space. Format: See [Space
             * keys](https://confluence.atlassian.com/x/lqNMMQ).
             */
            key: string;
            /**
             * The name of the new space.
             */
            name: string;
            description?: /**
             * The description of the new/updated space. Note, only the 'plain' representation
             * can be used for the description when creating or updating a space.
             */
            SpaceDescriptionCreate;
            /**
             * The permissions for the new space. If no permissions are provided, the
             * [Confluence default space permissions](https://confluence.atlassian.com/x/UAgzKw#CreateaSpace-Spacepermissions)
             * are applied. Note that if permissions are provided, the space is
             * created with only the provided set of permissions, not
             * including the default space permissions. Space permissions
             * can be modified after creation using the space permissions
             * endpoints, and a private space can be created using the
             * create private space endpoint.
             */
            permissions?: /**
             * This object represents a permission for given space. Permissions consist of
             * at least one operation object with an accompanying subjects object.
             *
             * The following combinations of `operation` and `targetType` values are
             * valid for the `operation` object:
             *
             *   - 'create': 'page', 'blogpost', 'comment', 'attachment'
             *   - 'read': 'space'
             *   - 'delete': 'page', 'blogpost', 'comment', 'attachment'
             *   - 'export': 'space'
             *   - 'administer': 'space'
             */
            SpacePermissionCreate[] | null;
        }
        export interface SpaceDescription {
            [name: string]: any;
            value: string;
            representation: "plain" | "view";
            embeddedContent: {
                [key: string]: any;
            }[];
        }
        /**
         * The description of the new/updated space. Note, only the 'plain' representation
         * can be used for the description when creating or updating a space.
         */
        export type SpaceDescriptionCreate = {
            plain: {
                [name: string]: any;
                /**
                 * The space description.
                 */
                value?: string;
                /**
                 * Set to 'plain'.
                 */
                representation?: string;
            };
        } | null;
        /**
         * This object represents a permission for given space. Permissions consist of
         * at least one operation object with an accompanying subjects object.
         *
         * The following combinations of `operation` and `targetType` values are
         * valid for the `operation` object:
         *
         *   - 'create': 'page', 'blogpost', 'comment', 'attachment'
         *   - 'read': 'space'
         *   - 'delete': 'page', 'blogpost', 'comment', 'attachment'
         *   - 'export': 'space'
         *   - 'administer': 'space'
         */
        export interface SpacePermission {
            id?: number; // int64
            /**
             * The users and/or groups that the permission applies to.
             */
            subjects?: {
                user?: {
                    results: User[];
                    size: number; // int32
                    start?: number; // int32
                    limit?: number; // int32
                };
                group?: {
                    results: Group[];
                    size: number; // int32
                    start?: number; // int32
                    limit?: number; // int32
                };
                _expandable?: {
                    user?: string;
                    group?: string;
                };
            };
            operation: /* An operation and the target entity that it applies to, e.g. create page. */ OperationCheckResult;
            /**
             * Grant anonymous users permission to use the operation.
             */
            anonymousAccess: boolean;
            /**
             * Grants access to unlicensed users from JIRA Service Desk when used
             * with the 'read space' operation.
             */
            unlicensedAccess: boolean;
        }
        /**
         * This object represents a permission for given space. Permissions consist of
         * at least one operation object with an accompanying subjects object.
         *
         * The following combinations of `operation` and `targetType` values are
         * valid for the `operation` object:
         *
         *   - 'create': 'page', 'blogpost', 'comment', 'attachment'
         *   - 'read': 'space'
         *   - 'delete': 'page', 'blogpost', 'comment', 'attachment'
         *   - 'export': 'space'
         *   - 'administer': 'space'
         */
        export interface SpacePermissionCreate {
            [name: string]: any;
            /**
             * The users and/or groups that the permission applies to.
             */
            subjects?: {
                user?: {
                    results: User[];
                    size: number; // int32
                };
                group?: {
                    results: /* The name property will soon be deprecated in favor of using id. */ GroupCreate[];
                    size: number; // int32
                };
            };
            operation: /* An operation and the target entity that it applies to, e.g. create page. */ OperationCheckResult;
            /**
             * Grant anonymous users permission to use the operation.
             */
            anonymousAccess: boolean;
            /**
             * Grants access to unlicensed users from JIRA Service Desk when used
             * with the 'read space' operation.
             */
            unlicensedAccess: boolean;
        }
        /**
         * This object represents a list of space permissions for custom content type for an individual user. Permissions consist of
         * a subjects object and a list with at least one operation object.
         */
        export interface SpacePermissionCustomContent {
            subject: /* The user or group that the permission applies to. */ PermissionSubject;
            operations: {
                /**
                 * The operation type
                 */
                key: "read" | "create" | "delete";
                /**
                 * The custom content type
                 */
                target: string;
                /**
                 * Grant or restrict access
                 */
                access: boolean;
            }[];
        }
        /**
         * This object represents the request for the single space permission. Permissions consist of
         * one operation object with an accompanying subjects object.
         *
         * The following combinations of `operation.key` and `operation.target` values are
         * valid for the `operation` object:
         * ``` bash
         * 'create': 'page', 'blogpost', 'comment', 'attachment'
         * 'read': 'space'
         * 'delete': 'page', 'blogpost', 'comment', 'attachment', 'space'
         * 'export': 'space'
         * 'administer': 'space'
         * 'archive': 'page'
         * 'restrict_content': 'space'
         * ```
         *
         * For example, to enable Delete Own permission, set the `operation` object to the following:
         * ```
         * "operation": {
         *     "key": "delete",
         *     "target": "space"
         * }
         * ```
         * To enable Add/Delete Restrictions permissions, set the `operation` object to the following:
         * ```
         * "operation": {
         *     "key": "restrict_content",
         *     "target": "space"
         * }
         * ```
         */
        export interface SpacePermissionRequest {
            [name: string]: any;
            subject: /* The user or group that the permission applies to. */ PermissionSubject;
            operation: {
                key: "administer" | "archive" | "copy" | "create" | "delete" | "export" | "move" | "purge" | "purge_version" | "read" | "restore" | "restrict_content" | "update" | "use";
                /**
                 * The space or content type that the operation applies to.
                 */
                target: "page" | "blogpost" | "comment" | "attachment" | "space";
            };
            _links?: GenericLinks;
        }
        /**
         * This object represents a single space permission. Permissions consist of
         * at least one operation object with an accompanying subjects object.
         *
         * The following combinations of `operation.key` and `operation.target` values are
         * valid for the `operation` object:
         * ``` bash
         * 'create': 'page', 'blogpost', 'comment', 'attachment'
         * 'read': 'space'
         * 'delete': 'page', 'blogpost', 'comment', 'attachment', 'space'
         * 'export': 'space'
         * 'administer': 'space'
         * 'archive': 'page'
         * 'restrict_content': 'space'
         * ```
         *
         * For example, to enable Delete Own permission, set the `operation` object to the following:
         * ```
         * "operation": {
         *     "key": "delete",
         *     "target": "space"
         * }
         * ```
         * To enable Add/Delete Restrictions permissions, set the `operation` object to the following:
         * ```
         * "operation": {
         *     "key": "restrict_content",
         *     "target": "space"
         * }
         * ```
         */
        export interface SpacePermissionV2 {
            id: number; // int64
            subject: /* The user or group that the permission applies to. */ PermissionSubject;
            operation: {
                key: "administer" | "archive" | "copy" | "create" | "delete" | "export" | "move" | "purge" | "purge_version" | "read" | "restore" | "restrict_content" | "update" | "use";
                /**
                 * The space or content type that the operation applies to.
                 */
                target: "page" | "blogpost" | "comment" | "attachment" | "space";
            };
            _links?: GenericLinks;
        }
        export interface SpaceProperty {
            id: string;
            key: string;
            value: string[] | boolean | {
                [name: string]: any;
            } | string;
            version?: Version;
            space?: Space;
            _links?: GenericLinks;
            _expandable: {
                version?: string;
                space?: string;
            };
        }
        export interface SpacePropertyArray {
            results: SpaceProperty[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        export interface SpacePropertyCreate {
            /**
             * The key of the new property.
             */
            key: string;
            value: /**
             * The value of the property. This can be empty or a complex object. 64KB Size Limit
             * For example,
             * ```
             * "value": {
             *   "example1": "value",
             *   "example2": true,
             *   "example3": 123,
             *   "example4": ["value1", "value2"],
             * }
             * ```
             */
            PropertyValue;
            space?: {
                [name: string]: any;
                /**
                 * The key of the space
                 */
                key?: string;
            };
        }
        export interface SpacePropertyCreateNoKey {
            value: /**
             * The value of the property. This can be empty or a complex object. 64KB Size Limit
             * For example,
             * ```
             * "value": {
             *   "example1": "value",
             *   "example2": true,
             *   "example3": 123,
             *   "example4": ["value1", "value2"],
             * }
             * ```
             */
            PropertyValue;
        }
        export interface SpacePropertyUpdate {
            key?: string;
            value: string[] | boolean | {
                [name: string]: any;
            } | string;
            version: Version;
            space?: {
                [name: string]: any;
                /**
                 * The key of the space
                 */
                key?: string;
            };
        }
        export type SpaceSettings = {
            /**
             * Defines whether an override for the space home should be used. This is
             * used in conjunction with a space theme provided by an app. For
             * example, if this property is set to true, a theme can display a page
             * other than the space homepage when users visit the root URL for a
             * space. This property allows apps to provide content-only theming
             * without overriding the space home.
             */
            routeOverrideEnabled: boolean;
            editor?: {
                page: string;
                blogpost: string;
                default: string;
            };
            spaceKey?: string;
            _links: GenericLinks;
        } | null;
        export interface SpaceSettingsUpdate {
            /**
             * Defines whether an override for the space home should be used. This is
             * used in conjunction with a space theme provided by an app. For
             * example, if this property is set to true, a theme can display a page
             * other than the space homepage when users visit the root URL for a
             * space. This property allows apps to provide content-only theming
             * without overriding the space home.
             */
            routeOverrideEnabled?: boolean;
        }
        /**
         * The properties of a space that can be updated.
         */
        export interface SpaceUpdate {
            [name: string]: any;
            /**
             * The updated name of the space.
             */
            name?: string | null;
            description?: /**
             * The description of the new/updated space. Note, only the 'plain' representation
             * can be used for the description when creating or updating a space.
             */
            SpaceDescriptionCreate;
            /**
             * The updated homepage for this space
             */
            homepage?: {
                [key: string]: any;
            } | null;
            /**
             * The updated type for this space.
             */
            type?: string;
            /**
             * The updated status for this space.
             */
            status?: string | null;
        }
        export interface SpaceWatch {
            type: string;
            watcher: /**
             * This essentially the same as the `User` object, but no `_links` property and
             * no `_expandable` property (therefore, different required fields).
             */
            WatchUser;
            spaceKey?: string;
            labelName?: string;
            prefix?: string;
        }
        export interface SpaceWatchArray {
            results: SpaceWatch[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links?: GenericLinks;
        }
        export interface SuperBatchWebResources {
            uris?: {
                all?: string[] | string;
                css?: string[] | string;
                js?: string[] | string;
            };
            tags?: {
                all?: string;
                css?: string;
                data?: string;
                js?: string;
            };
            metatags?: string;
            _expandable?: {
                [name: string]: any;
            };
        }
        export type SystemInfoEntity = {
            cloudId: string;
            commitHash: string;
            baseUrl?: string;
            edition?: string;
            siteTitle?: string;
            defaultLocale?: string;
            defaultTimeZone?: string;
        } | null;
        export interface Task {
            globalId: number; // int64
            id: number; // int64
            contentId: number; // int64
            status: string;
            title?: string;
            description?: string;
            body?: string;
            creator: string;
            assignee?: string;
            completeUser?: string;
            createDate: number; // int64
            dueDate?: number; // int64
            updateDate?: number; // int64
            completeDate?: number; // int64
            _links?: GenericLinks;
        }
        export interface TaskPageResponse {
            results: Task[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
        }
        export interface TaskStatusUpdate {
            status: "complete" | "incomplete";
        }
        export interface Theme {
            themeKey: string;
            name?: string;
            description?: string;
            icon?: /* This object represents an icon. If used as a profilePicture, this may be returned as null, depending on the user's privacy setting. */ Icon;
            _links?: GenericLinks;
        }
        export interface ThemeArray {
            results: /* Theme object without links. Used in ThemeArray. */ ThemeNoLinks[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        /**
         * Theme object without links. Used in ThemeArray.
         */
        export interface ThemeNoLinks {
            themeKey: string;
            name?: string;
            description?: string;
            icon?: /* This object represents an icon. If used as a profilePicture, this may be returned as null, depending on the user's privacy setting. */ Icon;
        }
        export interface ThemeUpdate {
            /**
             * The key of the theme to be set as the space theme.
             */
            themeKey: string;
        }
        export interface TopNavigationLookAndFeel {
            color?: string | null;
            highlightColor: string;
            hoverOrFocus?: {
                backgroundColor?: string;
                color?: string;
            };
        }
        export type User = {
            [name: string]: any;
            type: "known" | "unknown" | "anonymous" | "user";
            username?: /**
             * This property is no longer available and will be removed from the documentation soon.
             * Use `accountId` instead.
             * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
             */
            GenericUserName;
            userKey?: /**
             * This property is no longer available and will be removed from the documentation soon.
             * Use `accountId` instead.
             * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
             */
            GenericUserKey;
            accountId?: /**
             * The account ID of the user, which uniquely identifies the user across all Atlassian products.
             * For example, `384093:32b4d9w0-f6a5-3535-11a3-9c8c88d10192`.
             */
            GenericAccountId;
            /**
             * The account type of the user, may return empty string if unavailable. App is if the user is a bot user created on behalf of an Atlassian app.
             */
            accountType?: "atlassian" | "app" | "";
            /**
             * The email address of the user. Depending on the user's privacy setting, this may return an empty string.
             */
            email?: string | null;
            /**
             * The public name or nickname of the user. Will always contain a value.
             */
            publicName?: string;
            profilePicture?: /* This object represents an icon. If used as a profilePicture, this may be returned as null, depending on the user's privacy setting. */ Icon;
            /**
             * The displays name of the user. Depending on the user's privacy setting, this may be the same as publicName.
             */
            displayName?: string | null;
            /**
             * This displays user time zone. Depending on the user's privacy setting, this may return null.
             */
            timeZone?: string | null;
            /**
             * Whether the user is an external collaborator user
             */
            isExternalCollaborator?: boolean;
            /**
             * Whether the user is an external collaborator user
             */
            externalCollaborator?: boolean;
            operations?: /* An operation and the target entity that it applies to, e.g. create page. */ OperationCheckResult[] | null;
            details?: UserDetails;
            personalSpace?: Space;
            _expandable?: {
                operations?: string;
                details?: string;
                personalSpace?: string;
            };
            _links?: GenericLinks;
        } | null;
        export interface UserAnonymous {
            type: string;
            profilePicture: /* This object represents an icon. If used as a profilePicture, this may be returned as null, depending on the user's privacy setting. */ Icon;
            displayName: string;
            operations?: /* An operation and the target entity that it applies to, e.g. create page. */ OperationCheckResult[];
            /**
             * Whether the user is an external collaborator user
             */
            isExternalCollaborator?: boolean;
            _expandable?: {
                operations?: string;
            };
            _links: GenericLinks;
        }
        export interface UserArray {
            results: User[];
            start?: number; // int32
            limit?: number; // int32
            size?: number; // int32
            /**
             * This property will return total count of the objects before pagination is applied.
             * This value is returned if `shouldReturnTotalSize` is set to `true`.
             */
            totalSize?: number; // int64
            _links?: GenericLinks;
        }
        export interface UserDetails {
            business?: {
                /**
                 * This property has been deprecated due to privacy changes. There is no replacement. See the
                 * [migration guide](https://developer.atlassian.com/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/)
                 * for details.
                 */
                position?: string;
                /**
                 * This property has been deprecated due to privacy changes. There is no replacement. See the
                 * [migration guide](https://developer.atlassian.com/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/)
                 * for details.
                 */
                department?: string;
                /**
                 * This property has been deprecated due to privacy changes. There is no replacement. See the
                 * [migration guide](https://developer.atlassian.com/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/)
                 * for details.
                 */
                location?: string;
            };
            personal?: {
                /**
                 * This property has been deprecated due to privacy changes. There is no replacement. See the
                 * [migration guide](https://developer.atlassian.com/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/)
                 * for details.
                 */
                phone?: string;
                /**
                 * This property has been deprecated due to privacy changes. There is no replacement. See the
                 * [migration guide](https://developer.atlassian.com/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/)
                 * for details.
                 */
                im?: string;
                /**
                 * This property has been deprecated due to privacy changes. There is no replacement. See the
                 * [migration guide](https://developer.atlassian.com/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/)
                 * for details.
                 */
                website?: string;
                /**
                 * This property has been deprecated due to privacy changes. Use the `User.email` property instead. See the
                 * [migration guide](https://developer.atlassian.com/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/)
                 * for details.
                 */
                email?: string;
            };
        }
        export interface UserProperty {
            key: string;
            /**
             * The value of the content property.
             */
            value: {
                [name: string]: any;
            };
            /**
             * a unique identifier for the user property
             */
            id: string;
            /**
             * datetime when the property was last modified such as `2022-02-01T12:00:00.111Z`
             */
            lastModifiedDate: string; // date-time
            /**
             * datetime when the property was created such as `2022-01-01T12:00:00.111Z`
             */
            createdDate: string; // date-time
            _links?: GenericLinks;
        }
        export interface UserPropertyCreate {
            /**
             * The value of the user property.
             */
            value: {
                [name: string]: any;
            };
        }
        export interface UserPropertyKeyArray {
            results: {
                key?: string;
            }[];
            start?: number; // int32
            limit?: number; // int32
            size?: number; // int32
            _links?: GenericLinks;
        }
        export interface UserPropertyUpdate {
            /**
             * The value of the user property.
             */
            value: {
                [name: string]: any;
            };
        }
        export interface UserWatch {
            watching: boolean;
        }
        export type UsersUserKeys = {
            [name: string]: any;
            users?: User[];
            userKeys?: string[];
            _links?: GenericLinks;
        } | null;
        export type Version = {
            [name: string]: any;
            by?: User;
            when: string | null; // date-time
            friendlyWhen?: string | null;
            message?: string | null;
            /**
             * Set this to the current version number incremented by one
             */
            number: number; // int32
            /**
             * If `minorEdit` is set to 'true', no notification email or activity
             * stream will be generated for the change.
             */
            minorEdit: boolean;
            content?: /* Base object for all content types. */ Content;
            collaborators?: UsersUserKeys;
            _expandable?: {
                content?: string;
                collaborators?: string;
            };
            _links?: GenericLinks;
            /**
             * True if content type is modifed in this version (e.g. page to blog)
             */
            contentTypeModified?: boolean;
            /**
             * The revision id provided by confluence to be used as a revision in Synchrony
             */
            confRev?: string | null;
            /**
             * The revision id provided by Synchrony
             */
            syncRev?: string | null;
            /**
             * Source of the synchrony revision
             */
            syncRevSource?: string | null;
        } | null;
        export interface VersionArray {
            results: Version[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        export interface VersionRestore {
            /**
             * Set to 'restore'.
             */
            operationKey: "restore";
            params: {
                /**
                 * The version number to be restored.
                 */
                versionNumber: number; // int32
                /**
                 * Description for the version.
                 */
                message: string;
                /**
                 * If true, the content title will be the same as the title from the version restored. Defaults to `false`.
                 */
                restoreTitle?: boolean;
            };
        }
        export interface Watch {
            type: string;
            watcher: /**
             * This essentially the same as the `User` object, but no `_links` property and
             * no `_expandable` property (therefore, different required fields).
             */
            WatchUser;
            contentId: number; // int64
        }
        export interface WatchArray {
            results: Watch[];
            start: number; // int32
            limit: number; // int32
            size: number; // int32
            _links: GenericLinks;
        }
        /**
         * This essentially the same as the `User` object, but no `_links` property and
         * no `_expandable` property (therefore, different required fields).
         */
        export interface WatchUser {
            type: string;
            username?: /**
             * This property is no longer available and will be removed from the documentation soon.
             * Use `accountId` instead.
             * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
             */
            GenericUserName;
            userKey?: /**
             * This property is no longer available and will be removed from the documentation soon.
             * Use `accountId` instead.
             * See the [deprecation notice](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/) for details.
             */
            GenericUserKey;
            accountId: /**
             * The account ID of the user, which uniquely identifies the user across all Atlassian products.
             * For example, `384093:32b4d9w0-f6a5-3535-11a3-9c8c88d10192`.
             */
            GenericAccountId;
            profilePicture: /* This object represents an icon. If used as a profilePicture, this may be returned as null, depending on the user's privacy setting. */ Icon;
            displayName: string;
            timeZone?: string | null;
            operations: /* An operation and the target entity that it applies to, e.g. create page. */ OperationCheckResult[] | null;
            isExternalCollaborator: boolean;
            details?: UserDetails;
            accountType: string;
            email: string;
            publicName: string;
            personalSpace: {
                [key: string]: any;
            } | null;
            externalCollaborator: boolean;
        }
        export interface WebResourceDependencies {
            _expandable?: {
                [name: string]: any;
                uris?: string | {
                    [name: string]: any;
                };
            };
            keys?: string[];
            contexts?: string[];
            uris?: {
                all?: string[] | string;
                css?: string[] | string;
                js?: string[] | string;
                _expandable?: {
                    [name: string]: any;
                    css?: string[] | string;
                    js?: string[] | string;
                };
            };
            tags?: {
                all?: string;
                css?: string;
                data?: string;
                js?: string;
                _expandable?: {
                    [name: string]: any;
                };
            };
            superbatch?: SuperBatchWebResources;
        }
    }
}
export declare namespace Paths {
    namespace AddContentWatcher {
        namespace Parameters {
            export type AccountId = string;
            export type ContentId = string;
            export type Key = string;
            export type Username = string;
        }
        export interface PathParameters {
            contentId: Parameters.ContentId;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace AddCustomContentPermissions {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export type RequestBody = /**
         * This object represents a list of space permissions for custom content type for an individual user. Permissions consist of
         * a subjects object and a list with at least one operation object.
         */
        Components.Schemas.SpacePermissionCustomContent;
    }
    namespace AddGroupToContentRestriction {
        namespace Parameters {
            export type GroupName = string;
            export type Id = string;
            export type OperationKey = "read" | "update";
        }
        export interface PathParameters {
            id: Parameters.Id;
            operationKey: Parameters.OperationKey;
            groupName: Parameters.GroupName;
        }
    }
    namespace AddGroupToContentRestrictionByGroupId {
        namespace Parameters {
            export type GroupId = string;
            export type Id = string;
            export type OperationKey = "read" | "update";
        }
        export interface PathParameters {
            id: Parameters.Id;
            operationKey: Parameters.OperationKey;
            groupId: Parameters.GroupId;
        }
    }
    namespace AddLabelWatcher {
        export interface HeaderParameters {
            "X-Atlassian-Token": Parameters.XAtlassianToken;
        }
        namespace Parameters {
            export type AccountId = string;
            export type Key = string;
            export type LabelName = string;
            export type Username = string;
            export type XAtlassianToken = string;
        }
        export interface PathParameters {
            labelName: Parameters.LabelName;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace AddLabelsToContent {
        namespace Parameters {
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export type RequestBody = Components.Schemas.LabelCreateArray | Components.Schemas.LabelCreate;
        namespace Responses {
            export type $200 = Components.Schemas.LabelArray;
        }
    }
    namespace AddLabelsToSpace {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export type RequestBody = Components.Schemas.LabelCreate[];
        namespace Responses {
            export type $200 = Components.Schemas.LabelArray;
        }
    }
    namespace AddPermissionToSpace {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export type RequestBody = /**
         * This object represents the request for the single space permission. Permissions consist of
         * one operation object with an accompanying subjects object.
         *
         * The following combinations of `operation.key` and `operation.target` values are
         * valid for the `operation` object:
         * ``` bash
         * 'create': 'page', 'blogpost', 'comment', 'attachment'
         * 'read': 'space'
         * 'delete': 'page', 'blogpost', 'comment', 'attachment', 'space'
         * 'export': 'space'
         * 'administer': 'space'
         * 'archive': 'page'
         * 'restrict_content': 'space'
         * ```
         *
         * For example, to enable Delete Own permission, set the `operation` object to the following:
         * ```
         * "operation": {
         *     "key": "delete",
         *     "target": "space"
         * }
         * ```
         * To enable Add/Delete Restrictions permissions, set the `operation` object to the following:
         * ```
         * "operation": {
         *     "key": "restrict_content",
         *     "target": "space"
         * }
         * ```
         */
        Components.Schemas.SpacePermissionRequest;
        namespace Responses {
            export type $200 = /**
             * This object represents a single space permission. Permissions consist of
             * at least one operation object with an accompanying subjects object.
             *
             * The following combinations of `operation.key` and `operation.target` values are
             * valid for the `operation` object:
             * ``` bash
             * 'create': 'page', 'blogpost', 'comment', 'attachment'
             * 'read': 'space'
             * 'delete': 'page', 'blogpost', 'comment', 'attachment', 'space'
             * 'export': 'space'
             * 'administer': 'space'
             * 'archive': 'page'
             * 'restrict_content': 'space'
             * ```
             *
             * For example, to enable Delete Own permission, set the `operation` object to the following:
             * ```
             * "operation": {
             *     "key": "delete",
             *     "target": "space"
             * }
             * ```
             * To enable Add/Delete Restrictions permissions, set the `operation` object to the following:
             * ```
             * "operation": {
             *     "key": "restrict_content",
             *     "target": "space"
             * }
             * ```
             */
            Components.Schemas.SpacePermissionV2;
        }
    }
    namespace AddRestrictions {
        namespace Parameters {
            export type Expand = ("restrictions.user" | "read.restrictions.user" | "update.restrictions.user" | "restrictions.group" | "read.restrictions.group" | "update.restrictions.group" | "content")[];
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        export type RequestBody = Components.Schemas.ContentRestrictionAddOrUpdateArray;
        namespace Responses {
            export type $200 = Components.Schemas.ContentRestrictionArray;
        }
    }
    namespace AddSpaceWatcher {
        export interface HeaderParameters {
            "X-Atlassian-Token": Parameters.XAtlassianToken;
        }
        namespace Parameters {
            export type AccountId = string;
            export type Key = string;
            export type SpaceKey = string;
            export type Username = string;
            export type XAtlassianToken = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace AddUserToContentRestriction {
        namespace Parameters {
            export type AccountId = string;
            export type Id = string;
            export type Key = string;
            export type OperationKey = string;
            export type Username = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
            operationKey: Parameters.OperationKey;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace AddUserToGroup {
        namespace Parameters {
            export type Name = string;
        }
        export interface QueryParameters {
            name: Parameters.Name;
        }
        export type RequestBody = Components.Schemas.AccountId;
    }
    namespace AddUserToGroupByGroupId {
        namespace Parameters {
            export type GroupId = string;
        }
        export interface QueryParameters {
            groupId: Parameters.GroupId;
        }
        export type RequestBody = Components.Schemas.AccountId;
    }
    namespace ArchivePages {
        export interface RequestBody {
            pages?: {
                /**
                 * The `id` of the page to be archived.
                 */
                id: number; // int64
            }[];
        }
        namespace Responses {
            export type $202 = Components.Schemas.LongTask;
        }
    }
    namespace AsyncConvertContentBodyRequest {
        namespace Parameters {
            export type AllowCache = boolean;
            export type ContentIdContext = string;
            export type EmbeddedContentRender = "current" | "version-at-save";
            export type Expand = string[];
            export type SpaceKeyContext = string;
            export type To = "export_view";
        }
        export interface PathParameters {
            to: Parameters.To;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            spaceKeyContext?: Parameters.SpaceKeyContext;
            contentIdContext?: Parameters.ContentIdContext;
            allowCache?: Parameters.AllowCache;
            embeddedContentRender?: Parameters.EmbeddedContentRender;
        }
        export type RequestBody = /* This object is used when creating or updating content. */ Components.Schemas.ContentBodyCreate;
        namespace Responses {
            export type $200 = Components.Schemas.AsyncId;
        }
    }
    namespace AsyncConvertContentBodyResponse {
        namespace Parameters {
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        namespace Responses {
            export type $200 = Components.Schemas.AsyncContentBody;
        }
    }
    namespace BulkRemoveContentStates {
        namespace Parameters {
            export type Status = "current" | "draft";
        }
        export interface QueryParameters {
            status: Parameters.Status;
        }
        export interface RequestBody {
            ids: Components.Schemas.ContentId[];
        }
        namespace Responses {
            export type $200 = Components.Schemas.AsyncId;
            export interface $400 {
            }
            export interface $401 {
            }
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace BulkSetContentStates {
        namespace Parameters {
            export type Status = "current" | "draft";
        }
        export interface QueryParameters {
            status: Parameters.Status;
        }
        export type RequestBody = Components.Schemas.BulkContentStateSetInput;
        namespace Responses {
            export type $200 = Components.Schemas.AsyncId;
            export interface $400 {
            }
            export interface $404 {
            }
        }
    }
    namespace CheckContentPermission {
        namespace Parameters {
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export type RequestBody = /* This object represents the request for the content permission check API. */ Components.Schemas.ContentPermissionRequest;
        namespace Responses {
            export type $200 = /**
             * This object represents the response for the content permission check API. If the user or group does not have
             * permissions, the following errors may be returned:
             *
             * - Group does not have permission to the space
             * - Group does not have permission to the content
             * - User is not allowed to use Confluence
             * - User does not have permission to the space
             * - User does not have permission to the content
             * - Anonymous users are not allowed to use Confluence
             * - Anonymous user does not have permission to the space
             * - Anonymous user does not have permission to the content
             */
            Components.Schemas.PermissionCheckResponse;
        }
    }
    namespace ConvertContentBody {
        namespace Parameters {
            export type ContentIdContext = string;
            export type EmbeddedContentRender = "current" | "version-at-save";
            export type Expand = string[];
            export type SpaceKeyContext = string;
            export type To = string;
        }
        export interface PathParameters {
            to: Parameters.To;
        }
        export interface QueryParameters {
            spaceKeyContext?: Parameters.SpaceKeyContext;
            contentIdContext?: Parameters.ContentIdContext;
            embeddedContentRender?: Parameters.EmbeddedContentRender;
            expand?: Parameters.Expand;
        }
        export type RequestBody = /* This object is used when creating or updating content. */ Components.Schemas.ContentBodyCreate;
        namespace Responses {
            export type $200 = Components.Schemas.ContentBody;
        }
    }
    namespace CopyPage {
        namespace Parameters {
            export type Expand = string[];
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        export type RequestBody = Components.Schemas.CopyPageRequest;
        namespace Responses {
            export type $200 = /* Base object for all content types. */ Components.Schemas.Content;
        }
    }
    namespace CopyPageHierarchy {
        namespace Parameters {
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export type RequestBody = Components.Schemas.CopyPageHierarchyRequest;
        namespace Responses {
            export type $202 = Components.Schemas.LongTask;
        }
    }
    namespace CreateAttachment {
        namespace Parameters {
            export type Id = string;
            export type Status = "current" | "draft";
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
        }
        export interface RequestBody {
            /**
             * The relative location and name of the attachment to be added to
             * the content.
             */
            file: string; // binary
            /**
             * The comment for the attachment that is being added.
             * If you specify a comment, then every file must have a comment and
             * the comments must be in the same order as the files. Alternatively,
             * don't specify any comments.
             */
            comment?: string; // binary
            /**
             * If `minorEdits` is set to 'true', no notification email or activity stream
             * will be generated when the attachment is added to the content.
             */
            minorEdit: string; // binary
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentArray;
        }
    }
    namespace CreateAuditRecord {
        export type RequestBody = Components.Schemas.AuditRecordCreate;
        namespace Responses {
            export type $200 = Components.Schemas.AuditRecord;
        }
    }
    namespace CreateContent {
        namespace Parameters {
            export type Expand = string[];
            export type Status = string;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
            expand?: Parameters.Expand;
        }
        export type RequestBody = Components.Schemas.ContentCreate;
        namespace Responses {
            export type $200 = /* Base object for all content types. */ Components.Schemas.Content;
            export interface $404 {
            }
        }
    }
    namespace CreateContentProperty {
        namespace Parameters {
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export type RequestBody = Components.Schemas.ContentPropertyCreate;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace CreateContentPropertyForKey {
        namespace Parameters {
            export type Id = string;
            export type Key = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
            key: Parameters.Key;
        }
        export type RequestBody = Components.Schemas.ContentPropertyCreateNoKey;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace CreateContentTemplate {
        export type RequestBody = /* This object is used to create content templates. */ Components.Schemas.ContentTemplateCreate;
        namespace Responses {
            export type $200 = Components.Schemas.ContentTemplate;
        }
    }
    namespace CreateGroup {
        export type RequestBody = Components.Schemas.GroupName;
        namespace Responses {
            export type $201 = Components.Schemas.Group;
        }
    }
    namespace CreateOrUpdateAttachments {
        namespace Parameters {
            export type Id = string;
            export type Status = "current" | "draft";
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
        }
        export interface RequestBody {
            /**
             * The relative location and name of the attachment to be added to
             * the content.
             */
            file: string; // binary
            /**
             * The comment for the attachment that is being added.
             * If you specify a comment, then every file must have a comment and
             * the comments must be in the same order as the files. Alternatively,
             * don't specify any comments.
             */
            comment?: string; // binary
            /**
             * If `minorEdits` is set to 'true', no notification email or activity stream
             * will be generated when the attachment is added to the content.
             */
            minorEdit: string; // binary
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentArray;
        }
    }
    namespace CreatePrivateSpace {
        export type RequestBody = /* This is the request object used when creating a new space. */ Components.Schemas.SpaceCreate;
        namespace Responses {
            export type $200 = Components.Schemas.Space;
        }
    }
    namespace CreateRelationship {
        namespace Parameters {
            export type RelationName = string;
            export type SourceKey = string;
            export type SourceStatus = string;
            export type SourceType = "user" | "content" | "space";
            export type SourceVersion = number; // int32
            export type TargetKey = string;
            export type TargetStatus = string;
            export type TargetType = "user" | "content" | "space";
            export type TargetVersion = number; // int32
        }
        export interface PathParameters {
            relationName: Parameters.RelationName;
            sourceType: Parameters.SourceType;
            sourceKey: Parameters.SourceKey;
            targetType: Parameters.TargetType;
            targetKey: Parameters.TargetKey;
        }
        export interface QueryParameters {
            sourceStatus?: Parameters.SourceStatus;
            targetStatus?: Parameters.TargetStatus;
            sourceVersion?: Parameters.SourceVersion /* int32 */;
            targetVersion?: Parameters.TargetVersion /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Relation;
        }
    }
    namespace CreateSpace {
        export type RequestBody = /* This is the request object used when creating a new space. */ Components.Schemas.SpaceCreate;
        namespace Responses {
            export type $200 = Components.Schemas.Space;
        }
    }
    namespace CreateSpaceProperty {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export type RequestBody = Components.Schemas.SpacePropertyCreate;
        namespace Responses {
            export type $200 = Components.Schemas.SpaceProperty;
        }
    }
    namespace CreateSpacePropertyForKey {
        namespace Parameters {
            export type Key = string;
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
            key: Parameters.Key;
        }
        export type RequestBody = Components.Schemas.SpacePropertyCreateNoKey;
        namespace Responses {
            export type $200 = Components.Schemas.SpaceProperty;
        }
    }
    namespace CreateUserProperty {
        namespace Parameters {
            export type Key = string; // ^[-_a-zA-Z0-9]+$
            export type UserId = string;
        }
        export interface PathParameters {
            userId: Parameters.UserId;
            key: Parameters.Key /* ^[-_a-zA-Z0-9]+$ */;
        }
        export type RequestBody = Components.Schemas.UserPropertyCreate;
        namespace Responses {
            export interface $201 {
            }
        }
    }
    namespace DeleteContent {
        namespace Parameters {
            export type Id = string;
            export type Status = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
        }
    }
    namespace DeleteContentProperty {
        namespace Parameters {
            export type Id = string;
            export type Key = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
            key: Parameters.Key;
        }
    }
    namespace DeleteContentVersion {
        namespace Parameters {
            export type Id = string;
            export type VersionNumber = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
            versionNumber: Parameters.VersionNumber /* int32 */;
        }
    }
    namespace DeleteLabelFromSpace {
        namespace Parameters {
            export type Name = string;
            export type Prefix = string;
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export interface QueryParameters {
            name: Parameters.Name;
            prefix?: Parameters.Prefix;
        }
    }
    namespace DeletePageTree {
        namespace Parameters {
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        namespace Responses {
            export type $202 = Components.Schemas.LongTask;
        }
    }
    namespace DeleteRelationship {
        namespace Parameters {
            export type RelationName = string;
            export type SourceKey = string;
            export type SourceStatus = string;
            export type SourceType = "user" | "content" | "space";
            export type SourceVersion = number; // int32
            export type TargetKey = string;
            export type TargetStatus = string;
            export type TargetType = "user" | "content" | "space";
            export type TargetVersion = number; // int32
        }
        export interface PathParameters {
            relationName: Parameters.RelationName;
            sourceType: Parameters.SourceType;
            sourceKey: Parameters.SourceKey;
            targetType: Parameters.TargetType;
            targetKey: Parameters.TargetKey;
        }
        export interface QueryParameters {
            sourceStatus?: Parameters.SourceStatus;
            targetStatus?: Parameters.TargetStatus;
            sourceVersion?: Parameters.SourceVersion /* int32 */;
            targetVersion?: Parameters.TargetVersion /* int32 */;
        }
    }
    namespace DeleteRestrictions {
        namespace Parameters {
            export type Expand = ("restrictions.user" | "read.restrictions.user" | "update.restrictions.user" | "restrictions.group" | "read.restrictions.group" | "update.restrictions.group" | "content")[];
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentRestrictionArray;
        }
    }
    namespace DeleteSpace {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        namespace Responses {
            export type $202 = Components.Schemas.LongTask;
        }
    }
    namespace DeleteSpaceProperty {
        namespace Parameters {
            export type Key = string;
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
            key: Parameters.Key;
        }
    }
    namespace DeleteUserProperty {
        namespace Parameters {
            export type Key = string; // ^[-_a-zA-Z0-9]+$
            export type UserId = string;
        }
        export interface PathParameters {
            userId: Parameters.UserId;
            key: Parameters.Key /* ^[-_a-zA-Z0-9]+$ */;
        }
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace DownloadAttatchment {
        namespace Parameters {
            export type AttachmentId = string;
            export type Id = string;
            export type Version = number;
        }
        export interface PathParameters {
            id: Parameters.Id;
            attachmentId: Parameters.AttachmentId;
        }
        export interface QueryParameters {
            version?: Parameters.Version;
        }
        namespace Responses {
            export interface $302 {
            }
            export interface $400 {
            }
            export interface $401 {
            }
            export interface $404 {
            }
        }
    }
    namespace ExportAuditRecords {
        namespace Parameters {
            export type EndDate = string;
            export type Format = "csv" | "zip";
            export type SearchString = string;
            export type StartDate = string;
        }
        export interface QueryParameters {
            startDate?: Parameters.StartDate;
            endDate?: Parameters.EndDate;
            searchString?: Parameters.SearchString;
            format?: Parameters.Format;
        }
        namespace Responses {
            export type $200 = string; // binary
        }
    }
    namespace FindSourcesForTarget {
        namespace Parameters {
            export type Expand = ("relationData" | "source" | "target")[];
            export type Limit = number; // int32
            export type RelationName = string;
            export type SourceStatus = string;
            export type SourceType = "user" | "content" | "space";
            export type SourceVersion = number; // int32
            export type Start = number; // int32
            export type TargetKey = string;
            export type TargetStatus = string;
            export type TargetType = "user" | "content" | "space";
            export type TargetVersion = number; // int32
        }
        export interface PathParameters {
            relationName: Parameters.RelationName;
            sourceType: Parameters.SourceType;
            targetType: Parameters.TargetType;
            targetKey: Parameters.TargetKey;
        }
        export interface QueryParameters {
            sourceStatus?: Parameters.SourceStatus;
            targetStatus?: Parameters.TargetStatus;
            sourceVersion?: Parameters.SourceVersion /* int32 */;
            targetVersion?: Parameters.TargetVersion /* int32 */;
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.RelationArray;
        }
    }
    namespace FindTargetFromSource {
        namespace Parameters {
            export type Expand = ("relationData" | "source" | "target")[];
            export type Limit = number; // int32
            export type RelationName = string;
            export type SourceKey = string;
            export type SourceStatus = string;
            export type SourceType = "user" | "content" | "space";
            export type SourceVersion = number; // int32
            export type Start = number; // int32
            export type TargetStatus = string;
            export type TargetType = "user" | "content" | "space";
            export type TargetVersion = number; // int32
        }
        export interface PathParameters {
            relationName: Parameters.RelationName;
            sourceType: Parameters.SourceType;
            sourceKey: Parameters.SourceKey;
            targetType: Parameters.TargetType;
        }
        export interface QueryParameters {
            sourceStatus?: Parameters.SourceStatus;
            targetStatus?: Parameters.TargetStatus;
            sourceVersion?: Parameters.SourceVersion /* int32 */;
            targetVersion?: Parameters.TargetVersion /* int32 */;
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.RelationArray;
        }
    }
    namespace GetAllLabelContent {
        namespace Parameters {
            export type Limit = number; // int32
            export type Name = string;
            export type Start = number; // int32
            export type Type = "page" | "blogpost" | "attachment" | "page_template";
        }
        export interface QueryParameters {
            name: Parameters.Name;
            type?: Parameters.Type;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.LabelDetails;
        }
    }
    namespace GetAndAsyncConvertMacroBodyByMacroId {
        namespace Parameters {
            export type AllowCache = boolean;
            export type EmbeddedContentRender = "current" | "version-at-save";
            export type Expand = string[];
            export type Id = string;
            export type MacroId = string;
            export type SpaceKeyContext = string;
            export type To = "export_view" | "view" | "styled_view";
            export type Version = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
            version: Parameters.Version /* int32 */;
            macroId: Parameters.MacroId;
            to: Parameters.To;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            allowCache?: Parameters.AllowCache;
            spaceKeyContext?: Parameters.SpaceKeyContext;
            embeddedContentRender?: Parameters.EmbeddedContentRender;
        }
        namespace Responses {
            export type $200 = Components.Schemas.AsyncId;
        }
    }
    namespace GetAndConvertMacroBodyByMacroId {
        namespace Parameters {
            export type EmbeddedContentRender = "current" | "version-at-save";
            export type Expand = string[];
            export type Id = string;
            export type MacroId = string;
            export type SpaceKeyContext = string;
            export type To = string;
            export type Version = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
            version: Parameters.Version /* int32 */;
            macroId: Parameters.MacroId;
            to: Parameters.To;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            spaceKeyContext?: Parameters.SpaceKeyContext;
            embeddedContentRender?: Parameters.EmbeddedContentRender;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentBody;
            export interface $400 {
            }
        }
    }
    namespace GetAnonymousUser {
        namespace Parameters {
            export type Expand = ("operations")[];
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.UserAnonymous;
        }
    }
    namespace GetAttachments {
        namespace Parameters {
            export type Expand = string[];
            export type Filename = string;
            export type Id = string;
            export type Limit = number; // int32
            export type MediaType = string;
            export type Start = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            filename?: Parameters.Filename;
            mediaType?: Parameters.MediaType;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentArray;
        }
    }
    namespace GetAuditRecords {
        namespace Parameters {
            export type EndDate = string;
            export type Limit = number; // int32
            export type SearchString = string;
            export type Start = number; // int32
            export type StartDate = string;
        }
        export interface QueryParameters {
            startDate?: Parameters.StartDate;
            endDate?: Parameters.EndDate;
            searchString?: Parameters.SearchString;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.AuditRecordArray;
        }
    }
    namespace GetAuditRecordsForTimePeriod {
        namespace Parameters {
            export type Limit = number; // int32
            export type Number = number; // int64
            export type SearchString = string;
            export type Start = number; // int32
            export type Units = "NANOS" | "MICROS" | "MILLIS" | "SECONDS" | "MINUTES" | "HOURS" | "HALF_DAYS" | "DAYS" | "WEEKS" | "MONTHS" | "YEARS" | "DECADES" | "CENTURIES";
        }
        export interface QueryParameters {
            number?: Parameters.Number /* int64 */;
            units?: Parameters.Units;
            searchString?: Parameters.SearchString;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.AuditRecordArray;
        }
    }
    namespace GetAvailableContentStates {
        namespace Parameters {
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        namespace Responses {
            export type $200 = Components.Schemas.AvailableContentStates;
            export interface $400 {
            }
            export interface $401 {
            }
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace GetBlueprintTemplates {
        namespace Parameters {
            export type Expand = ("body")[];
            export type Limit = number; // int32
            export type SpaceKey = string;
            export type Start = number; // int32
        }
        export interface QueryParameters {
            spaceKey?: Parameters.SpaceKey;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.BlueprintTemplateArray;
        }
    }
    namespace GetBulkUserLookup {
        namespace Parameters {
            export type AccountId = string;
            export type Expand = ("operations" | "personalSpace" | "isExternalCollaborator")[];
            export type Limit = number; // int32
        }
        export interface QueryParameters {
            accountId: Parameters.AccountId;
            expand?: Parameters.Expand;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.BulkUserLookupArray;
        }
    }
    namespace GetContent {
        namespace Parameters {
            export type Expand = string[];
            export type Limit = number; // int32
            export type Orderby = string;
            export type PostingDay = string;
            export type SpaceKey = string;
            export type Start = number; // int32
            export type Status = string[];
            export type Title = string;
            export type Trigger = "viewed";
            export type Type = string;
        }
        export interface QueryParameters {
            type?: Parameters.Type;
            spaceKey?: Parameters.SpaceKey;
            title?: Parameters.Title;
            status?: Parameters.Status;
            postingDay?: Parameters.PostingDay;
            expand?: Parameters.Expand;
            trigger?: Parameters.Trigger;
            orderby?: Parameters.Orderby;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentArray;
        }
    }
    namespace GetContentById {
        namespace Parameters {
            export type EmbeddedContentRender = "current" | "version-at-save";
            export type Expand = string[];
            export type Id = string;
            export type Status = string[];
            export type Trigger = "viewed";
            export type Version = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
            version?: Parameters.Version /* int32 */;
            embeddedContentRender?: Parameters.EmbeddedContentRender;
            expand?: Parameters.Expand;
            trigger?: Parameters.Trigger;
        }
        namespace Responses {
            export type $200 = /* Base object for all content types. */ Components.Schemas.Content;
        }
    }
    namespace GetContentByTypeForSpace {
        namespace Parameters {
            export type Depth = "all" | "root";
            export type Expand = string[];
            export type Limit = number; // int32
            export type SpaceKey = string;
            export type Start = number; // int32
            export type Type = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
            type: Parameters.Type;
        }
        export interface QueryParameters {
            depth?: Parameters.Depth;
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentArray;
        }
    }
    namespace GetContentChildren {
        namespace Parameters {
            export type Expand = string[];
            export type Id = string;
            export type ParentVersion = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            parentVersion?: Parameters.ParentVersion /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentChildren;
        }
    }
    namespace GetContentChildrenByType {
        namespace Parameters {
            export type Expand = string[];
            export type Id = string;
            export type Limit = number; // int32
            export type ParentVersion = number; // int32
            export type Start = number; // int32
            export type Type = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
            type: Parameters.Type;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            parentVersion?: Parameters.ParentVersion /* int32 */;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentArray;
        }
    }
    namespace GetContentComments {
        namespace Parameters {
            export type Depth = string;
            export type Expand = string[];
            export type Id = string;
            export type Limit = number; // int32
            export type Location = string[];
            export type ParentVersion = number; // int32
            export type Start = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            parentVersion?: Parameters.ParentVersion /* int32 */;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            location?: Parameters.Location;
            depth?: Parameters.Depth;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentArray;
        }
    }
    namespace GetContentDescendants {
        namespace Parameters {
            export type Expand = ("attachment" | "comment" | "page")[];
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentChildren;
        }
    }
    namespace GetContentForSpace {
        namespace Parameters {
            export type Depth = "all" | "root";
            export type Expand = string[];
            export type Limit = number; // int32
            export type SpaceKey = string;
            export type Start = number; // int32
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export interface QueryParameters {
            depth?: Parameters.Depth;
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export interface $200 {
                page?: Components.Schemas.ContentArray;
                blogpost?: Components.Schemas.ContentArray;
                _links?: Components.Schemas.GenericLinks;
            }
        }
    }
    namespace GetContentProperties {
        namespace Parameters {
            export type Expand = ("content" | "version")[];
            export type Id = string;
            export type Key = string[];
            export type Limit = number; // int32
            export type Start = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentPropertyArray;
        }
    }
    namespace GetContentProperty {
        namespace Parameters {
            export type Expand = ("content" | "version")[];
            export type Id = string;
            export type Key = string;
            export type Status = string[];
        }
        export interface PathParameters {
            id: Parameters.Id;
            key: Parameters.Key;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            status?: Parameters.Status;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace GetContentRestrictionStatusForGroup {
        namespace Parameters {
            export type GroupName = string;
            export type Id = string;
            export type OperationKey = "read" | "update";
        }
        export interface PathParameters {
            id: Parameters.Id;
            operationKey: Parameters.OperationKey;
            groupName: Parameters.GroupName;
        }
    }
    namespace GetContentRestrictionStatusForUser {
        namespace Parameters {
            export type AccountId = string;
            export type Id = string;
            export type Key = string;
            export type OperationKey = string;
            export type Username = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
            operationKey: Parameters.OperationKey;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace GetContentState {
        namespace Parameters {
            export type Id = string;
            export type Status = "current" | "draft" | "archived";
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentStateResponse;
            export interface $401 {
            }
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace GetContentStateSettings {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentStateSettings;
            export interface $401 {
            }
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace GetContentTemplate {
        namespace Parameters {
            export type ContentTemplateId = string;
        }
        export interface PathParameters {
            contentTemplateId: Parameters.ContentTemplateId;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentTemplate;
        }
    }
    namespace GetContentTemplates {
        namespace Parameters {
            export type Expand = ("body")[];
            export type Limit = number; // int32
            export type SpaceKey = string;
            export type Start = number; // int32
        }
        export interface QueryParameters {
            spaceKey?: Parameters.SpaceKey;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentTemplateArray;
        }
    }
    namespace GetContentVersion {
        namespace Parameters {
            export type Expand = string[];
            export type Id = string;
            export type VersionNumber = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
            versionNumber: Parameters.VersionNumber /* int32 */;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Version;
        }
    }
    namespace GetContentVersions {
        namespace Parameters {
            export type Expand = string[];
            export type Id = string;
            export type Limit = number; // int32
            export type Start = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.VersionArray;
        }
    }
    namespace GetContentWatchStatus {
        namespace Parameters {
            export type AccountId = string;
            export type ContentId = string;
            export type Key = string;
            export type Username = string;
        }
        export interface PathParameters {
            contentId: Parameters.ContentId;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
        namespace Responses {
            export type $200 = Components.Schemas.UserWatch;
        }
    }
    namespace GetContentsWithState {
        namespace Parameters {
            export type Expand = string[];
            export type Limit = number; // int32
            export type SpaceKey = string;
            export type Start = number; // int32
            export type StateId = number; // int32
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export interface QueryParameters {
            "state-id": Parameters.StateId /* int32 */;
            expand?: Parameters.Expand;
            limit?: Parameters.Limit /* int32 */;
            start?: Parameters.Start /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentArray;
            export interface $400 {
            }
            export interface $401 {
            }
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace GetCurrentUser {
        namespace Parameters {
            export type Expand = ("operations" | "personalSpace" | "isExternalCollaborator")[];
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.User;
        }
    }
    namespace GetCustomContentStates {
        namespace Responses {
            export type $200 = Components.Schemas.ContentState[];
            export interface $401 {
            }
        }
    }
    namespace GetDescendantsOfType {
        namespace Parameters {
            export type Depth = "all" | "root" | "<any positive integer argument in the range of 1 and 100>";
            export type Expand = string[];
            export type Id = string;
            export type Limit = number; // int32
            export type Start = number; // int32
            export type Type = "page" | "comment" | "attachment";
        }
        export interface PathParameters {
            id: Parameters.Id;
            type: Parameters.Type;
        }
        export interface QueryParameters {
            depth?: Parameters.Depth;
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentArray;
        }
    }
    namespace GetGlobalTheme {
        namespace Responses {
            export type $200 = Components.Schemas.Theme;
        }
    }
    namespace GetGroupByGroupId {
        namespace Parameters {
            export type Id = string;
        }
        export interface QueryParameters {
            id: Parameters.Id;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Group;
        }
    }
    namespace GetGroupByName {
        namespace Parameters {
            export type GroupName = string;
        }
        export interface PathParameters {
            groupName: Parameters.GroupName;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Group;
        }
    }
    namespace GetGroupByQueryParam {
        namespace Parameters {
            export type Name = string;
        }
        export interface QueryParameters {
            name: Parameters.Name;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Group;
        }
    }
    namespace GetGroupMembers {
        namespace Parameters {
            export type GroupName = string;
            export type Limit = number; // int32
            export type Start = number; // int32
        }
        export interface PathParameters {
            groupName: Parameters.GroupName;
        }
        export interface QueryParameters {
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.UserArray;
        }
    }
    namespace GetGroupMembersByGroupId {
        namespace Parameters {
            export type Expand = ("operations" | "personalSpace" | "isExternalCollaborator")[];
            export type GroupId = string;
            export type Limit = number; // int32
            export type ShouldReturnTotalSize = boolean;
            export type Start = number; // int32
        }
        export interface PathParameters {
            groupId: Parameters.GroupId;
        }
        export interface QueryParameters {
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            shouldReturnTotalSize?: Parameters.ShouldReturnTotalSize;
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.UserArray;
        }
    }
    namespace GetGroupMembershipsForUser {
        namespace Parameters {
            export type AccountId = string;
            export type Limit = number; // int32
            export type Start = number; // int32
        }
        export interface QueryParameters {
            accountId: Parameters.AccountId;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = /* Same as GroupArray but with `_links` property. */ Components.Schemas.GroupArrayWithLinks;
        }
    }
    namespace GetGroups {
        namespace Parameters {
            export type AccessType = "user" | "admin" | "site-admin";
            export type Limit = number; // int32
            export type Start = number; // int32
        }
        export interface QueryParameters {
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            accessType?: Parameters.AccessType;
        }
        namespace Responses {
            export type $200 = /* Same as GroupArray but with `_links` property. */ Components.Schemas.GroupArrayWithLinks;
        }
    }
    namespace GetHistoryForContent {
        namespace Parameters {
            export type Expand = ("lastUpdated" | "previousVersion" | "contributors" | "nextVersion")[];
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentHistory;
        }
    }
    namespace GetIndividualGroupRestrictionStatusByGroupId {
        namespace Parameters {
            export type GroupId = string;
            export type Id = string;
            export type OperationKey = "read" | "update";
        }
        export interface PathParameters {
            id: Parameters.Id;
            operationKey: Parameters.OperationKey;
            groupId: Parameters.GroupId;
        }
    }
    namespace GetLabelsForContent {
        namespace Parameters {
            export type Id = string;
            export type Limit = number; // int32
            export type Prefix = "global" | "my" | "team";
            export type Start = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            prefix?: Parameters.Prefix;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.LabelArray;
        }
    }
    namespace GetLabelsForSpace {
        namespace Parameters {
            export type Limit = number; // int32
            export type Prefix = "global" | "my" | "team";
            export type SpaceKey = string;
            export type Start = number; // int32
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export interface QueryParameters {
            prefix?: Parameters.Prefix;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.LabelArray;
        }
    }
    namespace GetLookAndFeelSettings {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface QueryParameters {
            spaceKey?: Parameters.SpaceKey;
        }
        namespace Responses {
            export type $200 = Components.Schemas.LookAndFeelSettings;
        }
    }
    namespace GetMacroBodyByMacroId {
        namespace Parameters {
            export type Id = string;
            export type MacroId = string;
            export type Version = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
            version: Parameters.Version /* int32 */;
            macroId: Parameters.MacroId;
        }
        namespace Responses {
            export type $200 = Components.Schemas.MacroInstance;
        }
    }
    namespace GetMembersByQueryParam {
        namespace Parameters {
            export type Expand = ("operations" | "personalSpace" | "isExternalCollaborator")[];
            export type Limit = number; // int32
            export type Name = string;
            export type ShouldReturnTotalSize = boolean;
            export type Start = number; // int32
        }
        export interface QueryParameters {
            name: Parameters.Name;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            shouldReturnTotalSize?: Parameters.ShouldReturnTotalSize;
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.UserArray;
        }
    }
    namespace GetPrivacyUnsafeUserEmail {
        namespace Parameters {
            export type AccountId = string;
        }
        export interface QueryParameters {
            accountId: Parameters.AccountId;
        }
        namespace Responses {
            export type $200 = Components.Schemas.AccountIdEmailRecord;
        }
    }
    namespace GetPrivacyUnsafeUserEmailBulk {
        namespace Parameters {
            export type AccountId = string[];
        }
        export interface QueryParameters {
            accountId: Parameters.AccountId;
        }
        namespace Responses {
            export type $200 = Components.Schemas.AccountIdEmailRecordArray;
        }
    }
    namespace GetRelationship {
        namespace Parameters {
            export type Expand = ("relationData" | "source" | "target")[];
            export type RelationName = string;
            export type SourceKey = string;
            export type SourceStatus = string;
            export type SourceType = "user" | "content" | "space";
            export type SourceVersion = number; // int32
            export type TargetKey = string;
            export type TargetStatus = string;
            export type TargetType = "user" | "content" | "space";
            export type TargetVersion = number; // int32
        }
        export interface PathParameters {
            relationName: Parameters.RelationName;
            sourceType: Parameters.SourceType;
            sourceKey: Parameters.SourceKey;
            targetType: Parameters.TargetType;
            targetKey: Parameters.TargetKey;
        }
        export interface QueryParameters {
            sourceStatus?: Parameters.SourceStatus;
            targetStatus?: Parameters.TargetStatus;
            sourceVersion?: Parameters.SourceVersion /* int32 */;
            targetVersion?: Parameters.TargetVersion /* int32 */;
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Relation;
        }
    }
    namespace GetRestrictions {
        namespace Parameters {
            export type Expand = ("restrictions.user" | "read.restrictions.user" | "update.restrictions.user" | "restrictions.group" | "read.restrictions.group" | "update.restrictions.group" | "content")[];
            export type Id = string;
            export type Limit = number; // int32
            export type Start = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentRestrictionArray;
        }
    }
    namespace GetRestrictionsByOperation {
        namespace Parameters {
            export type Expand = ("restrictions.user" | "restrictions.group" | "content")[];
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export interface $200 {
                [name: string]: {
                    operationType?: Components.Schemas.ContentRestriction;
                    _links?: Components.Schemas.GenericLinks;
                };
            }
        }
    }
    namespace GetRestrictionsForOperation {
        namespace Parameters {
            export type Expand = ("restrictions.user" | "restrictions.group" | "content")[];
            export type Id = string;
            export type Limit = number; // int32
            export type OperationKey = "read" | "update";
            export type Start = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
            operationKey: Parameters.OperationKey;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentRestriction;
        }
    }
    namespace GetRetentionPeriod {
        namespace Responses {
            export type $200 = Components.Schemas.RetentionPeriod;
        }
    }
    namespace GetSpace {
        namespace Parameters {
            export type Expand = ("settings" | "metadata" | "metadata.labels" | "operations" | "lookAndFeel" | "permissions" | "icon" | "description" | "description.plain" | "description.view" | "theme" | "homepage" | "history")[];
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Space;
        }
    }
    namespace GetSpaceContentStates {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        namespace Responses {
            /**
             * Space suggested content states that users can choose from
             */
            export type $200 = Components.Schemas.ContentState[];
            export interface $401 {
            }
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace GetSpaceProperties {
        namespace Parameters {
            export type Expand = ("version" | "space")[];
            export type Limit = number; // int32
            export type SpaceKey = string;
            export type Start = number; // int32
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.SpacePropertyArray;
        }
    }
    namespace GetSpaceProperty {
        namespace Parameters {
            export type Expand = ("version" | "space")[];
            export type Key = string;
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
            key: Parameters.Key;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.SpaceProperty;
        }
    }
    namespace GetSpaceSettings {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        namespace Responses {
            export type $200 = Components.Schemas.SpaceSettings;
        }
    }
    namespace GetSpaceTheme {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Theme;
        }
    }
    namespace GetSpaces {
        namespace Parameters {
            export type Expand = ("settings" | "metadata" | "metadata.labels" | "operations" | "lookAndFeel" | "permissions" | "icon" | "description" | "description.plain" | "description.view" | "theme" | "homepage" | "history")[];
            export type Favourite = boolean;
            export type FavouriteUserKey = string;
            export type Label = string[];
            export type Limit = number; // int32
            export type SpaceId = number /* int64 */[];
            export type SpaceKey = string[];
            export type Start = number; // int32
            export type Status = "current" | "archived";
            export type Type = "global" | "personal";
        }
        export interface QueryParameters {
            spaceKey?: Parameters.SpaceKey;
            spaceId?: Parameters.SpaceId;
            type?: Parameters.Type;
            status?: Parameters.Status;
            label?: Parameters.Label;
            favourite?: Parameters.Favourite;
            favouriteUserKey?: Parameters.FavouriteUserKey;
            expand?: Parameters.Expand;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.SpaceArray;
        }
    }
    namespace GetSystemInfo {
        namespace Responses {
            export type $200 = Components.Schemas.SystemInfoEntity;
        }
    }
    namespace GetTask {
        namespace Parameters {
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        namespace Responses {
            export type $200 = /**
             * Same as LongTaskStatus but with `_links` property.
             *
             * Status keys:
             *
             * - `ERROR_UNKNOWN` - Generic error
             * - `ERROR_LOCK_FAILED` - Could not get the lock on destination space
             * - `ERROR_RELINK` - Error when relink pages/attachments
             * - `ERROR_COPY_PAGE` - Error while copying 1 page
             * - `WARN_RENAME_PAGE` - Warning page is rename during copy
             * - `WARN_IGNORE_COPY_PERMISSION` - Warning could not copy permission
             * - `WARN_IGNORE_COPY_ATTACHMENT` - Warning could not copy attachment
             * - `WARN_IGNORE_DELETE_PAGE` - Warning ignoring delete of a non agreed on page
             * - `STATUS_COPIED_PAGES` - Message total pages are copied
             * - `STATUS_COPYING_PAGES` - Message copy pages
             * - `STATUS_RELINK_PAGES` - Message relink pages/attachments
             * - `STATUS_DELETING_PAGES` - Message delete pages
             * - `STATUS_DELETED_PAGES` - Message total pages are deleted
             * - `STATUS_MOVING_PAGES` - Message move pages
             * - `WARN_IGNORE_VIEW_RESTRICTED` - Permission changed - view restricted
             * - `WARN_IGNORE_EDIT_RESTRICTED` - Permission changed - edit restricted
             * - `INITIALIZING_TASK` - Message when initializing task
             * - `UNKNOWN_STATUS` - Message when status is unknown
             */
            Components.Schemas.LongTaskStatusWithLinks;
        }
    }
    namespace GetTaskById {
        namespace Parameters {
            export type InlineTaskId = string;
        }
        export interface PathParameters {
            inlineTaskId: Parameters.InlineTaskId;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Task;
            export interface $401 {
            }
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace GetTaskUpdate {
        namespace Parameters {
            export type TaskId = string;
        }
        export interface PathParameters {
            taskId: Parameters.TaskId;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentStateBulkSetTaskUpdate;
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace GetTasks {
        namespace Parameters {
            export type Limit = number; // int32
            export type Start = number; // int32
        }
        export interface QueryParameters {
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.LongTaskStatusArray;
        }
    }
    namespace GetTheme {
        namespace Parameters {
            export type ThemeKey = string;
        }
        export interface PathParameters {
            themeKey: Parameters.ThemeKey;
        }
        namespace Responses {
            export type $200 = Components.Schemas.Theme;
        }
    }
    namespace GetThemes {
        namespace Parameters {
            export type Limit = number; // int32
            export type Start = number; // int32
        }
        export interface QueryParameters {
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ThemeArray;
        }
    }
    namespace GetUser {
        namespace Parameters {
            export type AccountId = string;
            export type Expand = ("operations" | "personalSpace" | "isExternalCollaborator")[];
        }
        export interface QueryParameters {
            accountId: Parameters.AccountId;
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.User;
        }
    }
    namespace GetUserProperties {
        namespace Parameters {
            export type Limit = number; // int32
            export type Start = number; // int32
            export type UserId = string;
        }
        export interface PathParameters {
            userId: Parameters.UserId;
        }
        export interface QueryParameters {
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.UserPropertyKeyArray;
        }
    }
    namespace GetUserProperty {
        namespace Parameters {
            export type Key = string; // ^[-_a-zA-Z0-9]+$
            export type UserId = string;
        }
        export interface PathParameters {
            userId: Parameters.UserId;
            key: Parameters.Key /* ^[-_a-zA-Z0-9]+$ */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.UserProperty;
        }
    }
    namespace GetViewers {
        namespace Parameters {
            export type ContentId = string;
            export type FromDate = string;
        }
        export interface PathParameters {
            contentId: Parameters.ContentId;
        }
        export interface QueryParameters {
            fromDate?: Parameters.FromDate;
        }
        namespace Responses {
            export interface $200 {
                /**
                 * The content ID.
                 */
                id?: number;
                /**
                 * The total number of distinct viewers for the content.
                 */
                count?: number;
            }
            export interface $400 {
            }
            export interface $404 {
            }
        }
    }
    namespace GetViews {
        namespace Parameters {
            export type ContentId = string;
            export type FromDate = string;
        }
        export interface PathParameters {
            contentId: Parameters.ContentId;
        }
        export interface QueryParameters {
            fromDate?: Parameters.FromDate;
        }
        namespace Responses {
            export interface $200 {
                /**
                 * The content ID.
                 */
                id?: number;
                /**
                 * The total number of views for the content.
                 */
                count?: number;
            }
            export interface $400 {
            }
            export interface $404 {
            }
        }
    }
    namespace GetWatchersForSpace {
        namespace Parameters {
            export type Limit = string;
            export type SpaceKey = string;
            export type Start = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export interface QueryParameters {
            start?: Parameters.Start;
            limit?: Parameters.Limit;
        }
        namespace Responses {
            export type $200 = Components.Schemas.SpaceWatchArray;
        }
    }
    namespace GetWatchesForPage {
        namespace Parameters {
            export type Id = string;
            export type Limit = number; // int32
            export type Start = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.WatchArray;
        }
    }
    namespace GetWatchesForSpace {
        namespace Parameters {
            export type Id = string;
            export type Limit = number; // int32
            export type Start = number; // int32
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.SpaceWatchArray;
        }
    }
    namespace IsWatchingLabel {
        namespace Parameters {
            export type AccountId = string;
            export type Key = string;
            export type LabelName = string;
            export type Username = string;
        }
        export interface PathParameters {
            labelName: Parameters.LabelName;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
        namespace Responses {
            export type $200 = Components.Schemas.UserWatch;
        }
    }
    namespace IsWatchingSpace {
        namespace Parameters {
            export type AccountId = string;
            export type Key = string;
            export type SpaceKey = string;
            export type Username = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
        namespace Responses {
            export type $200 = Components.Schemas.UserWatch;
        }
    }
    namespace MovePage {
        namespace Parameters {
            export type PageId = string;
            export type Position = "before" | "after" | "append";
            export type TargetId = string;
        }
        export interface PathParameters {
            pageId: Parameters.PageId;
            position: Parameters.Position;
            targetId: Parameters.TargetId;
        }
        namespace Responses {
            export interface $200 {
                pageId?: Components.Schemas.ContentId;
            }
        }
    }
    namespace PublishLegacyDraft {
        namespace Parameters {
            export type DraftId = string;
            export type Expand = string[];
            export type Status = string;
        }
        export interface PathParameters {
            draftId: Parameters.DraftId;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
            expand?: Parameters.Expand;
        }
        export type RequestBody = Components.Schemas.ContentBlueprintDraft;
        namespace Responses {
            export type $200 = /* Base object for all content types. */ Components.Schemas.Content;
        }
    }
    namespace PublishSharedDraft {
        namespace Parameters {
            export type DraftId = string;
            export type Expand = string[];
            export type Status = string;
        }
        export interface PathParameters {
            draftId: Parameters.DraftId;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
            expand?: Parameters.Expand;
        }
        export type RequestBody = Components.Schemas.ContentBlueprintDraft;
        namespace Responses {
            export type $200 = /* Base object for all content types. */ Components.Schemas.Content;
        }
    }
    namespace RemoveContentState {
        namespace Parameters {
            export type Id = string;
            export type Status = "current" | "draft";
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentStateResponse;
            export interface $401 {
            }
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace RemoveContentWatcher {
        export interface HeaderParameters {
            "X-Atlassian-Token": Parameters.XAtlassianToken;
        }
        namespace Parameters {
            export type AccountId = string;
            export type ContentId = string;
            export type Key = string;
            export type Username = string;
            export type XAtlassianToken = string;
        }
        export interface PathParameters {
            contentId: Parameters.ContentId;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace RemoveGroup {
        namespace Parameters {
            export type Name = string;
        }
        export interface QueryParameters {
            name: Parameters.Name;
        }
    }
    namespace RemoveGroupById {
        namespace Parameters {
            export type Id = string;
        }
        export interface QueryParameters {
            id: Parameters.Id;
        }
    }
    namespace RemoveGroupFromContentRestriction {
        namespace Parameters {
            export type GroupId = string;
            export type Id = string;
            export type OperationKey = "read" | "update";
        }
        export interface PathParameters {
            id: Parameters.Id;
            operationKey: Parameters.OperationKey;
            groupId: Parameters.GroupId;
        }
    }
    namespace RemoveGroupFromContentRestrictionById {
        namespace Parameters {
            export type GroupName = string;
            export type Id = string;
            export type OperationKey = "read" | "update";
        }
        export interface PathParameters {
            id: Parameters.Id;
            operationKey: Parameters.OperationKey;
            groupName: Parameters.GroupName;
        }
    }
    namespace RemoveLabelFromContent {
        namespace Parameters {
            export type Id = string;
            export type Label = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
            label: Parameters.Label;
        }
    }
    namespace RemoveLabelFromContentUsingQueryParameter {
        namespace Parameters {
            export type Id = string;
            export type Name = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            name: Parameters.Name;
        }
    }
    namespace RemoveLabelWatcher {
        namespace Parameters {
            export type AccountId = string;
            export type Key = string;
            export type LabelName = string;
            export type Username = string;
        }
        export interface PathParameters {
            labelName: Parameters.LabelName;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace RemoveMemberFromGroup {
        namespace Parameters {
            export type AccountId = string;
            export type Key = string;
            export type Name = string;
            export type Username = string;
        }
        export interface QueryParameters {
            name: Parameters.Name;
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace RemoveMemberFromGroupByGroupId {
        namespace Parameters {
            export type AccountId = string;
            export type GroupId = string;
            export type Key = string;
            export type Username = string;
        }
        export interface QueryParameters {
            groupId: Parameters.GroupId;
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace RemoveModules {
        namespace Parameters {
            export type ModuleKey = string[];
        }
        export interface QueryParameters {
            moduleKey: Parameters.ModuleKey;
        }
    }
    namespace RemovePermission {
        namespace Parameters {
            export type Id = number;
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
            id: Parameters.Id;
        }
    }
    namespace RemoveSpaceWatch {
        namespace Parameters {
            export type AccountId = string;
            export type Key = string;
            export type SpaceKey = string;
            export type Username = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace RemoveTemplate {
        namespace Parameters {
            export type ContentTemplateId = string;
        }
        export interface PathParameters {
            contentTemplateId: Parameters.ContentTemplateId;
        }
        namespace Responses {
            export interface $403 {
            }
        }
    }
    namespace RemoveUserFromContentRestriction {
        namespace Parameters {
            export type AccountId = string;
            export type Id = string;
            export type Key = string;
            export type OperationKey = "read" | "update";
            export type Username = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
            operationKey: Parameters.OperationKey;
        }
        export interface QueryParameters {
            key?: Parameters.Key;
            username?: Parameters.Username;
            accountId?: Parameters.AccountId;
        }
    }
    namespace ResetLookAndFeelSettings {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface QueryParameters {
            spaceKey?: Parameters.SpaceKey;
        }
    }
    namespace ResetSpaceTheme {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
    }
    namespace RestoreContentVersion {
        namespace Parameters {
            export type Expand = string[];
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        export type RequestBody = Components.Schemas.VersionRestore;
        namespace Responses {
            export type $200 = Components.Schemas.Version;
        }
    }
    namespace SearchByCQL {
        namespace Parameters {
            export type Cql = string;
            export type Cqlcontext = string;
            export type Cursor = string;
            export type Excerpt = "highlight" | "indexed" | "none" | "highlight_unescaped" | "indexed_unescaped";
            export type ExcludeCurrentSpaces = boolean;
            export type Expand = string[];
            export type IncludeArchivedSpaces = boolean;
            export type Limit = number; // int32
            export type Next = boolean;
            export type Prev = boolean;
            export type SitePermissionTypeFilter = "all" | "externalCollaborator" | "none";
            export type Start = number; // int32
            export type _ = number; // int64
        }
        export interface QueryParameters {
            cql: Parameters.Cql;
            cqlcontext?: Parameters.Cqlcontext;
            cursor?: Parameters.Cursor;
            next?: Parameters.Next;
            prev?: Parameters.Prev;
            limit?: Parameters.Limit /* int32 */;
            start?: Parameters.Start /* int32 */;
            includeArchivedSpaces?: Parameters.IncludeArchivedSpaces;
            excludeCurrentSpaces?: Parameters.ExcludeCurrentSpaces;
            excerpt?: Parameters.Excerpt;
            sitePermissionTypeFilter?: Parameters.SitePermissionTypeFilter;
            _?: Parameters._ /* int64 */;
            expand?: Parameters.Expand;
        }
        namespace Responses {
            export type $200 = Components.Schemas.SearchPageResponseSearchResult;
        }
    }
    namespace SearchContentByCQL {
        namespace Parameters {
            export type Cql = string;
            export type Cqlcontext = string;
            export type Cursor = string;
            export type Expand = string[];
            export type Limit = number; // int32
        }
        export interface QueryParameters {
            cql: Parameters.Cql;
            cqlcontext?: Parameters.Cqlcontext;
            expand?: Parameters.Expand;
            cursor?: Parameters.Cursor;
            limit?: Parameters.Limit /* int32 */;
        }
        namespace Responses {
            export type $200 = Components.Schemas.ContentArray;
        }
    }
    namespace SearchGroups {
        namespace Parameters {
            export type Limit = number; // int32
            export type Query = string;
            export type ShouldReturnTotalSize = boolean;
            export type Start = number; // int32
        }
        export interface QueryParameters {
            query: Parameters.Query;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            shouldReturnTotalSize?: Parameters.ShouldReturnTotalSize;
        }
        namespace Responses {
            export type $200 = /* Same as GroupArray but with `_links` property. */ Components.Schemas.GroupArrayWithLinks;
        }
    }
    namespace SearchTasks {
        namespace Parameters {
            export type Assignee = string;
            export type CompletedUser = string;
            export type CompletedateFrom = number; // int64
            export type CompletedateTo = number; // int64
            export type CreatedateFrom = number; // int64
            export type CreatedateTo = number; // int64
            export type Creator = string;
            export type DuedateFrom = number; // int64
            export type DuedateTo = number; // int64
            export type Limit = number; // int32
            export type PageId = string;
            export type SpaceKey = string;
            export type Start = number; // int32
            export type Status = "complete" | "incomplete";
        }
        export interface QueryParameters {
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            spaceKey?: Parameters.SpaceKey;
            pageId?: Parameters.PageId;
            assignee?: Parameters.Assignee;
            creator?: Parameters.Creator;
            completedUser?: Parameters.CompletedUser;
            duedateFrom?: Parameters.DuedateFrom /* int64 */;
            duedateTo?: Parameters.DuedateTo /* int64 */;
            createdateFrom?: Parameters.CreatedateFrom /* int64 */;
            createdateTo?: Parameters.CreatedateTo /* int64 */;
            completedateFrom?: Parameters.CompletedateFrom /* int64 */;
            completedateTo?: Parameters.CompletedateTo /* int64 */;
            status?: Parameters.Status;
        }
        namespace Responses {
            export type $200 = Components.Schemas.TaskPageResponse;
        }
    }
    namespace SearchUser {
        namespace Parameters {
            export type Cql = string;
            export type Expand = string[];
            export type Limit = number; // int32
            export type SitePermissionTypeFilter = "all" | "externalCollaborator" | "none";
            export type Start = number; // int32
        }
        export interface QueryParameters {
            cql: Parameters.Cql;
            start?: Parameters.Start /* int32 */;
            limit?: Parameters.Limit /* int32 */;
            expand?: Parameters.Expand;
            sitePermissionTypeFilter?: Parameters.SitePermissionTypeFilter;
        }
        namespace Responses {
            export type $200 = Components.Schemas.SearchPageResponseSearchResult;
        }
    }
    namespace SetContentState {
        namespace Parameters {
            export type Id = string;
            export type Status = "current" | "draft";
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
        }
        export type RequestBody = Components.Schemas.ContentStateRestInput;
        namespace Responses {
            export type $200 = Components.Schemas.ContentStateResponse;
            export interface $400 {
            }
            export interface $401 {
            }
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace SetLookAndFeelSettings {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface QueryParameters {
            spaceKey?: Parameters.SpaceKey;
        }
        export type RequestBody = "global" | "custom" | "theme";
        namespace Responses {
            export type $200 = string;
        }
    }
    namespace SetRetentionPeriod {
        export type RequestBody = Components.Schemas.RetentionPeriod;
        namespace Responses {
            export type $200 = Components.Schemas.RetentionPeriod;
        }
    }
    namespace SetSpaceTheme {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export type RequestBody = Components.Schemas.ThemeUpdate;
        namespace Responses {
            export type $200 = Components.Schemas.Theme;
        }
    }
    namespace UpdateAttachmentData {
        namespace Parameters {
            export type AttachmentId = string;
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
            attachmentId: Parameters.AttachmentId;
        }
        export interface RequestBody {
            /**
             * The relative location and name of the attachment to be added to
             * the content.
             */
            file: string; // binary
            /**
             * The comment for the attachment that is being added.
             * If you specify a comment, then every file must have a comment and
             * the comments must be in the same order as the files. Alternatively,
             * don't specify any comments.
             */
            comment?: string; // binary
            /**
             * If `minorEdits` is set to 'true', no notification email or activity stream
             * will be generated when the attachment is added to the content.
             */
            minorEdit: string; // binary
        }
        namespace Responses {
            export type $200 = /* Base object for all content types. */ Components.Schemas.Content;
        }
    }
    namespace UpdateAttachmentProperties {
        namespace Parameters {
            export type AttachmentId = string;
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
            attachmentId: Parameters.AttachmentId;
        }
        export type RequestBody = Components.Schemas.AttachmentPropertiesUpdateBody;
        namespace Responses {
            export type $200 = /* Base object for all content types. */ Components.Schemas.Content;
        }
    }
    namespace UpdateContent {
        namespace Parameters {
            export type ConflictPolicy = "abort";
            export type Id = string;
            export type Status = "current" | "trashed" | "deleted" | "historical" | "draft";
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            status?: Parameters.Status;
            conflictPolicy?: Parameters.ConflictPolicy;
        }
        export type RequestBody = Components.Schemas.ContentUpdate;
        namespace Responses {
            export type $200 = /* Base object for all content types. */ Components.Schemas.Content;
        }
    }
    namespace UpdateContentProperty {
        namespace Parameters {
            export type Id = string;
            export type Key = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
            key: Parameters.Key;
        }
        export type RequestBody = Components.Schemas.ContentPropertyUpdate;
        namespace Responses {
            export type $200 = Components.Schemas.ContentProperty;
        }
    }
    namespace UpdateContentTemplate {
        export type RequestBody = /* This object is used to update content templates. */ Components.Schemas.ContentTemplateUpdate;
        namespace Responses {
            export type $200 = Components.Schemas.ContentTemplate;
        }
    }
    namespace UpdateLookAndFeel {
        export type RequestBody = /* Look and feel selection */ Components.Schemas.LookAndFeelSelection;
        namespace Responses {
            export type $200 = /* Look and feel selection */ Components.Schemas.LookAndFeelSelection;
        }
    }
    namespace UpdateLookAndFeelSettings {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface QueryParameters {
            spaceKey?: Parameters.SpaceKey;
        }
        export type RequestBody = Components.Schemas.LookAndFeel;
        namespace Responses {
            export type $200 = /* Look and feel settings returned after an update. */ Components.Schemas.LookAndFeelWithLinks;
        }
    }
    namespace UpdateRestrictions {
        namespace Parameters {
            export type Expand = ("restrictions.user" | "read.restrictions.user" | "update.restrictions.user" | "restrictions.group" | "read.restrictions.group" | "update.restrictions.group" | "content")[];
            export type Id = string;
        }
        export interface PathParameters {
            id: Parameters.Id;
        }
        export interface QueryParameters {
            expand?: Parameters.Expand;
        }
        export type RequestBody = Components.Schemas.ContentRestrictionAddOrUpdateArray;
        namespace Responses {
            export type $200 = Components.Schemas.ContentRestrictionArray;
        }
    }
    namespace UpdateSpace {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export type RequestBody = /* The properties of a space that can be updated. */ Components.Schemas.SpaceUpdate;
        namespace Responses {
            export type $200 = Components.Schemas.Space;
        }
    }
    namespace UpdateSpaceProperty {
        namespace Parameters {
            export type Key = string;
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
            key: Parameters.Key;
        }
        export type RequestBody = Components.Schemas.SpacePropertyUpdate;
        namespace Responses {
            export type $200 = Components.Schemas.SpaceProperty;
        }
    }
    namespace UpdateSpaceSettings {
        namespace Parameters {
            export type SpaceKey = string;
        }
        export interface PathParameters {
            spaceKey: Parameters.SpaceKey;
        }
        export type RequestBody = Components.Schemas.SpaceSettingsUpdate;
        namespace Responses {
            export type $200 = Components.Schemas.SpaceSettings;
        }
    }
    namespace UpdateTaskById {
        namespace Parameters {
            export type InlineTaskId = string;
        }
        export interface PathParameters {
            inlineTaskId: Parameters.InlineTaskId;
        }
        export type RequestBody = Components.Schemas.TaskStatusUpdate;
        namespace Responses {
            export type $200 = Components.Schemas.Task;
            export interface $400 {
            }
            export interface $401 {
            }
            export interface $403 {
            }
            export interface $404 {
            }
        }
    }
    namespace UpdateUserProperty {
        namespace Parameters {
            export type Key = string; // ^[-_a-zA-Z0-9]+$
            export type UserId = string;
        }
        export interface PathParameters {
            userId: Parameters.UserId;
            key: Parameters.Key /* ^[-_a-zA-Z0-9]+$ */;
        }
        export type RequestBody = Components.Schemas.UserPropertyUpdate;
        namespace Responses {
            export interface $204 {
            }
        }
    }
}

export interface OperationMethods {
  /**
   * getAuditRecords - Get audit records
   * 
   * Returns all records in the audit log, optionally for a certain date range.
   * This contains information about events like space exports, group membership
   * changes, app installations, etc. For more information, see
   * [Audit log](https://confluence.atlassian.com/confcloud/audit-log-802164269.html)
   * in the Confluence administrator's guide.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission.
   */
  'getAuditRecords'(
    parameters?: Parameters<Paths.GetAuditRecords.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAuditRecords.Responses.$200>
  /**
   * createAuditRecord - Create audit record
   * 
   * Creates a record in the audit log.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission.
   */
  'createAuditRecord'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateAuditRecord.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateAuditRecord.Responses.$200>
  /**
   * exportAuditRecords - Export audit records
   * 
   * Exports audit records as a CSV file or ZIP file.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission.
   */
  'exportAuditRecords'(
    parameters?: Parameters<Paths.ExportAuditRecords.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.ExportAuditRecords.Responses.$200>
  /**
   * getRetentionPeriod - Get retention period
   * 
   * Returns the retention period for records in the audit log. The retention
   * period is how long an audit record is kept for, from creation date until
   * it is deleted.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission.
   */
  'getRetentionPeriod'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetRetentionPeriod.Responses.$200>
  /**
   * setRetentionPeriod - Set retention period
   * 
   * Sets the retention period for records in the audit log. The retention period
   * can be set to a maximum of 1 year.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission.
   */
  'setRetentionPeriod'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.SetRetentionPeriod.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SetRetentionPeriod.Responses.$200>
  /**
   * getAuditRecordsForTimePeriod - Get audit records for time period
   * 
   * Returns records from the audit log, for a time period back from the current
   * date. For example, you can use this method to get the last 3 months of records.
   * 
   * This contains information about events like space exports, group membership
   * changes, app installations, etc. For more information, see
   * [Audit log](https://confluence.atlassian.com/confcloud/audit-log-802164269.html)
   * in the Confluence administrator's guide.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission.
   */
  'getAuditRecordsForTimePeriod'(
    parameters?: Parameters<Paths.GetAuditRecordsForTimePeriod.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAuditRecordsForTimePeriod.Responses.$200>
  /**
   * getContent - Get content
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns all content in a Confluence instance.
   * 
   * By default, the following objects are expanded: `space`, `history`, `version`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Only content that the user has permission to view will be returned.
   */
  'getContent'(
    parameters?: Parameters<Paths.GetContent.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContent.Responses.$200>
  /**
   * createContent - Create content
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Creates a new piece of content or publishes an existing draft.
   * 
   * To publish a draft, add the `id` and `status` properties to the body of the request.
   * Set the `id` to the ID of the draft and set the `status` to 'current'. When the
   * request is sent, a new piece of content will be created and the metadata from the
   * draft will be transferred into it.
   * 
   * By default, the following objects are expanded: `space`, `history`, `version`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: 'Add' permission for the
   * space that the content will be created in, and permission to view the draft if publishing a draft.
   */
  'createContent'(
    parameters?: Parameters<Paths.CreateContent.QueryParameters> | null,
    data?: Paths.CreateContent.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateContent.Responses.$200>
  /**
   * archivePages - Archive pages
   * 
   * Archives a list of pages. The pages to be archived are specified as a list of content IDs.
   * This API accepts the archival request and returns a task ID.
   * The archival process happens asynchronously.
   * Use the /longtask/<taskId> REST API to get the copy task status.
   * 
   * Each content ID needs to resolve to page objects that are not already in an archived state.
   * The content IDs need not belong to the same space.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Archive' permission for each of the pages in the corresponding space it belongs to.
   */
  'archivePages'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.ArchivePages.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.ArchivePages.Responses.$202>
  /**
   * publishSharedDraft - Publish shared draft
   * 
   * Publishes a shared draft of a page created from a blueprint.
   * 
   * By default, the following objects are expanded: `body.storage`, `history`, `space`, `version`, `ancestors`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the draft and 'Add' permission for the space that
   * the content will be created in.
   */
  'publishSharedDraft'(
    parameters?: Parameters<Paths.PublishSharedDraft.PathParameters & Paths.PublishSharedDraft.QueryParameters> | null,
    data?: Paths.PublishSharedDraft.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PublishSharedDraft.Responses.$200>
  /**
   * publishLegacyDraft - Publish legacy draft
   * 
   * Publishes a legacy draft of a page created from a blueprint. Legacy drafts
   * will eventually be removed in favor of shared drafts. For now, this method
   * works the same as [Publish shared draft](#api-content-blueprint-instance-draftId-put).
   * 
   * By default, the following objects are expanded: `body.storage`, `history`, `space`, `version`, `ancestors`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the draft and 'Add' permission for the space that
   * the content will be created in.
   */
  'publishLegacyDraft'(
    parameters?: Parameters<Paths.PublishLegacyDraft.PathParameters & Paths.PublishLegacyDraft.QueryParameters> | null,
    data?: Paths.PublishLegacyDraft.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PublishLegacyDraft.Responses.$200>
  /**
   * searchContentByCQL - Search content by CQL
   * 
   * Returns the list of content that matches a Confluence Query Language
   * (CQL) query. For information on CQL, see:
   * [Advanced searching using CQL](https://developer.atlassian.com/cloud/confluence/advanced-searching-using-cql/).
   * 
   * Example initial call:
   * ```
   * /wiki/rest/api/content/search?cql=type=page&limit=25
   * ```
   * 
   * Example response:
   * ```
   * {
   *   "results": [
   *     { ... },
   *     { ... },
   *     ...
   *     { ... }
   *   ],
   *   "limit": 25,
   *   "size": 25,
   *   ...
   *   "_links": {
   *     "base": "<url>",
   *     "context": "<url>",
   *     "next": "/rest/api/content/search?cql=type=page&limit=25&cursor=raNDoMsTRiNg",
   *     "self": "<url>"
   *   }
   * }
   * ```
   * 
   * When additional results are available, returns `next` and `prev` URLs to retrieve them in subsequent calls. The URLs each contain a cursor that points to the appropriate set of results. Use `limit` to specify the number of results returned in each call.
   * Example subsequent call (taken from example response):
   * ```
   * /wiki/rest/api/content/search?cql=type=page&limit=25&cursor=raNDoMsTRiNg
   * ```
   * The response to this will have a `prev` URL similar to the `next` in the example response.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Only content that the user has permission to view will be returned.
   */
  'searchContentByCQL'(
    parameters?: Parameters<Paths.SearchContentByCQL.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SearchContentByCQL.Responses.$200>
  /**
   * getContentById - Get content by ID
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns a single piece of content, like a page or a blog post.
   * 
   * By default, the following objects are expanded: `space`, `history`, `version`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content. If the content is a blog post, 'View' permission
   * for the space is required.
   */
  'getContentById'(
    parameters?: Parameters<Paths.GetContentById.PathParameters & Paths.GetContentById.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentById.Responses.$200>
  /**
   * updateContent - Update content
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Updates a piece of content. Use this method to update the title or body
   * of a piece of content, change the status, change the parent page, and more.
   * 
   * Note, updating draft content is currently not supported.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'updateContent'(
    parameters?: Parameters<Paths.UpdateContent.PathParameters & Paths.UpdateContent.QueryParameters> | null,
    data?: Paths.UpdateContent.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateContent.Responses.$200>
  /**
   * deleteContent - Delete content
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Moves a piece of content to the space's trash or purges it from the trash,
   * depending on the content's type and status:
   * 
   * - If the content's type is `page`,`blogpost`, or `attachment` and its status is `current`,
   * it will be trashed.
   * - If the content's type is `page`,`blogpost`, or `attachment` and its status is `trashed`,
   * the content will be purged from the trash and deleted permanently. Note,
   * you must also set the `status` query parameter to `trashed` in your request.
   * - If the content's type is `comment`, it will be deleted
   * permanently without being trashed.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Delete' permission for the space that the content is in.
   */
  'deleteContent'(
    parameters?: Parameters<Paths.DeleteContent.PathParameters & Paths.DeleteContent.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * deletePageTree - Delete page tree
   * 
   * Moves a pagetree rooted at a page to the space's trash:
   * 
   * - If the content's type is `page` and its status is `current`, it will be trashed including
   * all its descendants.
   * - For every other combination of content type and status, this API is not supported.
   * 
   * This API accepts the pageTree delete request and returns a task ID.
   * The delete process happens asynchronously.
   * 
   *  Response example:
   *  <pre><code>
   *  {
   *       "id" : "1180606",
   *       "links" : {
   *            "status" : "/rest/api/longtask/1180606"
   *       }
   *  }
   *  </code></pre>
   *  Use the `/longtask/<taskId>` REST API to get the copy task status.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Delete' permission for the space that the content is in.
   */
  'deletePageTree'(
    parameters?: Parameters<Paths.DeletePageTree.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeletePageTree.Responses.$202>
  /**
   * getContentChildren - Get content children
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns a map of the direct children of a piece of content. A piece of content
   * has different types of child content, depending on its type. These are
   * the default parent-child content type relationships:
   * 
   * - `page`: child content is `page`, `comment`, `attachment`
   * - `blogpost`: child content is `comment`, `attachment`
   * - `attachment`: child content is `comment`
   * - `comment`: child content is `attachment`
   * 
   * Apps can override these default relationships. Apps can also introduce
   * new content types that create new parent-child content relationships.
   * 
   * Note, the map will always include all child content types that are valid
   * for the content. However, if the content has no instances of a child content
   * type, the map will contain an empty array for that child content type.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: 'View' permission for the space,
   * and permission to view the content if it is a page.
   */
  'getContentChildren'(
    parameters?: Parameters<Paths.GetContentChildren.PathParameters & Paths.GetContentChildren.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentChildren.Responses.$200>
  /**
   * movePage - Move a page to a new location relative to a target page
   * 
   * Move a page to a new location relative to a target page:
   * 
   * * `before` - move the page under the same parent as the target, before the target in the list of children
   * * `after` - move the page under the same parent as the target, after the target in the list of children
   * * `append` - move the page to be a child of the target
   * 
   * Caution: This API can move pages to the top level of a space. Top-level pages are difficult to find in the UI
   * because they do not show up in the page tree display. To avoid this, never use `before` or `after` positions
   * when the `targetId` is a top-level page.
   */
  'movePage'(
    parameters?: Parameters<Paths.MovePage.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.MovePage.Responses.$200>
  /**
   * getAttachments - Get attachments
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns the attachments for a piece of content.
   * 
   * By default, the following objects are expanded: `metadata`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content. If the content is a blog post, 'View' permission
   * for the space is required.
   */
  'getAttachments'(
    parameters?: Parameters<Paths.GetAttachments.PathParameters & Paths.GetAttachments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAttachments.Responses.$200>
  /**
   * createOrUpdateAttachments - Create or update attachment
   * 
   * Adds an attachment to a piece of content. If the attachment already exists
   * for the content, then the attachment is updated (i.e. a new version of the
   * attachment is created).
   * 
   * Note, you must set a `X-Atlassian-Token: nocheck` header on the request
   * for this method, otherwise it will be blocked. This protects against XSRF
   * attacks, which is necessary as this method accepts multipart/form-data.
   * 
   * The media type 'multipart/form-data' is defined in [RFC 7578](https://www.ietf.org/rfc/rfc7578.txt).
   * Most client libraries have classes that make it easier to implement
   * multipart posts, like the [MultipartEntityBuilder](https://hc.apache.org/httpcomponents-client-5.1.x/current/httpclient5/apidocs/)
   * Java class provided by Apache HTTP Components.
   * 
   * Note, according to [RFC 7578](https://tools.ietf.org/html/rfc7578#section-4.5),
   * in the case where the form data is text,
   * the charset parameter for the "text/plain" Content-Type may be used to
   * indicate the character encoding used in that part. In the case of this
   * API endpoint, the `comment` body parameter should be sent with `type=text/plain`
   * and `charset=utf-8` values. This will force the charset to be UTF-8.
   * 
   * Example: This curl command attaches a file ('example.txt') to a piece of
   * content (id='123') with a comment and `minorEdits`=true. If the 'example.txt'
   * file already exists, it will update it with a new version of the attachment.
   * 
   * ``` bash
   * curl -D- \
   *   -u admin:admin \
   *   -X PUT \
   *   -H 'X-Atlassian-Token: nocheck' \
   *   -F 'file=@"example.txt"' \
   *   -F 'minorEdit="true"' \
   *   -F 'comment="Example attachment comment"; type=text/plain; charset=utf-8' \
   *   http://myhost/rest/api/content/123/child/attachment
   * ```
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'createOrUpdateAttachments'(
    parameters?: Parameters<Paths.CreateOrUpdateAttachments.PathParameters & Paths.CreateOrUpdateAttachments.QueryParameters> | null,
    data?: Paths.CreateOrUpdateAttachments.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateOrUpdateAttachments.Responses.$200>
  /**
   * createAttachment - Create attachment
   * 
   * Adds an attachment to a piece of content. This method only adds a new
   * attachment. If you want to update an existing attachment, use
   * [Create or update attachments](#api-content-id-child-attachment-put).
   * 
   * Note, you must set a `X-Atlassian-Token: nocheck` header on the request
   * for this method, otherwise it will be blocked. This protects against XSRF
   * attacks, which is necessary as this method accepts multipart/form-data.
   * 
   * The media type 'multipart/form-data' is defined in [RFC 7578](https://www.ietf.org/rfc/rfc7578.txt).
   * Most client libraries have classes that make it easier to implement
   * multipart posts, like the [MultipartEntityBuilder](https://hc.apache.org/httpcomponents-client-5.1.x/current/httpclient5/apidocs/)
   * Java class provided by Apache HTTP Components.
   * 
   * Note, according to [RFC 7578](https://tools.ietf.org/html/rfc7578#section-4.5),
   * in the case where the form data is text,
   * the charset parameter for the "text/plain" Content-Type may be used to
   * indicate the character encoding used in that part. In the case of this
   * API endpoint, the `comment` body parameter should be sent with `type=text/plain`
   * and `charset=utf-8` values. This will force the charset to be UTF-8.
   * 
   * Example: This curl command attaches a file ('example.txt') to a container
   * (id='123') with a comment and `minorEdits`=true.
   * 
   * ``` bash
   * curl -D- \
   *   -u admin:admin \
   *   -X POST \
   *   -H 'X-Atlassian-Token: nocheck' \
   *   -F 'file=@"example.txt"' \
   *   -F 'minorEdit="true"' \
   *   -F 'comment="Example attachment comment"; type=text/plain; charset=utf-8' \
   *   http://myhost/rest/api/content/123/child/attachment
   * ```
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'createAttachment'(
    parameters?: Parameters<Paths.CreateAttachment.PathParameters & Paths.CreateAttachment.QueryParameters> | null,
    data?: Paths.CreateAttachment.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateAttachment.Responses.$200>
  /**
   * updateAttachmentProperties - Update attachment properties
   * 
   * Updates the attachment properties, i.e. the non-binary data of an attachment
   * like the filename, media-type, comment, and parent container.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'updateAttachmentProperties'(
    parameters?: Parameters<Paths.UpdateAttachmentProperties.PathParameters> | null,
    data?: Paths.UpdateAttachmentProperties.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateAttachmentProperties.Responses.$200>
  /**
   * updateAttachmentData - Update attachment data
   * 
   * Updates the binary data of an attachment, given the attachment ID, and
   * optionally the comment and the minor edit field.
   * 
   * This method is essentially the same as [Create or update attachments](#api-content-id-child-attachment-put),
   * except that it matches the attachment ID rather than the name.
   * 
   * Note, you must set a `X-Atlassian-Token: nocheck` header on the request
   * for this method, otherwise it will be blocked. This protects against XSRF
   * attacks, which is necessary as this method accepts multipart/form-data.
   * 
   * The media type 'multipart/form-data' is defined in [RFC 7578](https://www.ietf.org/rfc/rfc7578.txt).
   * Most client libraries have classes that make it easier to implement
   * multipart posts, like the [MultipartEntityBuilder](https://hc.apache.org/httpcomponents-client-5.1.x/current/httpclient5/apidocs/)
   * Java class provided by Apache HTTP Components.
   * 
   * Note, according to [RFC 7578](https://tools.ietf.org/html/rfc7578#section-4.5),
   * in the case where the form data is text,
   * the charset parameter for the "text/plain" Content-Type may be used to
   * indicate the character encoding used in that part. In the case of this
   * API endpoint, the `comment` body parameter should be sent with `type=text/plain`
   * and `charset=utf-8` values. This will force the charset to be UTF-8.
   * 
   * Example: This curl command updates an attachment (id='att456') that is attached
   * to a piece of content (id='123') with a comment and `minorEdits`=true.
   * 
   * ``` bash
   * curl -D- \
   *   -u admin:admin \
   *   -X POST \
   *   -H 'X-Atlassian-Token: nocheck' \
   *   -F 'file=@"example.txt"' \
   *   -F 'minorEdit="true"' \
   *   -F 'comment="Example attachment comment"; type=text/plain; charset=utf-8' \
   *   http://myhost/rest/api/content/123/child/attachment/att456/data
   * ```
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'updateAttachmentData'(
    parameters?: Parameters<Paths.UpdateAttachmentData.PathParameters> | null,
    data?: Paths.UpdateAttachmentData.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateAttachmentData.Responses.$200>
  /**
   * downloadAttatchment - Get URI to download attachment
   * 
   * Redirects the client to a URL that serves an attachment's binary data.
   */
  'downloadAttatchment'(
    parameters?: Parameters<Paths.DownloadAttatchment.PathParameters & Paths.DownloadAttatchment.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getContentComments - Get content comments
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns the comments on a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: 'View' permission for the space,
   * and permission to view the content if it is a page.
   */
  'getContentComments'(
    parameters?: Parameters<Paths.GetContentComments.PathParameters & Paths.GetContentComments.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentComments.Responses.$200>
  /**
   * getContentChildrenByType - Get content children by type
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns all children of a given type, for a piece of content.
   * A piece of content has different types of child content, depending on its type:
   * 
   * - `page`: child content is `page`, `comment`, `attachment`
   * - `blogpost`: child content is `comment`, `attachment`
   * - `attachment`: child content is `comment`
   * - `comment`: child content is `attachment`
   * 
   * Custom content types that are provided by apps can also be returned.
   * 
   * Note, this method only returns direct children. To return children at all
   * levels, use [Get descendants by type](#api-content-id-descendant-type-get).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: 'View' permission for the space,
   * and permission to view the content if it is a page.
   */
  'getContentChildrenByType'(
    parameters?: Parameters<Paths.GetContentChildrenByType.PathParameters & Paths.GetContentChildrenByType.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentChildrenByType.Responses.$200>
  /**
   * getContentDescendants - Get content descendants
   * 
   * Returns a map of the descendants of a piece of content. This is similar
   * to [Get content children](#api-content-id-child-get), except that this
   * method returns child pages at all levels, rather than just the direct
   * child pages.
   * 
   * A piece of content has different types of descendants, depending on its type:
   * 
   * - `page`: descendant is `page`, `comment`, `attachment`
   * - `blogpost`: descendant is `comment`, `attachment`
   * - `attachment`: descendant is `comment`
   * - `comment`: descendant is `attachment`
   * 
   * The map will always include all descendant types that are valid for the content.
   * However, if the content has no instances of a descendant type, the map will
   * contain an empty array for that descendant type.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space, and permission to view the content if it
   * is a page.
   */
  'getContentDescendants'(
    parameters?: Parameters<Paths.GetContentDescendants.PathParameters & Paths.GetContentDescendants.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentDescendants.Responses.$200>
  /**
   * getDescendantsOfType - Get content descendants by type
   * 
   * Returns all descendants of a given type, for a piece of content. This is
   * similar to [Get content children by type](#api-content-id-child-type-get),
   * except that this method returns child pages at all levels, rather than just
   * the direct child pages.
   * 
   * A piece of content has different types of descendants, depending on its type:
   * 
   * - `page`: descendant is `page`, `comment`, `attachment`
   * - `blogpost`: descendant is `comment`, `attachment`
   * - `attachment`: descendant is `comment`
   * - `comment`: descendant is `attachment`
   * 
   * Custom content types that are provided by apps can also be returned.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space, and permission to view the content if it
   * is a page.
   */
  'getDescendantsOfType'(
    parameters?: Parameters<Paths.GetDescendantsOfType.PathParameters & Paths.GetDescendantsOfType.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetDescendantsOfType.Responses.$200>
  /**
   * getHistoryForContent - Get content history
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns the most recent update for a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: Permission to view the content.
   */
  'getHistoryForContent'(
    parameters?: Parameters<Paths.GetHistoryForContent.PathParameters & Paths.GetHistoryForContent.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetHistoryForContent.Responses.$200>
  /**
   * getMacroBodyByMacroId - Get macro body by macro ID
   * 
   * Returns the body of a macro in storage format, for the given macro ID.
   * This includes information like the name of the macro, the body of the macro,
   * and any macro parameters. This method is mainly used by Cloud apps.
   * 
   * About the macro ID: When a macro is created in a new version of content,
   * Confluence will generate a random ID for it, unless an ID is specified
   * (by an app). The macro ID will look similar to this: '50884bd9-0cb8-41d5-98be-f80943c14f96'.
   * The ID is then persisted as new versions of content are created, and is
   * only modified by Confluence if there are conflicting IDs.
   * 
   * Note, to preserve backwards compatibility this resource will also match on
   * the hash of the macro body, even if a macro ID is found. This check will
   * eventually become redundant, as macro IDs are generated for pages and
   * transparently propagate out to all instances.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content that the macro is in.
   */
  'getMacroBodyByMacroId'(
    parameters?: Parameters<Paths.GetMacroBodyByMacroId.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetMacroBodyByMacroId.Responses.$200>
  /**
   * getAndConvertMacroBodyByMacroId - Get macro body by macro ID and convert the representation synchronously
   * 
   * Returns the body of a macro in format specified in path, for the given macro ID.
   * This includes information like the name of the macro, the body of the macro,
   * and any macro parameters.
   * 
   * About the macro ID: When a macro is created in a new version of content,
   * Confluence will generate a random ID for it, unless an ID is specified
   * (by an app). The macro ID will look similar to this: '50884bd9-0cb8-41d5-98be-f80943c14f96'.
   * The ID is then persisted as new versions of content are created, and is
   * only modified by Confluence if there are conflicting IDs.
   * 
   * Note, to preserve backwards compatibility this resource will also match on
   * the hash of the macro body, even if a macro ID is found. This check will
   * eventually become redundant, as macro IDs are generated for pages and
   * transparently propagate out to all instances.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content that the macro is in.
   */
  'getAndConvertMacroBodyByMacroId'(
    parameters?: Parameters<Paths.GetAndConvertMacroBodyByMacroId.PathParameters & Paths.GetAndConvertMacroBodyByMacroId.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAndConvertMacroBodyByMacroId.Responses.$200>
  /**
   * getAndAsyncConvertMacroBodyByMacroId - Get macro body by macro ID and convert representation Asynchronously
   * 
   * Returns Async Id of the conversion task which will convert the macro into a content body of the desired format.
   * The result will be available for 5 minutes after completion of the conversion.
   * 
   * About the macro ID: When a macro is created in a new version of content,
   * Confluence will generate a random ID for it, unless an ID is specified
   * (by an app). The macro ID will look similar to this: '884bd9-0cb8-41d5-98be-f80943c14f96'.
   * The ID is then persisted as new versions of content are created, and is
   * only modified by Confluence if there are conflicting IDs.
   * 
   * Note, to preserve backwards compatibility this resource will also match on
   * the hash of the macro body, even if a macro ID is found. This check will
   * eventually become redundant, as macro IDs are generated for pages and
   * transparently propagate out to all instances.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content that the macro is in.
   */
  'getAndAsyncConvertMacroBodyByMacroId'(
    parameters?: Parameters<Paths.GetAndAsyncConvertMacroBodyByMacroId.PathParameters & Paths.GetAndAsyncConvertMacroBodyByMacroId.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAndAsyncConvertMacroBodyByMacroId.Responses.$200>
  /**
   * getLabelsForContent - Get labels for content
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns the labels on a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space and permission to view the content if it is a page.
   */
  'getLabelsForContent'(
    parameters?: Parameters<Paths.GetLabelsForContent.PathParameters & Paths.GetLabelsForContent.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetLabelsForContent.Responses.$200>
  /**
   * addLabelsToContent - Add labels to content
   * 
   * Adds labels to a piece of content. Does not modify the existing labels.
   * 
   * Notes:
   * 
   * - Labels can also be added when creating content ([Create content](#api-content-post)).
   * - Labels can be updated when updating content ([Update content](#api-content-id-put)).
   * This will delete the existing labels and replace them with the labels in
   * the request.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'addLabelsToContent'(
    parameters?: Parameters<Paths.AddLabelsToContent.PathParameters> | null,
    data?: Paths.AddLabelsToContent.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AddLabelsToContent.Responses.$200>
  /**
   * removeLabelFromContentUsingQueryParameter - Remove label from content using query parameter
   * 
   * Removes a label from a piece of content. Labels can't be deleted from archived content.
   * This is similar to [Remove label from content](#api-content-id-label-label-delete)
   * except that the label name is specified via a query parameter.
   * 
   * Use this method if the label name has "/" characters, as
   * [Remove label from content using query parameter](#api-content-id-label-delete)
   * does not accept "/" characters for the label name.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'removeLabelFromContentUsingQueryParameter'(
    parameters?: Parameters<Paths.RemoveLabelFromContentUsingQueryParameter.PathParameters & Paths.RemoveLabelFromContentUsingQueryParameter.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removeLabelFromContent - Remove label from content
   * 
   * Removes a label from a piece of content. Labels can't be deleted from archived content.
   * This is similar to [Remove label from content using query parameter](#api-content-id-label-delete)
   * except that the label name is specified via a path parameter.
   * 
   * Use this method if the label name does not have "/" characters, as the path
   * parameter does not accept "/" characters for security reasons. Otherwise,
   * use [Remove label from content using query parameter](#api-content-id-label-delete).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'removeLabelFromContent'(
    parameters?: Parameters<Paths.RemoveLabelFromContent.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getWatchesForPage - Get watches for page
   * 
   * Returns the watches for a page. A user that watches a page will receive
   * receive notifications when the page is updated.
   * 
   * If you want to manage watches for a page, use the following `user` methods:
   * 
   * - [Get content watch status for user](#api-user-watch-content-contentId-get)
   * - [Add content watch](#api-user-watch-content-contentId-post)
   * - [Remove content watch](#api-user-watch-content-contentId-delete)
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getWatchesForPage'(
    parameters?: Parameters<Paths.GetWatchesForPage.PathParameters & Paths.GetWatchesForPage.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWatchesForPage.Responses.$200>
  /**
   * getWatchesForSpace - Get watches for space
   * 
   * Returns all space watches for the space that the content is in. A user that
   * watches a space will receive receive notifications when any content in the
   * space is updated.
   * 
   * If you want to manage watches for a space, use the following `user` methods:
   * 
   * - [Get space watch status for user](#api-user-watch-space-spaceKey-get)
   * - [Add space watch](#api-user-watch-space-spaceKey-post)
   * - [Remove space watch](#api-user-watch-space-spaceKey-delete)
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getWatchesForSpace'(
    parameters?: Parameters<Paths.GetWatchesForSpace.PathParameters & Paths.GetWatchesForSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWatchesForSpace.Responses.$200>
  /**
   * copyPageHierarchy - Copy page hierarchy
   * 
   * Copy page hierarchy allows the copying of an entire hierarchy of pages and their associated properties, permissions and attachments.
   *  The id path parameter refers to the content id of the page to copy, and the new parent of this copied page is defined using the destinationPageId in the request body.
   *  The titleOptions object defines the rules of renaming page titles during the copy;
   *  for example, search and replace can be used in conjunction to rewrite the copied page titles.
   * 
   *  Response example:
   *  <pre><code>
   *  {
   *       "id" : "1180606",
   *       "links" : {
   *            "status" : "/rest/api/longtask/1180606"
   *       }
   *  }
   *  </code></pre>
   *  Use the /longtask/<taskId> REST API to get the copy task status.
   */
  'copyPageHierarchy'(
    parameters?: Parameters<Paths.CopyPageHierarchy.PathParameters> | null,
    data?: Paths.CopyPageHierarchy.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CopyPageHierarchy.Responses.$202>
  /**
   * copyPage - Copy single page
   * 
   * Copies a single page and its associated properties, permissions, attachments, and custom contents.
   *  The `id` path parameter refers to the content ID of the page to copy. The target of the page to be copied
   *  is defined using the `destination` in the request body and can be one of the following types.
   * 
   *   - `space`: page will be copied to the specified space as a root page on the space
   *   - `parent_page`: page will be copied as a child of the specified parent page
   *   - `existing_page`: page will be copied and replace the specified page
   * 
   * By default, the following objects are expanded: `space`, `history`, `version`.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: 'Add' permission for the space that the content will be copied in and permission to update the content if copying to an `existing_page`.
   */
  'copyPage'(
    parameters?: Parameters<Paths.CopyPage.PathParameters & Paths.CopyPage.QueryParameters> | null,
    data?: Paths.CopyPage.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CopyPage.Responses.$200>
  /**
   * checkContentPermission - Check content permissions
   * 
   * Check if a user or a group can perform an operation to the specified content. The `operation` to check
   * must be provided. The user’s account ID or the ID of the group can be provided in the `subject` to check
   * permissions against a specified user or group. The following permission checks are done to make sure that the
   * user or group has the proper access:
   * 
   * - site permissions
   * - space permissions
   * - content restrictions
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission) if checking permission for self,
   * otherwise 'Confluence Administrator' global permission is required.
   */
  'checkContentPermission'(
    parameters?: Parameters<Paths.CheckContentPermission.PathParameters> | null,
    data?: Paths.CheckContentPermission.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CheckContentPermission.Responses.$200>
  /**
   * getContentProperties - Get content properties
   * 
   * Returns the properties for a piece of content. For more information
   * about content properties, see [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space, and permission to view the content if it is a page.
   */
  'getContentProperties'(
    parameters?: Parameters<Paths.GetContentProperties.PathParameters & Paths.GetContentProperties.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentProperties.Responses.$200>
  /**
   * createContentProperty - Create content property
   * 
   * Creates a property for an existing piece of content. For more information
   * about content properties, see [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * 
   * This is the same as [Create content property for key](#api-content-id-property-key-post)
   * except that the key is specified in the request body instead of as a
   * path parameter.
   * 
   * Content properties can also be added when creating a new piece of content
   * by including them in the `metadata.properties` of the request.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'createContentProperty'(
    parameters?: Parameters<Paths.CreateContentProperty.PathParameters> | null,
    data?: Paths.CreateContentProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateContentProperty.Responses.$200>
  /**
   * getContentProperty - Get content property
   * 
   * Returns a content property for a piece of content. For more information, see
   * [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space, and permission to view the content if it is a page.
   */
  'getContentProperty'(
    parameters?: Parameters<Paths.GetContentProperty.PathParameters & Paths.GetContentProperty.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentProperty.Responses.$200>
  /**
   * updateContentProperty - Update content property
   * 
   * Updates an existing content property. This method will also create a new
   * property for a piece of content, if the property key does not exist and
   * the property version is 1. For more information about content properties, see
   * [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'updateContentProperty'(
    parameters?: Parameters<Paths.UpdateContentProperty.PathParameters> | null,
    data?: Paths.UpdateContentProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateContentProperty.Responses.$200>
  /**
   * createContentPropertyForKey - Create content property for key
   * 
   * Creates a property for an existing piece of content. For more information
   * about content properties, see [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * 
   * This is the same as [Create content property](#api-content-id-property-post)
   * except that the key is specified as a path parameter instead of in the
   * request body.
   * 
   * Content properties can also be added when creating a new piece of content
   * by including them in the `metadata.properties` of the request.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'createContentPropertyForKey'(
    parameters?: Parameters<Paths.CreateContentPropertyForKey.PathParameters> | null,
    data?: Paths.CreateContentPropertyForKey.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateContentPropertyForKey.Responses.$200>
  /**
   * deleteContentProperty - Delete content property
   * 
   * Deletes a content property. For more information about content properties, see
   * [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'deleteContentProperty'(
    parameters?: Parameters<Paths.DeleteContentProperty.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getRestrictions - Get restrictions
   * 
   * Returns the restrictions on a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content.
   */
  'getRestrictions'(
    parameters?: Parameters<Paths.GetRestrictions.PathParameters & Paths.GetRestrictions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetRestrictions.Responses.$200>
  /**
   * updateRestrictions - Update restrictions
   * 
   * Updates restrictions for a piece of content. This removes the existing
   * restrictions and replaces them with the restrictions in the request.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'updateRestrictions'(
    parameters?: Parameters<Paths.UpdateRestrictions.PathParameters & Paths.UpdateRestrictions.QueryParameters> | null,
    data?: Paths.UpdateRestrictions.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateRestrictions.Responses.$200>
  /**
   * addRestrictions - Add restrictions
   * 
   * Adds restrictions to a piece of content. Note, this does not change any
   * existing restrictions on the content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'addRestrictions'(
    parameters?: Parameters<Paths.AddRestrictions.PathParameters & Paths.AddRestrictions.QueryParameters> | null,
    data?: Paths.AddRestrictions.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AddRestrictions.Responses.$200>
  /**
   * deleteRestrictions - Delete restrictions
   * 
   * Removes all restrictions (read and update) on a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'deleteRestrictions'(
    parameters?: Parameters<Paths.DeleteRestrictions.PathParameters & Paths.DeleteRestrictions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteRestrictions.Responses.$200>
  /**
   * getRestrictionsByOperation - Get restrictions by operation
   * 
   * Returns restrictions on a piece of content by operation. This method is
   * similar to [Get restrictions](#api-content-id-restriction-get) except that
   * the operations are properties of the return object, rather than items in
   * a results array.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content.
   */
  'getRestrictionsByOperation'(
    parameters?: Parameters<Paths.GetRestrictionsByOperation.PathParameters & Paths.GetRestrictionsByOperation.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetRestrictionsByOperation.Responses.$200>
  /**
   * getRestrictionsForOperation - Get restrictions for operation
   * 
   * Returns the restictions on a piece of content for a given operation (read
   * or update).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content.
   */
  'getRestrictionsForOperation'(
    parameters?: Parameters<Paths.GetRestrictionsForOperation.PathParameters & Paths.GetRestrictionsForOperation.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetRestrictionsForOperation.Responses.$200>
  /**
   * getContentRestrictionStatusForGroup - Get content restriction status for group
   * 
   * Deprecated, use [Get content restriction status for group via groupId](https://developer.atlassian.com/cloud/confluence/rest/v1/api-group-content-restrictions/#api-wiki-rest-api-content-id-restriction-byoperation-operationkey-bygroupid-groupid-get).
   * Returns whether the specified content restriction applies to a group.
   * For example, if a page with `id=123` has a `read` restriction for the `admins` group,
   * the following request will return `true`:
   * 
   * `/wiki/rest/api/content/123/restriction/byOperation/read/group/admins`
   * 
   * Note that a response of `true` does not guarantee that the group can view the page, as it does not account for
   * account-inherited restrictions, space permissions, or even product access. For more
   * information, see [Confluence permissions](https://confluence.atlassian.com/x/_AozKw).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content.
   */
  'getContentRestrictionStatusForGroup'(
    parameters?: Parameters<Paths.GetContentRestrictionStatusForGroup.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * addGroupToContentRestriction - Add group to content restriction
   * 
   * Deprecated, use [Add group to content restriction via groupId](https://developer.atlassian.com/cloud/confluence/rest/v1/api-group-content-restrictions/#api-wiki-rest-api-content-id-restriction-byoperation-operationkey-bygroupid-groupid-put).
   * Adds a group to a content restriction. That is, grant read or update
   * permission to the group for a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'addGroupToContentRestriction'(
    parameters?: Parameters<Paths.AddGroupToContentRestriction.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removeGroupFromContentRestrictionById - Remove group from content restriction
   * 
   * Deprecated, use [Remove group from content restriction by groupId](https://developer.atlassian.com/cloud/confluence/rest/v1/api-group-content-restrictions/#api-wiki-rest-api-content-id-restriction-byoperation-operationkey-user-delete).
   * Removes a group from a content restriction. That is, remove read or update
   * permission for the group for a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'removeGroupFromContentRestrictionById'(
    parameters?: Parameters<Paths.RemoveGroupFromContentRestrictionById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getIndividualGroupRestrictionStatusByGroupId - Get content restriction status for group
   * 
   * Returns whether the specified content restriction applies to a group.
   * For example, if a page with `id=123` has a `read` restriction for the `123456` group id,
   * the following request will return `true`:
   * 
   * `/wiki/rest/api/content/123/restriction/byOperation/read/byGroupId/123456`
   * 
   * Note that a response of `true` does not guarantee that the group can view the page, as it does not account for
   * account-inherited restrictions, space permissions, or even product access. For more
   * information, see [Confluence permissions](https://confluence.atlassian.com/x/_AozKw).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content.
   */
  'getIndividualGroupRestrictionStatusByGroupId'(
    parameters?: Parameters<Paths.GetIndividualGroupRestrictionStatusByGroupId.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * addGroupToContentRestrictionByGroupId - Add group to content restriction
   * 
   * Adds a group to a content restriction by Group Id. That is, grant read or update
   * permission to the group for a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'addGroupToContentRestrictionByGroupId'(
    parameters?: Parameters<Paths.AddGroupToContentRestrictionByGroupId.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removeGroupFromContentRestriction - Remove group from content restriction
   * 
   * Removes a group from a content restriction. That is, remove read or update
   * permission for the group for a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'removeGroupFromContentRestriction'(
    parameters?: Parameters<Paths.RemoveGroupFromContentRestriction.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getContentRestrictionStatusForUser - Get content restriction status for user
   * 
   * Returns whether the specified content restriction applies to a user.
   * For example, if a page with `id=123` has a `read` restriction for a user with an account ID of
   * `384093:32b4d9w0-f6a5-3535-11a3-9c8c88d10192`, the following request will return `true`:
   * 
   * `/wiki/rest/api/content/123/restriction/byOperation/read/user?accountId=384093:32b4d9w0-f6a5-3535-11a3-9c8c88d10192`
   * 
   * Note that a response of `true` does not guarantee that the user can view the page, as it does not account for
   * account-inherited restrictions, space permissions, or even product access. For more
   * information, see [Confluence permissions](https://confluence.atlassian.com/x/_AozKw).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content.
   */
  'getContentRestrictionStatusForUser'(
    parameters?: Parameters<Paths.GetContentRestrictionStatusForUser.PathParameters & Paths.GetContentRestrictionStatusForUser.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * addUserToContentRestriction - Add user to content restriction
   * 
   * Adds a user to a content restriction. That is, grant read or update
   * permission to the user for a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'addUserToContentRestriction'(
    parameters?: Parameters<Paths.AddUserToContentRestriction.PathParameters & Paths.AddUserToContentRestriction.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removeUserFromContentRestriction - Remove user from content restriction
   * 
   * Removes a group from a content restriction. That is, remove read or update
   * permission for the group for a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'removeUserFromContentRestriction'(
    parameters?: Parameters<Paths.RemoveUserFromContentRestriction.PathParameters & Paths.RemoveUserFromContentRestriction.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getContentState - Get content state
   * 
   * Gets the current content state of the draft or current version of content. To specify the draft version, set
   * the parameter status to draft, otherwise archived or current will get the relevant published state.
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content.
   */
  'getContentState'(
    parameters?: Parameters<Paths.GetContentState.PathParameters & Paths.GetContentState.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentState.Responses.$200>
  /**
   * setContentState - Set the content state of a content and publishes a new version of the content.
   * 
   * Sets the content state of the content specified and creates a new version
   * (publishes the content without changing the body) of the content with the new state.
   * 
   * You may pass in either an id of a state, or the name and color of a desired new state.
   * If all 3 are passed in, id will be used.
   * If the name and color passed in already exist under the current user's existing custom states, the existing state will be reused.
   * If custom states are disabled in the space of the content (which can be determined by getting the content state space settings of the content's space)
   * then this set will fail.
   * 
   * You may not remove a content state via this PUT request. You must use the DELETE method. A specified state is required in the body of this request.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'setContentState'(
    parameters?: Parameters<Paths.SetContentState.PathParameters & Paths.SetContentState.QueryParameters> | null,
    data?: Paths.SetContentState.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SetContentState.Responses.$200>
  /**
   * removeContentState - Removes the content state of a content and publishes a new version.
   * 
   * Removes the content state of the content specified and creates a new version
   * (publishes the content without changing the body) of the content with the new status.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'removeContentState'(
    parameters?: Parameters<Paths.RemoveContentState.PathParameters & Paths.RemoveContentState.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.RemoveContentState.Responses.$200>
  /**
   * getAvailableContentStates - Gets available content states for content.
   * 
   * Gets content states that are available for the content to be set as.
   * Will return all enabled Space Content States.
   * Will only return most the 3 most recently published custom content states to match UI editor list.
   * To get all custom content states, use the /content-states endpoint.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to edit the content.
   */
  'getAvailableContentStates'(
    parameters?: Parameters<Paths.GetAvailableContentStates.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAvailableContentStates.Responses.$200>
  /**
   * getContentVersions - Get content versions
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns the versions for a piece of content in descending order.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content. If the content is a blog post, 'View' permission
   * for the space is required.
   */
  'getContentVersions'(
    parameters?: Parameters<Paths.GetContentVersions.PathParameters & Paths.GetContentVersions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentVersions.Responses.$200>
  /**
   * restoreContentVersion - Restore content version
   * 
   * Restores a historical version to be the latest version. That is, a new version
   * is created with the content of the historical version.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'restoreContentVersion'(
    parameters?: Parameters<Paths.RestoreContentVersion.PathParameters & Paths.RestoreContentVersion.QueryParameters> | null,
    data?: Paths.RestoreContentVersion.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.RestoreContentVersion.Responses.$200>
  /**
   * getContentVersion - Get content version
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns a version for a piece of content.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content. If the content is a blog post, 'View' permission
   * for the space is required.
   */
  'getContentVersion'(
    parameters?: Parameters<Paths.GetContentVersion.PathParameters & Paths.GetContentVersion.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentVersion.Responses.$200>
  /**
   * deleteContentVersion - Delete content version
   * 
   * Delete a historical version. This does not delete the changes made to the
   * content in that version, rather the changes for the deleted version are
   * rolled up into the next version. Note, you cannot delete the current version.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'deleteContentVersion'(
    parameters?: Parameters<Paths.DeleteContentVersion.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getCustomContentStates - Get Custom Content States
   * 
   * Get custom content states that authenticated user has created.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**
   * Must have user authentication.
   */
  'getCustomContentStates'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCustomContentStates.Responses.$200>
  /**
   * bulkSetContentStates - Bulk set content state of many contents
   * 
   * Creates a long running task that sets content state of draft or published versions of pages specified.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**
   * Content Edit Permission for a content to have its state set via this endpoint.
   */
  'bulkSetContentStates'(
    parameters?: Parameters<Paths.BulkSetContentStates.QueryParameters> | null,
    data?: Paths.BulkSetContentStates.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.BulkSetContentStates.Responses.$200>
  /**
   * bulkRemoveContentStates - Bulk remove content states from content
   * 
   * Creates a long running task that Removes content state from draft or published versions of pages specified.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**
   * Content Edit Permission for a content to have its state removed via this endpoint.
   */
  'bulkRemoveContentStates'(
    parameters?: Parameters<Paths.BulkRemoveContentStates.QueryParameters> | null,
    data?: Paths.BulkRemoveContentStates.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.BulkRemoveContentStates.Responses.$200>
  /**
   * getTaskUpdate - Get update on long running task for setting of content state.
   * 
   * Get Status of long running task that was previously created to set or remove content states from content.
   * User must first create a task by passing in details to /wiki/rest/api/content-states PUT or DELETE endpoints.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**
   * Must have created long running task
   */
  'getTaskUpdate'(
    parameters?: Parameters<Paths.GetTaskUpdate.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetTaskUpdate.Responses.$200>
  /**
   * convertContentBody - Convert content body
   * 
   * Converts a content body from one format to another format.
   * 
   * Supported conversions:
   * 
   * - storage: view, export_view, styled_view, editor
   * - editor: storage
   * - view: none
   * - export_view: none
   * - styled_view: none
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * If request specifies 'contentIdContext', 'View' permission for the space, and permission to view the content.
   */
  'convertContentBody'(
    parameters?: Parameters<Paths.ConvertContentBody.PathParameters & Paths.ConvertContentBody.QueryParameters> | null,
    data?: Paths.ConvertContentBody.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.ConvertContentBody.Responses.$200>
  /**
   * asyncConvertContentBodyRequest - Asynchronously convert content body
   * 
   * Converts a content body from one format to another format asynchronously.
   * Returns the asyncId for the asynchronous task.
   * 
   * Supported conversions:
   * 
   * - storage: export_view
   * 
   * No other conversions are supported at the moment.
   * Once a conversion is completed, it will be available for 5 minutes at the result endpoint.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * If request specifies 'contentIdContext', 'View' permission for the space, and permission to view the content.
   */
  'asyncConvertContentBodyRequest'(
    parameters?: Parameters<Paths.AsyncConvertContentBodyRequest.PathParameters & Paths.AsyncConvertContentBodyRequest.QueryParameters> | null,
    data?: Paths.AsyncConvertContentBodyRequest.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AsyncConvertContentBodyRequest.Responses.$200>
  /**
   * asyncConvertContentBodyResponse - Get asynchronously converted content body from the id or the current status of the task.
   * 
   * Returns the asynchronous content body for the corresponding id if the task is complete 
   * or returns the status of the task.
   * 
   * After the task is completed, the result can be obtained for 5 minutes, or until an identical conversion request is made again,
   * with allowCache query param set to false.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * If request specifies 'contentIdContext', 'View' permission for the space, and permission to view the content.
   */
  'asyncConvertContentBodyResponse'(
    parameters?: Parameters<Paths.AsyncConvertContentBodyResponse.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AsyncConvertContentBodyResponse.Responses.$200>
  /**
   * searchTasks - Get inline tasks based on search parameters
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns inline tasks based on the search query.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission). Only tasks
   * in contents that the user has permission to view are returned.
   */
  'searchTasks'(
    parameters?: Parameters<Paths.SearchTasks.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SearchTasks.Responses.$200>
  /**
   * getTaskById - Get inline task based on global ID
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns inline task based on the global ID.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the content associated with the task.
   */
  'getTaskById'(
    parameters?: Parameters<Paths.GetTaskById.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetTaskById.Responses.$200>
  /**
   * updateTaskById - Update inline task given global ID
   * 
   * Updates an inline tasks status given its global ID
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content associated with the task.
   */
  'updateTaskById'(
    parameters?: Parameters<Paths.UpdateTaskById.PathParameters> | null,
    data?: Paths.UpdateTaskById.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateTaskById.Responses.$200>
  /**
   * getAllLabelContent - Get label information
   * 
   * Returns label information and a list of contents associated with the label.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission). Only contents
   * that the user is permitted to view is returned.
   */
  'getAllLabelContent'(
    parameters?: Parameters<Paths.GetAllLabelContent.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAllLabelContent.Responses.$200>
  /**
   * getGroups - Get groups
   * 
   * Returns all user groups. The returned groups are ordered alphabetically in
   * ascending order by group name.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getGroups'(
    parameters?: Parameters<Paths.GetGroups.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetGroups.Responses.$200>
  /**
   * createGroup - Create new user group
   * 
   * Creates a new user group.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * User must be a site admin.
   */
  'createGroup'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateGroup.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateGroup.Responses.$201>
  /**
   * removeGroup - Delete user group
   * 
   * Delete user group.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * User must be a site admin.
   */
  'removeGroup'(
    parameters?: Parameters<Paths.RemoveGroup.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getGroupByQueryParam - Get group
   * 
   * Returns a user group for a given group name.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getGroupByQueryParam'(
    parameters?: Parameters<Paths.GetGroupByQueryParam.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetGroupByQueryParam.Responses.$200>
  /**
   * getGroupByGroupId - Get group
   * 
   * Returns a user group for a given group id.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getGroupByGroupId'(
    parameters?: Parameters<Paths.GetGroupByGroupId.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetGroupByGroupId.Responses.$200>
  /**
   * removeGroupById - Delete user group
   * 
   * Delete user group.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * User must be a site admin.
   */
  'removeGroupById'(
    parameters?: Parameters<Paths.RemoveGroupById.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getGroupByName - Get group
   * 
   * Returns a user group for a given group name.
   * 
   * Use updated Get group API
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getGroupByName'(
    parameters?: Parameters<Paths.GetGroupByName.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetGroupByName.Responses.$200>
  /**
   * getMembersByQueryParam - Get group members
   * 
   * Returns the users that are members of a group.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getMembersByQueryParam'(
    parameters?: Parameters<Paths.GetMembersByQueryParam.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetMembersByQueryParam.Responses.$200>
  /**
   * getGroupMembers - Get group members
   * 
   * Returns the users that are members of a group.
   * 
   * Use updated Get group API
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getGroupMembers'(
    parameters?: Parameters<Paths.GetGroupMembers.PathParameters & Paths.GetGroupMembers.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetGroupMembers.Responses.$200>
  /**
   * searchGroups - Search groups by partial query
   * 
   * Get search results of groups by partial query provided.
   */
  'searchGroups'(
    parameters?: Parameters<Paths.SearchGroups.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SearchGroups.Responses.$200>
  /**
   * addUserToGroupByGroupId - Add member to group by groupId
   * 
   * Adds a user as a member in a group represented by its groupId
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * User must be a site admin.
   */
  'addUserToGroupByGroupId'(
    parameters?: Parameters<Paths.AddUserToGroupByGroupId.QueryParameters> | null,
    data?: Paths.AddUserToGroupByGroupId.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removeMemberFromGroupByGroupId - Remove member from group using group id
   * 
   * Remove user as a member from a group.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * User must be a site admin.
   */
  'removeMemberFromGroupByGroupId'(
    parameters?: Parameters<Paths.RemoveMemberFromGroupByGroupId.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getGroupMembersByGroupId - Get group members
   * 
   * Returns the users that are members of a group.
   * 
   * Use updated Get group API
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getGroupMembersByGroupId'(
    parameters?: Parameters<Paths.GetGroupMembersByGroupId.PathParameters & Paths.GetGroupMembersByGroupId.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetGroupMembersByGroupId.Responses.$200>
  /**
   * addUserToGroup - Add member to group
   * 
   * Adds a user as a member in a group.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * User must be a site admin.
   */
  'addUserToGroup'(
    parameters?: Parameters<Paths.AddUserToGroup.QueryParameters> | null,
    data?: Paths.AddUserToGroup.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removeMemberFromGroup - Remove member from group
   * 
   * Remove user as a member from a group.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * User must be a site admin.
   */
  'removeMemberFromGroup'(
    parameters?: Parameters<Paths.RemoveMemberFromGroup.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getTasks - Get long-running tasks
   * 
   * Returns information about all active long-running tasks (e.g. space export),
   * such as how long each task has been running and the percentage of each task
   * that has completed.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getTasks'(
    parameters?: Parameters<Paths.GetTasks.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetTasks.Responses.$200>
  /**
   * getTask - Get long-running task
   * 
   * Returns information about an active long-running task (e.g. space export),
   * such as how long it has been running and the percentage of the task that
   * has completed.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getTask'(
    parameters?: Parameters<Paths.GetTask.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetTask.Responses.$200>
  /**
   * findTargetFromSource - Find target entities related to a source entity
   * 
   * Returns all target entities that have a particular relationship to the
   * source entity. Note, relationships are one way.
   * 
   * For example, the following method finds all content that the current user
   * has an 'ignore' relationship with:
   * `GET /wiki/rest/api/relation/ignore/from/user/current/to/content`
   * Note, 'ignore' is an example custom relationship type.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view both the target entity and source entity.
   */
  'findTargetFromSource'(
    parameters?: Parameters<Paths.FindTargetFromSource.PathParameters & Paths.FindTargetFromSource.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.FindTargetFromSource.Responses.$200>
  /**
   * getRelationship - Find relationship from source to target
   * 
   * Find whether a particular type of relationship exists from a source
   * entity to a target entity. Note, relationships are one way.
   * 
   * For example, you can use this method to find whether the current user has
   * selected a particular page as a favorite (i.e. 'save for later'):
   * `GET /wiki/rest/api/relation/favourite/from/user/current/to/content/123`
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view both the target entity and source entity.
   */
  'getRelationship'(
    parameters?: Parameters<Paths.GetRelationship.PathParameters & Paths.GetRelationship.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetRelationship.Responses.$200>
  /**
   * createRelationship - Create relationship
   * 
   * Creates a relationship between two entities (user, space, content). The
   * 'favourite' relationship is supported by default, but you can use this method
   * to create any type of relationship between two entities.
   * 
   * For example, the following method creates a 'sibling' relationship between
   * two pieces of content:
   * `GET /wiki/rest/api/relation/sibling/from/content/123/to/content/456`
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'createRelationship'(
    parameters?: Parameters<Paths.CreateRelationship.PathParameters & Paths.CreateRelationship.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateRelationship.Responses.$200>
  /**
   * deleteRelationship - Delete relationship
   * 
   * Deletes a relationship between two entities (user, space, content).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * For favourite relationships, the current user can only delete their own
   * favourite relationships. A space administrator can delete favourite
   * relationships for any user.
   */
  'deleteRelationship'(
    parameters?: Parameters<Paths.DeleteRelationship.PathParameters & Paths.DeleteRelationship.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * findSourcesForTarget - Find source entities related to a target entity
   * 
   * Returns all target entities that have a particular relationship to the
   * source entity. Note, relationships are one way.
   * 
   * For example, the following method finds all users that have a 'collaborator'
   * relationship to a piece of content with an ID of '1234':
   * `GET /wiki/rest/api/relation/collaborator/to/content/1234/from/user`
   * Note, 'collaborator' is an example custom relationship type.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view both the target entity and source entity.
   */
  'findSourcesForTarget'(
    parameters?: Parameters<Paths.FindSourcesForTarget.PathParameters & Paths.FindSourcesForTarget.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.FindSourcesForTarget.Responses.$200>
  /**
   * searchByCQL - Search content
   * 
   * Searches for content using the
   * [Confluence Query Language (CQL)](https://developer.atlassian.com/cloud/confluence/advanced-searching-using-cql/).
   * 
   * **Note that CQL input queries submitted through the `/wiki/rest/api/search` endpoint no longer support user-specific fields like `user`, `user.fullname`, `user.accountid`, and `user.userkey`.** 
   * See this [deprecation notice](https://developer.atlassian.com/cloud/confluence/deprecation-notice-search-api/) for more details.
   * 
   * Example initial call:
   * ```
   * /wiki/rest/api/search?cql=type=page&limit=25
   * ```
   * 
   * Example response:
   * ```
   * {
   *   "results": [
   *     { ... },
   *     { ... },
   *     ...
   *     { ... }
   *   ],
   *   "limit": 25,
   *   "size": 25,
   *   ...
   *   "_links": {
   *     "base": "<url>",
   *     "context": "<url>",
   *     "next": "/rest/api/search?cql=type=page&limit=25&cursor=raNDoMsTRiNg",
   *     "self": "<url>"
   *   }
   * }
   * ```
   * 
   * When additional results are available, returns `next` and `prev` URLs to retrieve them in subsequent calls. The URLs each contain a cursor that points to the appropriate set of results. Use `limit` to specify the number of results returned in each call.
   * 
   * Example subsequent call (taken from example response):
   * ```
   * /wiki/rest/api/search?cql=type=page&limit=25&cursor=raNDoMsTRiNg
   * ```
   * The response to this will have a `prev` URL similar to the `next` in the example response.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to view the entities. Note, only entities that the user has
   * permission to view will be returned.
   */
  'searchByCQL'(
    parameters?: Parameters<Paths.SearchByCQL.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SearchByCQL.Responses.$200>
  /**
   * searchUser - Search users
   * 
   * Searches for users using user-specific queries from the
   * [Confluence Query Language (CQL)](https://developer.atlassian.com/cloud/confluence/advanced-searching-using-cql/).
   * 
   * Note that CQL input queries submitted through the `/wiki/rest/api/search/user` endpoint only support user-specific fields like `user`, `user.fullname`, `user.accountid`, and `user.userkey`.
   * 
   * Note that some user fields may be set to null depending on the user's privacy settings.
   * These are: email, profilePicture, displayName, and timeZone.
   */
  'searchUser'(
    parameters?: Parameters<Paths.SearchUser.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SearchUser.Responses.$200>
  /**
   * getLookAndFeelSettings - Get look and feel settings
   * 
   * Returns the look and feel settings for the site or a single space. This
   * includes attributes such as the color scheme, padding, and border radius.
   * 
   * The look and feel settings for a space can be inherited from the global
   * look and feel settings or provided by a theme.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * None
   */
  'getLookAndFeelSettings'(
    parameters?: Parameters<Paths.GetLookAndFeelSettings.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetLookAndFeelSettings.Responses.$200>
  /**
   * updateLookAndFeel - Select look and feel settings
   * 
   * Sets the look and feel settings to the default (global) settings, the
   * custom settings, or the current theme's settings for a space.
   * The custom and theme settings can only be selected if there is already
   * a theme set for a space. Note, the default space settings are inherited
   * from the current global settings.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'updateLookAndFeel'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.UpdateLookAndFeel.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateLookAndFeel.Responses.$200>
  /**
   * updateLookAndFeelSettings - Update look and feel settings
   * 
   * Updates the look and feel settings for the site or for a single space.
   * If custom settings exist, they are updated. If no custom settings exist,
   * then a set of custom settings is created.
   * 
   * Note, if a theme is selected for a space, the space look and feel settings
   * are provided by the theme and cannot be overridden.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'updateLookAndFeelSettings'(
    parameters?: Parameters<Paths.UpdateLookAndFeelSettings.QueryParameters> | null,
    data?: Paths.UpdateLookAndFeelSettings.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateLookAndFeelSettings.Responses.$200>
  /**
   * resetLookAndFeelSettings - Reset look and feel settings
   * 
   * Resets the custom look and feel settings for the site or a single space.
   * This changes the values of the custom settings to be the same as the
   * default settings. It does not change which settings (default or custom)
   * are selected. Note, the default space settings are inherited from the
   * current global settings.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'resetLookAndFeelSettings'(
    parameters?: Parameters<Paths.ResetLookAndFeelSettings.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * setLookAndFeelSettings - Set look and feel settings
   * 
   * Sets the look and feel settings to either the default settings or the
   * custom settings, for the site or a single space. Note, the default
   * space settings are inherited from the current global settings.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'setLookAndFeelSettings'(
    parameters?: Parameters<Paths.SetLookAndFeelSettings.QueryParameters> | null,
    data?: Paths.SetLookAndFeelSettings.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SetLookAndFeelSettings.Responses.$200>
  /**
   * getSystemInfo - Get system info
   * 
   * Returns the system information for the Confluence Cloud tenant. This
   * information is used by Atlassian.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getSystemInfo'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSystemInfo.Responses.$200>
  /**
   * getThemes - Get themes
   * 
   * Returns all themes, not including the default theme.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: None
   */
  'getThemes'(
    parameters?: Parameters<Paths.GetThemes.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetThemes.Responses.$200>
  /**
   * getGlobalTheme - Get global theme
   * 
   * Returns the globally assigned theme.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: None
   */
  'getGlobalTheme'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetGlobalTheme.Responses.$200>
  /**
   * getTheme - Get theme
   * 
   * Returns a theme. This includes information about the theme name,
   * description, and icon.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: None
   */
  'getTheme'(
    parameters?: Parameters<Paths.GetTheme.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetTheme.Responses.$200>
  /**
   * getSpaces - Get spaces
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns all spaces. The returned spaces are ordered alphabetically in
   * ascending order by space key.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   * Note, the returned list will only contain spaces that the current user
   * has permission to view.
   */
  'getSpaces'(
    parameters?: Parameters<Paths.GetSpaces.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaces.Responses.$200>
  /**
   * createSpace - Create space
   * 
   * Creates a new space. Note, currently you cannot set space labels when
   * creating a space.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Create Space(s)' global permission.
   */
  'createSpace'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateSpace.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateSpace.Responses.$200>
  /**
   * createPrivateSpace - Create private space
   * 
   * Creates a new space that is only visible to the creator. This method is
   * the same as the [Create space](#api-space-post) method with permissions
   * set to the current user only. Note, currently you cannot set space
   * labels when creating a space.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Create Space(s)' global permission.
   */
  'createPrivateSpace'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreatePrivateSpace.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreatePrivateSpace.Responses.$200>
  /**
   * getSpace - Get space
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns a space. This includes information like the name, description,
   * and permissions, but not the content in the space.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space.
   */
  'getSpace'(
    parameters?: Parameters<Paths.GetSpace.PathParameters & Paths.GetSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpace.Responses.$200>
  /**
   * updateSpace - Update space
   * 
   * Updates the name, description, or homepage of a space.
   * 
   * -   For security reasons, permissions cannot be updated via the API and
   * must be changed via the user interface instead.
   * -   Currently you cannot set space labels when updating a space.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'updateSpace'(
    parameters?: Parameters<Paths.UpdateSpace.PathParameters> | null,
    data?: Paths.UpdateSpace.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateSpace.Responses.$200>
  /**
   * deleteSpace - Delete space
   * 
   * Deletes a space. Note, the space will be deleted in a long running task.
   * Therefore, the space may not be deleted yet when this method has
   * returned. Clients should poll the status link that is returned in the
   * response until the task completes.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'deleteSpace'(
    parameters?: Parameters<Paths.DeleteSpace.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteSpace.Responses.$202>
  /**
   * getContentForSpace - Get content for space
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns all content in a space. The returned content is grouped by type
   * (pages then blogposts), then ordered by content ID in ascending order.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space. Note, the returned list will only
   * contain content that the current user has permission to view.
   */
  'getContentForSpace'(
    parameters?: Parameters<Paths.GetContentForSpace.PathParameters & Paths.GetContentForSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentForSpace.Responses.$200>
  /**
   * addPermissionToSpace - Add new permission to space
   * 
   * Adds new permission to space.
   * 
   * If the permission to be added is a group permission, the group can be identified
   * by its group name or group id.
   * 
   * Note: Apps cannot access this REST resource - including when utilizing user impersonation.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'addPermissionToSpace'(
    parameters?: Parameters<Paths.AddPermissionToSpace.PathParameters> | null,
    data?: Paths.AddPermissionToSpace.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AddPermissionToSpace.Responses.$200>
  /**
   * addCustomContentPermissions - Add new custom content permission to space
   * 
   * Adds new custom content permission to space.
   * 
   * If the permission to be added is a group permission, the group can be identified
   * by its group name or group id.
   * 
   * Note: Only apps can access this REST resource and only make changes to the respective app permissions.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'addCustomContentPermissions'(
    parameters?: Parameters<Paths.AddCustomContentPermissions.PathParameters> | null,
    data?: Paths.AddCustomContentPermissions.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removePermission - Remove a space permission
   * 
   * Removes a space permission. Note that removing Read Space permission for a user or group will remove all
   * the space permissions for that user or group.
   * 
   * Note: Apps cannot access this REST resource - including when utilizing user impersonation.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'removePermission'(
    parameters?: Parameters<Paths.RemovePermission.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getContentByTypeForSpace - Get content by type for space
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns all content of a given type, in a space. The returned content is
   * ordered by content ID in ascending order.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space. Note, the returned list will only
   * contain content that the current user has permission to view.
   */
  'getContentByTypeForSpace'(
    parameters?: Parameters<Paths.GetContentByTypeForSpace.PathParameters & Paths.GetContentByTypeForSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentByTypeForSpace.Responses.$200>
  /**
   * getSpaceProperties - Get space properties
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns all properties for the given space. Space properties are a key-value storage associated with a space.
   * 
   * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**: ‘View’ permission for the space.
   */
  'getSpaceProperties'(
    parameters?: Parameters<Paths.GetSpaceProperties.PathParameters & Paths.GetSpaceProperties.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaceProperties.Responses.$200>
  /**
   * createSpaceProperty - Create space property
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Creates a new space property.
   * 
   * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**:
   * ‘Admin’ permission for the space.
   */
  'createSpaceProperty'(
    parameters?: Parameters<Paths.CreateSpaceProperty.PathParameters> | null,
    data?: Paths.CreateSpaceProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateSpaceProperty.Responses.$200>
  /**
   * getSpaceProperty - Get space property
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Returns a space property.
   * 
   * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**: ‘View’ permission for the space.
   */
  'getSpaceProperty'(
    parameters?: Parameters<Paths.GetSpaceProperty.PathParameters & Paths.GetSpaceProperty.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaceProperty.Responses.$200>
  /**
   * updateSpaceProperty - Update space property
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Updates a space property. Note, you cannot update the key of a space
   * property, only the value.
   * 
   * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**:
   * ‘Admin’ permission for the space.
   */
  'updateSpaceProperty'(
    parameters?: Parameters<Paths.UpdateSpaceProperty.PathParameters> | null,
    data?: Paths.UpdateSpaceProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateSpaceProperty.Responses.$200>
  /**
   * createSpacePropertyForKey - Create space property for key
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Creates a new space property. This is the same as `POST
   * /wiki/rest/api/space/{spaceKey}/property` but the key for the property is passed as a
   * path parameter, rather than in the request body.
   * 
   * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**:
   * ‘Admin’ permission for the space.
   */
  'createSpacePropertyForKey'(
    parameters?: Parameters<Paths.CreateSpacePropertyForKey.PathParameters> | null,
    data?: Paths.CreateSpacePropertyForKey.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateSpacePropertyForKey.Responses.$200>
  /**
   * deleteSpaceProperty - Delete space property
   * 
   * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
   * 
   * Deletes a space property.
   * 
   * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**:
   * ‘Admin’ permission for the space.
   */
  'deleteSpaceProperty'(
    parameters?: Parameters<Paths.DeleteSpaceProperty.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getSpaceSettings - Get space settings
   * 
   * Returns the settings of a space. Currently only the
   * `routeOverrideEnabled` setting can be returned.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space.
   */
  'getSpaceSettings'(
    parameters?: Parameters<Paths.GetSpaceSettings.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaceSettings.Responses.$200>
  /**
   * updateSpaceSettings - Update space settings
   * 
   * Updates the settings for a space. Currently only the
   * `routeOverrideEnabled` setting can be updated.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'updateSpaceSettings'(
    parameters?: Parameters<Paths.UpdateSpaceSettings.PathParameters> | null,
    data?: Paths.UpdateSpaceSettings.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateSpaceSettings.Responses.$200>
  /**
   * getSpaceContentStates - Get space suggested content states
   * 
   * Get content states that are suggested in the space.
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Space view permission
   */
  'getSpaceContentStates'(
    parameters?: Parameters<Paths.GetSpaceContentStates.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaceContentStates.Responses.$200>
  /**
   * getContentStateSettings - Get content state settings for space
   * 
   * Get object describing whether content states are allowed at all, if custom content states or space content states
   * are restricted, and a list of space content states allowed for the space if they are not restricted.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Space admin permission
   */
  'getContentStateSettings'(
    parameters?: Parameters<Paths.GetContentStateSettings.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentStateSettings.Responses.$200>
  /**
   * getContentsWithState - Get content in space with given content state
   * 
   * Finds paginated content with 
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Space View Permission
   */
  'getContentsWithState'(
    parameters?: Parameters<Paths.GetContentsWithState.PathParameters & Paths.GetContentsWithState.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentsWithState.Responses.$200>
  /**
   * getSpaceTheme - Get space theme
   * 
   * Returns the theme selected for a space, if one is set. If no space
   * theme is set, this means that the space is inheriting the global look
   * and feel settings.
   * 
   * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**: ‘View’ permission for the space.
   */
  'getSpaceTheme'(
    parameters?: Parameters<Paths.GetSpaceTheme.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSpaceTheme.Responses.$200>
  /**
   * setSpaceTheme - Set space theme
   * 
   * Sets the theme for a space. Note, if you want to reset the space theme to
   * the default Confluence theme, use the 'Reset space theme' method instead
   * of this method.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'setSpaceTheme'(
    parameters?: Parameters<Paths.SetSpaceTheme.PathParameters> | null,
    data?: Paths.SetSpaceTheme.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.SetSpaceTheme.Responses.$200>
  /**
   * resetSpaceTheme - Reset space theme
   * 
   * Resets the space theme. This means that the space will inherit the
   * global look and feel settings
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space.
   */
  'resetSpaceTheme'(
    parameters?: Parameters<Paths.ResetSpaceTheme.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getWatchersForSpace - Get space watchers
   * 
   * Returns a list of watchers of a space
   */
  'getWatchersForSpace'(
    parameters?: Parameters<Paths.GetWatchersForSpace.PathParameters & Paths.GetWatchersForSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWatchersForSpace.Responses.$200>
  /**
   * getLabelsForSpace - Get Space Labels
   * 
   * Returns a list of labels associated with a space. Can provide a prefix as well as other filters to
   * select different types of labels.
   */
  'getLabelsForSpace'(
    parameters?: Parameters<Paths.GetLabelsForSpace.PathParameters & Paths.GetLabelsForSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetLabelsForSpace.Responses.$200>
  /**
   * addLabelsToSpace - Add labels to a space
   * 
   * Adds labels to a piece of content. Does not modify the existing labels.
   * 
   * Notes:
   * 
   * - Labels can also be added when creating content ([Create content](#api-content-post)).
   * - Labels can be updated when updating content ([Update content](#api-content-id-put)).
   * This will delete the existing labels and replace them with the labels in
   * the request.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to update the content.
   */
  'addLabelsToSpace'(
    parameters?: Parameters<Paths.AddLabelsToSpace.PathParameters> | null,
    data?: Paths.AddLabelsToSpace.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.AddLabelsToSpace.Responses.$200>
  /**
   * deleteLabelFromSpace - Remove label from a space
   */
  'deleteLabelFromSpace'(
    parameters?: Parameters<Paths.DeleteLabelFromSpace.PathParameters & Paths.DeleteLabelFromSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * updateContentTemplate - Update content template
   * 
   * Updates a content template. Note, blueprint templates cannot be updated
   * via the REST API.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space to update a space template or 'Confluence Administrator'
   * global permission to update a global template.
   */
  'updateContentTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.UpdateContentTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateContentTemplate.Responses.$200>
  /**
   * createContentTemplate - Create content template
   * 
   * Creates a new content template. Note, blueprint templates cannot be created via the REST API.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Admin' permission for the space to create a space template or 'Confluence Administrator'
   * global permission to create a global template.
   */
  'createContentTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.CreateContentTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateContentTemplate.Responses.$200>
  /**
   * getBlueprintTemplates - Get blueprint templates
   * 
   * Returns all templates provided by blueprints. Use this method to retrieve
   * all global blueprint templates or all blueprint templates in a space.
   * 
   * Note, all global blueprints are inherited by each space. Space blueprints
   * can be customised without affecting the global blueprints.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space to view blueprints for the space and permission
   * to access the Confluence site ('Can use' global permission) to view global blueprints.
   */
  'getBlueprintTemplates'(
    parameters?: Parameters<Paths.GetBlueprintTemplates.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBlueprintTemplates.Responses.$200>
  /**
   * getContentTemplates - Get content templates
   * 
   * Returns all content templates. Use this method to retrieve all global
   * content templates or all content templates in a space.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space to view space templates and permission to
   * access the Confluence site ('Can use' global permission) to view global templates.
   */
  'getContentTemplates'(
    parameters?: Parameters<Paths.GetContentTemplates.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentTemplates.Responses.$200>
  /**
   * getContentTemplate - Get content template
   * 
   * Returns a content template. This includes information about template,
   * like the name, the space or blueprint that the template is in, the body
   * of the template, and more.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'View' permission for the space to view space templates and permission to
   * access the Confluence site ('Can use' global permission) to view global templates.
   */
  'getContentTemplate'(
    parameters?: Parameters<Paths.GetContentTemplate.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentTemplate.Responses.$200>
  /**
   * removeTemplate - Remove template
   * 
   * Deletes a template. This results in different actions depending on the
   * type of template:
   * 
   * - If the template is a content template, it is deleted.
   * - If the template is a modified space-level blueprint template, it reverts
   * to the template inherited from the global-level blueprint template.
   * - If the template is a modified global-level blueprint template, it reverts
   * to the default global-level blueprint template.
   * 
   *  Note, unmodified blueprint templates cannot be deleted.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   *         'Admin' permission for the space to delete a space template or 'Confluence Administrator'
   *         global permission to delete a global template.
   */
  'removeTemplate'(
    parameters?: Parameters<Paths.RemoveTemplate.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getUser - Get user
   * 
   * Returns a user. This includes information about the user, such as the
   * display name, account ID, profile picture, and more. The information returned may be
   * restricted by the user's profile visibility settings.
   * 
   * **Note:** to add, edit, or delete users in your organization, see the
   * [user management REST API](/cloud/admin/user-management/about/).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getUser'(
    parameters?: Parameters<Paths.GetUser.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetUser.Responses.$200>
  /**
   * getAnonymousUser - Get anonymous user
   * 
   * Returns information about how anonymous users are represented, like the
   * profile picture and display name.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getAnonymousUser'(
    parameters?: Parameters<Paths.GetAnonymousUser.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAnonymousUser.Responses.$200>
  /**
   * getCurrentUser - Get current user
   * 
   * Returns the currently logged-in user. This includes information about
   * the user, like the display name, userKey, account ID, profile picture,
   * and more.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getCurrentUser'(
    parameters?: Parameters<Paths.GetCurrentUser.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCurrentUser.Responses.$200>
  /**
   * getGroupMembershipsForUser - Get group memberships for user
   * 
   * Returns the groups that a user is a member of.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getGroupMembershipsForUser'(
    parameters?: Parameters<Paths.GetGroupMembershipsForUser.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetGroupMembershipsForUser.Responses.$200>
  /**
   * getBulkUserLookup - Get multiple users using ids
   * 
   * Returns user details for the ids provided in request.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getBulkUserLookup'(
    parameters?: Parameters<Paths.GetBulkUserLookup.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetBulkUserLookup.Responses.$200>
  /**
   * getContentWatchStatus - Get content watch status
   * 
   * Returns whether a user is watching a piece of content. Choose the user by
   * doing one of the following:
   * 
   * - Specify a user via a query parameter: Use the `accountId` to identify the user.
   * - Do not specify a user: The currently logged-in user will be used.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission if specifying a user, otherwise
   * permission to access the Confluence site ('Can use' global permission).
   */
  'getContentWatchStatus'(
    parameters?: Parameters<Paths.GetContentWatchStatus.PathParameters & Paths.GetContentWatchStatus.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetContentWatchStatus.Responses.$200>
  /**
   * addContentWatcher - Add content watcher
   * 
   * Adds a user as a watcher to a piece of content. Choose the user by doing
   * one of the following:
   * 
   * - Specify a user via a query parameter: Use the `accountId` to identify the user.
   * - Do not specify a user: The currently logged-in user will be used.
   * 
   * Note, you must add the `X-Atlassian-Token: no-check` header when making a
   * request, as this operation has XSRF protection.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission if specifying a user, otherwise
   * permission to access the Confluence site ('Can use' global permission).
   */
  'addContentWatcher'(
    parameters?: Parameters<Paths.AddContentWatcher.PathParameters & Paths.AddContentWatcher.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removeContentWatcher - Remove content watcher
   * 
   * Removes a user as a watcher from a piece of content. Choose the user by
   * doing one of the following:
   * 
   * - Specify a user via a query parameter: Use the `accountId` to identify the user.
   * - Do not specify a user: The currently logged-in user will be used.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission if specifying a user, otherwise
   * permission to access the Confluence site ('Can use' global permission).
   */
  'removeContentWatcher'(
    parameters?: Parameters<Paths.RemoveContentWatcher.PathParameters & Paths.RemoveContentWatcher.QueryParameters & Paths.RemoveContentWatcher.HeaderParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * isWatchingLabel - Get label watch status
   * 
   * Returns whether a user is watching a label. Choose the user by doing one
   * of the following:
   * 
   * - Specify a user via a query parameter: Use the `accountId` to identify the user.
   * - Do not specify a user: The currently logged-in user will be used.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission if specifying a user, otherwise
   * permission to access the Confluence site ('Can use' global permission).
   */
  'isWatchingLabel'(
    parameters?: Parameters<Paths.IsWatchingLabel.PathParameters & Paths.IsWatchingLabel.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.IsWatchingLabel.Responses.$200>
  /**
   * addLabelWatcher - Add label watcher
   * 
   * Adds a user as a watcher to a label. Choose the user by doing one of the
   * following:
   * 
   * - Specify a user via a query parameter: Use the `accountId` to identify the user.
   * - Do not specify a user: The currently logged-in user will be used.
   * 
   * Note, you must add the `X-Atlassian-Token: no-check` header when making a
   * request, as this operation has XSRF protection.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission if specifying a user, otherwise
   * permission to access the Confluence site ('Can use' global permission).
   */
  'addLabelWatcher'(
    parameters?: Parameters<Paths.AddLabelWatcher.PathParameters & Paths.AddLabelWatcher.QueryParameters & Paths.AddLabelWatcher.HeaderParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removeLabelWatcher - Remove label watcher
   * 
   * Removes a user as a watcher from a label. Choose the user by doing one of
   * the following:
   * 
   * - Specify a user via a query parameter: Use the `accountId` to identify the user.
   * - Do not specify a user: The currently logged-in user will be used.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission if specifying a user, otherwise
   * permission to access the Confluence site ('Can use' global permission).
   */
  'removeLabelWatcher'(
    parameters?: Parameters<Paths.RemoveLabelWatcher.PathParameters & Paths.RemoveLabelWatcher.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * isWatchingSpace - Get space watch status
   * 
   * Returns whether a user is watching a space. Choose the user by
   * doing one of the following:
   * 
   * - Specify a user via a query parameter: Use the `accountId` to identify the user.
   * - Do not specify a user: The currently logged-in user will be used.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission if specifying a user, otherwise
   * permission to access the Confluence site ('Can use' global permission).
   */
  'isWatchingSpace'(
    parameters?: Parameters<Paths.IsWatchingSpace.PathParameters & Paths.IsWatchingSpace.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.IsWatchingSpace.Responses.$200>
  /**
   * addSpaceWatcher - Add space watcher
   * 
   * Adds a user as a watcher to a space. Choose the user by doing one of the
   * following:
   * 
   * - Specify a user via a query parameter: Use the `accountId` to identify the user.
   * - Do not specify a user: The currently logged-in user will be used.
   * 
   * Note, you must add the `X-Atlassian-Token: no-check` header when making a
   * request, as this operation has XSRF protection.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission if specifying a user, otherwise
   * permission to access the Confluence site ('Can use' global permission).
   */
  'addSpaceWatcher'(
    parameters?: Parameters<Paths.AddSpaceWatcher.PathParameters & Paths.AddSpaceWatcher.QueryParameters & Paths.AddSpaceWatcher.HeaderParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removeSpaceWatch - Remove space watch
   * 
   * Removes a user as a watcher from a space. Choose the user by doing one of
   * the following:
   * 
   * - Specify a user via a query parameter: Use the `accountId` to identify the user.
   * - Do not specify a user: The currently logged-in user will be used.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * 'Confluence Administrator' global permission if specifying a user, otherwise
   * permission to access the Confluence site ('Can use' global permission).
   */
  'removeSpaceWatch'(
    parameters?: Parameters<Paths.RemoveSpaceWatch.PathParameters & Paths.RemoveSpaceWatch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getPrivacyUnsafeUserEmail - Get user email address
   * 
   * Returns a user's email address. This API is only available to apps approved by
   * Atlassian, according to these [guidelines](https://community.developer.atlassian.com/t/guidelines-for-requesting-access-to-email-address/27603).
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getPrivacyUnsafeUserEmail'(
    parameters?: Parameters<Paths.GetPrivacyUnsafeUserEmail.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPrivacyUnsafeUserEmail.Responses.$200>
  /**
   * getPrivacyUnsafeUserEmailBulk - Get user email addresses in batch
   * 
   * Returns user email addresses for a set of accountIds. This API is only available to apps approved by
   * Atlassian, according to these [guidelines](https://community.developer.atlassian.com/t/guidelines-for-requesting-access-to-email-address/27603).
   * 
   * Any accounts which are not available will not be included in the result.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getPrivacyUnsafeUserEmailBulk'(
    parameters?: Parameters<Paths.GetPrivacyUnsafeUserEmailBulk.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPrivacyUnsafeUserEmailBulk.Responses.$200>
  /**
   * getModules - Get modules
   * 
   * Returns all modules registered dynamically by the calling app.
   * 
   * **[Permissions](#permissions) required:** Only Connect apps can make this request.
   */
  'getModules'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * registerModules - Register modules
   * 
   * Registers a list of modules. For the list of modules that support dynamic registration, see [Dynamic modules](https://developer.atlassian.com/cloud/confluence/dynamic-modules/).
   * 
   * **[Permissions](#permissions) required:** Only Connect apps can make this request.
   */
  'registerModules'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * removeModules - Remove modules
   * 
   * Remove all or a list of modules registered by the calling app.
   * 
   * **[Permissions](#permissions) required:** Only Connect apps can make this request.
   */
  'removeModules'(
    parameters?: Parameters<Paths.RemoveModules.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getViews - Get views
   * 
   * Get the total number of views a piece of content has.
   */
  'getViews'(
    parameters?: Parameters<Paths.GetViews.PathParameters & Paths.GetViews.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetViews.Responses.$200>
  /**
   * getViewers - Get viewers
   * 
   * Get the total number of distinct viewers a piece of content has.
   */
  'getViewers'(
    parameters?: Parameters<Paths.GetViewers.PathParameters & Paths.GetViewers.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetViewers.Responses.$200>
  /**
   * getUserProperties - Get user properties
   * 
   * Returns the properties for a user as list of property keys. For more information
   * about user properties, see [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * `Note`, these properties stored against a user are on a Confluence site level and not space/content level.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getUserProperties'(
    parameters?: Parameters<Paths.GetUserProperties.PathParameters & Paths.GetUserProperties.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetUserProperties.Responses.$200>
  /**
   * getUserProperty - Get user property
   * 
   * Returns the property corresponding to `key` for a user. For more information
   * about user properties, see [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * `Note`, these properties stored against a user are on a Confluence site level and not space/content level.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'getUserProperty'(
    parameters?: Parameters<Paths.GetUserProperty.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetUserProperty.Responses.$200>
  /**
   * updateUserProperty - Update user property
   * 
   * Updates a property for the given user. Note, you cannot update the key of a user property, only the value.
   * For more information about user properties, see
   * [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * `Note`, these properties stored against a user are on a Confluence site level and not space/content level.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'updateUserProperty'(
    parameters?: Parameters<Paths.UpdateUserProperty.PathParameters> | null,
    data?: Paths.UpdateUserProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.UpdateUserProperty.Responses.$204>
  /**
   * createUserProperty - Create user property by key
   * 
   * Creates a property for a user. For more information  about user properties, see [Confluence entity properties]
   * (https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * `Note`, these properties stored against a user are on a Confluence site level and not space/content level.
   * 
   * `Note:` the number of properties which could be created per app in a tenant for each user might be
   * restricted by fixed system limits.
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'createUserProperty'(
    parameters?: Parameters<Paths.CreateUserProperty.PathParameters> | null,
    data?: Paths.CreateUserProperty.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.CreateUserProperty.Responses.$201>
  /**
   * deleteUserProperty - Delete user property
   * 
   * Deletes a property for the given user.
   * For more information about user properties, see
   * [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
   * `Note`, these properties stored against a user are on a Confluence site level and not space/content level.
   * 
   * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
   * Permission to access the Confluence site ('Can use' global permission).
   */
  'deleteUserProperty'(
    parameters?: Parameters<Paths.DeleteUserProperty.PathParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.DeleteUserProperty.Responses.$204>
}

export interface PathsDictionary {
  ['/wiki/rest/api/audit']: {
    /**
     * getAuditRecords - Get audit records
     * 
     * Returns all records in the audit log, optionally for a certain date range.
     * This contains information about events like space exports, group membership
     * changes, app installations, etc. For more information, see
     * [Audit log](https://confluence.atlassian.com/confcloud/audit-log-802164269.html)
     * in the Confluence administrator's guide.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission.
     */
    'get'(
      parameters?: Parameters<Paths.GetAuditRecords.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAuditRecords.Responses.$200>
    /**
     * createAuditRecord - Create audit record
     * 
     * Creates a record in the audit log.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateAuditRecord.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateAuditRecord.Responses.$200>
  }
  ['/wiki/rest/api/audit/export']: {
    /**
     * exportAuditRecords - Export audit records
     * 
     * Exports audit records as a CSV file or ZIP file.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission.
     */
    'get'(
      parameters?: Parameters<Paths.ExportAuditRecords.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.ExportAuditRecords.Responses.$200>
  }
  ['/wiki/rest/api/audit/retention']: {
    /**
     * getRetentionPeriod - Get retention period
     * 
     * Returns the retention period for records in the audit log. The retention
     * period is how long an audit record is kept for, from creation date until
     * it is deleted.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetRetentionPeriod.Responses.$200>
    /**
     * setRetentionPeriod - Set retention period
     * 
     * Sets the retention period for records in the audit log. The retention period
     * can be set to a maximum of 1 year.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission.
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.SetRetentionPeriod.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SetRetentionPeriod.Responses.$200>
  }
  ['/wiki/rest/api/audit/since']: {
    /**
     * getAuditRecordsForTimePeriod - Get audit records for time period
     * 
     * Returns records from the audit log, for a time period back from the current
     * date. For example, you can use this method to get the last 3 months of records.
     * 
     * This contains information about events like space exports, group membership
     * changes, app installations, etc. For more information, see
     * [Audit log](https://confluence.atlassian.com/confcloud/audit-log-802164269.html)
     * in the Confluence administrator's guide.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission.
     */
    'get'(
      parameters?: Parameters<Paths.GetAuditRecordsForTimePeriod.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAuditRecordsForTimePeriod.Responses.$200>
  }
  ['/wiki/rest/api/content']: {
    /**
     * getContent - Get content
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns all content in a Confluence instance.
     * 
     * By default, the following objects are expanded: `space`, `history`, `version`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Only content that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetContent.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContent.Responses.$200>
    /**
     * createContent - Create content
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Creates a new piece of content or publishes an existing draft.
     * 
     * To publish a draft, add the `id` and `status` properties to the body of the request.
     * Set the `id` to the ID of the draft and set the `status` to 'current'. When the
     * request is sent, a new piece of content will be created and the metadata from the
     * draft will be transferred into it.
     * 
     * By default, the following objects are expanded: `space`, `history`, `version`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: 'Add' permission for the
     * space that the content will be created in, and permission to view the draft if publishing a draft.
     */
    'post'(
      parameters?: Parameters<Paths.CreateContent.QueryParameters> | null,
      data?: Paths.CreateContent.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateContent.Responses.$200>
  }
  ['/wiki/rest/api/content/archive']: {
    /**
     * archivePages - Archive pages
     * 
     * Archives a list of pages. The pages to be archived are specified as a list of content IDs.
     * This API accepts the archival request and returns a task ID.
     * The archival process happens asynchronously.
     * Use the /longtask/<taskId> REST API to get the copy task status.
     * 
     * Each content ID needs to resolve to page objects that are not already in an archived state.
     * The content IDs need not belong to the same space.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Archive' permission for each of the pages in the corresponding space it belongs to.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.ArchivePages.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.ArchivePages.Responses.$202>
  }
  ['/wiki/rest/api/content/blueprint/instance/{draftId}']: {
    /**
     * publishSharedDraft - Publish shared draft
     * 
     * Publishes a shared draft of a page created from a blueprint.
     * 
     * By default, the following objects are expanded: `body.storage`, `history`, `space`, `version`, `ancestors`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the draft and 'Add' permission for the space that
     * the content will be created in.
     */
    'put'(
      parameters?: Parameters<Paths.PublishSharedDraft.PathParameters & Paths.PublishSharedDraft.QueryParameters> | null,
      data?: Paths.PublishSharedDraft.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PublishSharedDraft.Responses.$200>
    /**
     * publishLegacyDraft - Publish legacy draft
     * 
     * Publishes a legacy draft of a page created from a blueprint. Legacy drafts
     * will eventually be removed in favor of shared drafts. For now, this method
     * works the same as [Publish shared draft](#api-content-blueprint-instance-draftId-put).
     * 
     * By default, the following objects are expanded: `body.storage`, `history`, `space`, `version`, `ancestors`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the draft and 'Add' permission for the space that
     * the content will be created in.
     */
    'post'(
      parameters?: Parameters<Paths.PublishLegacyDraft.PathParameters & Paths.PublishLegacyDraft.QueryParameters> | null,
      data?: Paths.PublishLegacyDraft.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PublishLegacyDraft.Responses.$200>
  }
  ['/wiki/rest/api/content/search']: {
    /**
     * searchContentByCQL - Search content by CQL
     * 
     * Returns the list of content that matches a Confluence Query Language
     * (CQL) query. For information on CQL, see:
     * [Advanced searching using CQL](https://developer.atlassian.com/cloud/confluence/advanced-searching-using-cql/).
     * 
     * Example initial call:
     * ```
     * /wiki/rest/api/content/search?cql=type=page&limit=25
     * ```
     * 
     * Example response:
     * ```
     * {
     *   "results": [
     *     { ... },
     *     { ... },
     *     ...
     *     { ... }
     *   ],
     *   "limit": 25,
     *   "size": 25,
     *   ...
     *   "_links": {
     *     "base": "<url>",
     *     "context": "<url>",
     *     "next": "/rest/api/content/search?cql=type=page&limit=25&cursor=raNDoMsTRiNg",
     *     "self": "<url>"
     *   }
     * }
     * ```
     * 
     * When additional results are available, returns `next` and `prev` URLs to retrieve them in subsequent calls. The URLs each contain a cursor that points to the appropriate set of results. Use `limit` to specify the number of results returned in each call.
     * Example subsequent call (taken from example response):
     * ```
     * /wiki/rest/api/content/search?cql=type=page&limit=25&cursor=raNDoMsTRiNg
     * ```
     * The response to this will have a `prev` URL similar to the `next` in the example response.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Only content that the user has permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.SearchContentByCQL.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SearchContentByCQL.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}']: {
    /**
     * getContentById - Get content by ID
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns a single piece of content, like a page or a blog post.
     * 
     * By default, the following objects are expanded: `space`, `history`, `version`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content. If the content is a blog post, 'View' permission
     * for the space is required.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentById.PathParameters & Paths.GetContentById.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentById.Responses.$200>
    /**
     * updateContent - Update content
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Updates a piece of content. Use this method to update the title or body
     * of a piece of content, change the status, change the parent page, and more.
     * 
     * Note, updating draft content is currently not supported.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateContent.PathParameters & Paths.UpdateContent.QueryParameters> | null,
      data?: Paths.UpdateContent.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateContent.Responses.$200>
    /**
     * deleteContent - Delete content
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Moves a piece of content to the space's trash or purges it from the trash,
     * depending on the content's type and status:
     * 
     * - If the content's type is `page`,`blogpost`, or `attachment` and its status is `current`,
     * it will be trashed.
     * - If the content's type is `page`,`blogpost`, or `attachment` and its status is `trashed`,
     * the content will be purged from the trash and deleted permanently. Note,
     * you must also set the `status` query parameter to `trashed` in your request.
     * - If the content's type is `comment`, it will be deleted
     * permanently without being trashed.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Delete' permission for the space that the content is in.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteContent.PathParameters & Paths.DeleteContent.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/content/{id}/pageTree']: {
    /**
     * deletePageTree - Delete page tree
     * 
     * Moves a pagetree rooted at a page to the space's trash:
     * 
     * - If the content's type is `page` and its status is `current`, it will be trashed including
     * all its descendants.
     * - For every other combination of content type and status, this API is not supported.
     * 
     * This API accepts the pageTree delete request and returns a task ID.
     * The delete process happens asynchronously.
     * 
     *  Response example:
     *  <pre><code>
     *  {
     *       "id" : "1180606",
     *       "links" : {
     *            "status" : "/rest/api/longtask/1180606"
     *       }
     *  }
     *  </code></pre>
     *  Use the `/longtask/<taskId>` REST API to get the copy task status.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Delete' permission for the space that the content is in.
     */
    'delete'(
      parameters?: Parameters<Paths.DeletePageTree.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeletePageTree.Responses.$202>
  }
  ['/wiki/rest/api/content/{id}/child']: {
    /**
     * getContentChildren - Get content children
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns a map of the direct children of a piece of content. A piece of content
     * has different types of child content, depending on its type. These are
     * the default parent-child content type relationships:
     * 
     * - `page`: child content is `page`, `comment`, `attachment`
     * - `blogpost`: child content is `comment`, `attachment`
     * - `attachment`: child content is `comment`
     * - `comment`: child content is `attachment`
     * 
     * Apps can override these default relationships. Apps can also introduce
     * new content types that create new parent-child content relationships.
     * 
     * Note, the map will always include all child content types that are valid
     * for the content. However, if the content has no instances of a child content
     * type, the map will contain an empty array for that child content type.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: 'View' permission for the space,
     * and permission to view the content if it is a page.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentChildren.PathParameters & Paths.GetContentChildren.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentChildren.Responses.$200>
  }
  ['/wiki/rest/api/content/{pageId}/move/{position}/{targetId}']: {
    /**
     * movePage - Move a page to a new location relative to a target page
     * 
     * Move a page to a new location relative to a target page:
     * 
     * * `before` - move the page under the same parent as the target, before the target in the list of children
     * * `after` - move the page under the same parent as the target, after the target in the list of children
     * * `append` - move the page to be a child of the target
     * 
     * Caution: This API can move pages to the top level of a space. Top-level pages are difficult to find in the UI
     * because they do not show up in the page tree display. To avoid this, never use `before` or `after` positions
     * when the `targetId` is a top-level page.
     */
    'put'(
      parameters?: Parameters<Paths.MovePage.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.MovePage.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/child/attachment']: {
    /**
     * getAttachments - Get attachments
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns the attachments for a piece of content.
     * 
     * By default, the following objects are expanded: `metadata`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content. If the content is a blog post, 'View' permission
     * for the space is required.
     */
    'get'(
      parameters?: Parameters<Paths.GetAttachments.PathParameters & Paths.GetAttachments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAttachments.Responses.$200>
    /**
     * createOrUpdateAttachments - Create or update attachment
     * 
     * Adds an attachment to a piece of content. If the attachment already exists
     * for the content, then the attachment is updated (i.e. a new version of the
     * attachment is created).
     * 
     * Note, you must set a `X-Atlassian-Token: nocheck` header on the request
     * for this method, otherwise it will be blocked. This protects against XSRF
     * attacks, which is necessary as this method accepts multipart/form-data.
     * 
     * The media type 'multipart/form-data' is defined in [RFC 7578](https://www.ietf.org/rfc/rfc7578.txt).
     * Most client libraries have classes that make it easier to implement
     * multipart posts, like the [MultipartEntityBuilder](https://hc.apache.org/httpcomponents-client-5.1.x/current/httpclient5/apidocs/)
     * Java class provided by Apache HTTP Components.
     * 
     * Note, according to [RFC 7578](https://tools.ietf.org/html/rfc7578#section-4.5),
     * in the case where the form data is text,
     * the charset parameter for the "text/plain" Content-Type may be used to
     * indicate the character encoding used in that part. In the case of this
     * API endpoint, the `comment` body parameter should be sent with `type=text/plain`
     * and `charset=utf-8` values. This will force the charset to be UTF-8.
     * 
     * Example: This curl command attaches a file ('example.txt') to a piece of
     * content (id='123') with a comment and `minorEdits`=true. If the 'example.txt'
     * file already exists, it will update it with a new version of the attachment.
     * 
     * ``` bash
     * curl -D- \
     *   -u admin:admin \
     *   -X PUT \
     *   -H 'X-Atlassian-Token: nocheck' \
     *   -F 'file=@"example.txt"' \
     *   -F 'minorEdit="true"' \
     *   -F 'comment="Example attachment comment"; type=text/plain; charset=utf-8' \
     *   http://myhost/rest/api/content/123/child/attachment
     * ```
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'put'(
      parameters?: Parameters<Paths.CreateOrUpdateAttachments.PathParameters & Paths.CreateOrUpdateAttachments.QueryParameters> | null,
      data?: Paths.CreateOrUpdateAttachments.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateOrUpdateAttachments.Responses.$200>
    /**
     * createAttachment - Create attachment
     * 
     * Adds an attachment to a piece of content. This method only adds a new
     * attachment. If you want to update an existing attachment, use
     * [Create or update attachments](#api-content-id-child-attachment-put).
     * 
     * Note, you must set a `X-Atlassian-Token: nocheck` header on the request
     * for this method, otherwise it will be blocked. This protects against XSRF
     * attacks, which is necessary as this method accepts multipart/form-data.
     * 
     * The media type 'multipart/form-data' is defined in [RFC 7578](https://www.ietf.org/rfc/rfc7578.txt).
     * Most client libraries have classes that make it easier to implement
     * multipart posts, like the [MultipartEntityBuilder](https://hc.apache.org/httpcomponents-client-5.1.x/current/httpclient5/apidocs/)
     * Java class provided by Apache HTTP Components.
     * 
     * Note, according to [RFC 7578](https://tools.ietf.org/html/rfc7578#section-4.5),
     * in the case where the form data is text,
     * the charset parameter for the "text/plain" Content-Type may be used to
     * indicate the character encoding used in that part. In the case of this
     * API endpoint, the `comment` body parameter should be sent with `type=text/plain`
     * and `charset=utf-8` values. This will force the charset to be UTF-8.
     * 
     * Example: This curl command attaches a file ('example.txt') to a container
     * (id='123') with a comment and `minorEdits`=true.
     * 
     * ``` bash
     * curl -D- \
     *   -u admin:admin \
     *   -X POST \
     *   -H 'X-Atlassian-Token: nocheck' \
     *   -F 'file=@"example.txt"' \
     *   -F 'minorEdit="true"' \
     *   -F 'comment="Example attachment comment"; type=text/plain; charset=utf-8' \
     *   http://myhost/rest/api/content/123/child/attachment
     * ```
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'post'(
      parameters?: Parameters<Paths.CreateAttachment.PathParameters & Paths.CreateAttachment.QueryParameters> | null,
      data?: Paths.CreateAttachment.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateAttachment.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/child/attachment/{attachmentId}']: {
    /**
     * updateAttachmentProperties - Update attachment properties
     * 
     * Updates the attachment properties, i.e. the non-binary data of an attachment
     * like the filename, media-type, comment, and parent container.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateAttachmentProperties.PathParameters> | null,
      data?: Paths.UpdateAttachmentProperties.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateAttachmentProperties.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/child/attachment/{attachmentId}/data']: {
    /**
     * updateAttachmentData - Update attachment data
     * 
     * Updates the binary data of an attachment, given the attachment ID, and
     * optionally the comment and the minor edit field.
     * 
     * This method is essentially the same as [Create or update attachments](#api-content-id-child-attachment-put),
     * except that it matches the attachment ID rather than the name.
     * 
     * Note, you must set a `X-Atlassian-Token: nocheck` header on the request
     * for this method, otherwise it will be blocked. This protects against XSRF
     * attacks, which is necessary as this method accepts multipart/form-data.
     * 
     * The media type 'multipart/form-data' is defined in [RFC 7578](https://www.ietf.org/rfc/rfc7578.txt).
     * Most client libraries have classes that make it easier to implement
     * multipart posts, like the [MultipartEntityBuilder](https://hc.apache.org/httpcomponents-client-5.1.x/current/httpclient5/apidocs/)
     * Java class provided by Apache HTTP Components.
     * 
     * Note, according to [RFC 7578](https://tools.ietf.org/html/rfc7578#section-4.5),
     * in the case where the form data is text,
     * the charset parameter for the "text/plain" Content-Type may be used to
     * indicate the character encoding used in that part. In the case of this
     * API endpoint, the `comment` body parameter should be sent with `type=text/plain`
     * and `charset=utf-8` values. This will force the charset to be UTF-8.
     * 
     * Example: This curl command updates an attachment (id='att456') that is attached
     * to a piece of content (id='123') with a comment and `minorEdits`=true.
     * 
     * ``` bash
     * curl -D- \
     *   -u admin:admin \
     *   -X POST \
     *   -H 'X-Atlassian-Token: nocheck' \
     *   -F 'file=@"example.txt"' \
     *   -F 'minorEdit="true"' \
     *   -F 'comment="Example attachment comment"; type=text/plain; charset=utf-8' \
     *   http://myhost/rest/api/content/123/child/attachment/att456/data
     * ```
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'post'(
      parameters?: Parameters<Paths.UpdateAttachmentData.PathParameters> | null,
      data?: Paths.UpdateAttachmentData.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateAttachmentData.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/child/attachment/{attachmentId}/download']: {
    /**
     * downloadAttatchment - Get URI to download attachment
     * 
     * Redirects the client to a URL that serves an attachment's binary data.
     */
    'get'(
      parameters?: Parameters<Paths.DownloadAttatchment.PathParameters & Paths.DownloadAttatchment.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/content/{id}/child/comment']: {
    /**
     * getContentComments - Get content comments
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns the comments on a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: 'View' permission for the space,
     * and permission to view the content if it is a page.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentComments.PathParameters & Paths.GetContentComments.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentComments.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/child/{type}']: {
    /**
     * getContentChildrenByType - Get content children by type
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns all children of a given type, for a piece of content.
     * A piece of content has different types of child content, depending on its type:
     * 
     * - `page`: child content is `page`, `comment`, `attachment`
     * - `blogpost`: child content is `comment`, `attachment`
     * - `attachment`: child content is `comment`
     * - `comment`: child content is `attachment`
     * 
     * Custom content types that are provided by apps can also be returned.
     * 
     * Note, this method only returns direct children. To return children at all
     * levels, use [Get descendants by type](#api-content-id-descendant-type-get).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: 'View' permission for the space,
     * and permission to view the content if it is a page.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentChildrenByType.PathParameters & Paths.GetContentChildrenByType.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentChildrenByType.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/descendant']: {
    /**
     * getContentDescendants - Get content descendants
     * 
     * Returns a map of the descendants of a piece of content. This is similar
     * to [Get content children](#api-content-id-child-get), except that this
     * method returns child pages at all levels, rather than just the direct
     * child pages.
     * 
     * A piece of content has different types of descendants, depending on its type:
     * 
     * - `page`: descendant is `page`, `comment`, `attachment`
     * - `blogpost`: descendant is `comment`, `attachment`
     * - `attachment`: descendant is `comment`
     * - `comment`: descendant is `attachment`
     * 
     * The map will always include all descendant types that are valid for the content.
     * However, if the content has no instances of a descendant type, the map will
     * contain an empty array for that descendant type.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space, and permission to view the content if it
     * is a page.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentDescendants.PathParameters & Paths.GetContentDescendants.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentDescendants.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/descendant/{type}']: {
    /**
     * getDescendantsOfType - Get content descendants by type
     * 
     * Returns all descendants of a given type, for a piece of content. This is
     * similar to [Get content children by type](#api-content-id-child-type-get),
     * except that this method returns child pages at all levels, rather than just
     * the direct child pages.
     * 
     * A piece of content has different types of descendants, depending on its type:
     * 
     * - `page`: descendant is `page`, `comment`, `attachment`
     * - `blogpost`: descendant is `comment`, `attachment`
     * - `attachment`: descendant is `comment`
     * - `comment`: descendant is `attachment`
     * 
     * Custom content types that are provided by apps can also be returned.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space, and permission to view the content if it
     * is a page.
     */
    'get'(
      parameters?: Parameters<Paths.GetDescendantsOfType.PathParameters & Paths.GetDescendantsOfType.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetDescendantsOfType.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/history']: {
    /**
     * getHistoryForContent - Get content history
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns the most recent update for a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: Permission to view the content.
     */
    'get'(
      parameters?: Parameters<Paths.GetHistoryForContent.PathParameters & Paths.GetHistoryForContent.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetHistoryForContent.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/history/{version}/macro/id/{macroId}']: {
    /**
     * getMacroBodyByMacroId - Get macro body by macro ID
     * 
     * Returns the body of a macro in storage format, for the given macro ID.
     * This includes information like the name of the macro, the body of the macro,
     * and any macro parameters. This method is mainly used by Cloud apps.
     * 
     * About the macro ID: When a macro is created in a new version of content,
     * Confluence will generate a random ID for it, unless an ID is specified
     * (by an app). The macro ID will look similar to this: '50884bd9-0cb8-41d5-98be-f80943c14f96'.
     * The ID is then persisted as new versions of content are created, and is
     * only modified by Confluence if there are conflicting IDs.
     * 
     * Note, to preserve backwards compatibility this resource will also match on
     * the hash of the macro body, even if a macro ID is found. This check will
     * eventually become redundant, as macro IDs are generated for pages and
     * transparently propagate out to all instances.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content that the macro is in.
     */
    'get'(
      parameters?: Parameters<Paths.GetMacroBodyByMacroId.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetMacroBodyByMacroId.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/history/{version}/macro/id/{macroId}/convert/{to}']: {
    /**
     * getAndConvertMacroBodyByMacroId - Get macro body by macro ID and convert the representation synchronously
     * 
     * Returns the body of a macro in format specified in path, for the given macro ID.
     * This includes information like the name of the macro, the body of the macro,
     * and any macro parameters.
     * 
     * About the macro ID: When a macro is created in a new version of content,
     * Confluence will generate a random ID for it, unless an ID is specified
     * (by an app). The macro ID will look similar to this: '50884bd9-0cb8-41d5-98be-f80943c14f96'.
     * The ID is then persisted as new versions of content are created, and is
     * only modified by Confluence if there are conflicting IDs.
     * 
     * Note, to preserve backwards compatibility this resource will also match on
     * the hash of the macro body, even if a macro ID is found. This check will
     * eventually become redundant, as macro IDs are generated for pages and
     * transparently propagate out to all instances.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content that the macro is in.
     */
    'get'(
      parameters?: Parameters<Paths.GetAndConvertMacroBodyByMacroId.PathParameters & Paths.GetAndConvertMacroBodyByMacroId.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAndConvertMacroBodyByMacroId.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/history/{version}/macro/id/{macroId}/convert/async/{to}']: {
    /**
     * getAndAsyncConvertMacroBodyByMacroId - Get macro body by macro ID and convert representation Asynchronously
     * 
     * Returns Async Id of the conversion task which will convert the macro into a content body of the desired format.
     * The result will be available for 5 minutes after completion of the conversion.
     * 
     * About the macro ID: When a macro is created in a new version of content,
     * Confluence will generate a random ID for it, unless an ID is specified
     * (by an app). The macro ID will look similar to this: '884bd9-0cb8-41d5-98be-f80943c14f96'.
     * The ID is then persisted as new versions of content are created, and is
     * only modified by Confluence if there are conflicting IDs.
     * 
     * Note, to preserve backwards compatibility this resource will also match on
     * the hash of the macro body, even if a macro ID is found. This check will
     * eventually become redundant, as macro IDs are generated for pages and
     * transparently propagate out to all instances.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content that the macro is in.
     */
    'get'(
      parameters?: Parameters<Paths.GetAndAsyncConvertMacroBodyByMacroId.PathParameters & Paths.GetAndAsyncConvertMacroBodyByMacroId.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAndAsyncConvertMacroBodyByMacroId.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/label']: {
    /**
     * getLabelsForContent - Get labels for content
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns the labels on a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space and permission to view the content if it is a page.
     */
    'get'(
      parameters?: Parameters<Paths.GetLabelsForContent.PathParameters & Paths.GetLabelsForContent.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetLabelsForContent.Responses.$200>
    /**
     * addLabelsToContent - Add labels to content
     * 
     * Adds labels to a piece of content. Does not modify the existing labels.
     * 
     * Notes:
     * 
     * - Labels can also be added when creating content ([Create content](#api-content-post)).
     * - Labels can be updated when updating content ([Update content](#api-content-id-put)).
     * This will delete the existing labels and replace them with the labels in
     * the request.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'post'(
      parameters?: Parameters<Paths.AddLabelsToContent.PathParameters> | null,
      data?: Paths.AddLabelsToContent.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AddLabelsToContent.Responses.$200>
    /**
     * removeLabelFromContentUsingQueryParameter - Remove label from content using query parameter
     * 
     * Removes a label from a piece of content. Labels can't be deleted from archived content.
     * This is similar to [Remove label from content](#api-content-id-label-label-delete)
     * except that the label name is specified via a query parameter.
     * 
     * Use this method if the label name has "/" characters, as
     * [Remove label from content using query parameter](#api-content-id-label-delete)
     * does not accept "/" characters for the label name.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveLabelFromContentUsingQueryParameter.PathParameters & Paths.RemoveLabelFromContentUsingQueryParameter.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/content/{id}/label/{label}']: {
    /**
     * removeLabelFromContent - Remove label from content
     * 
     * Removes a label from a piece of content. Labels can't be deleted from archived content.
     * This is similar to [Remove label from content using query parameter](#api-content-id-label-delete)
     * except that the label name is specified via a path parameter.
     * 
     * Use this method if the label name does not have "/" characters, as the path
     * parameter does not accept "/" characters for security reasons. Otherwise,
     * use [Remove label from content using query parameter](#api-content-id-label-delete).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveLabelFromContent.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/content/{id}/notification/child-created']: {
    /**
     * getWatchesForPage - Get watches for page
     * 
     * Returns the watches for a page. A user that watches a page will receive
     * receive notifications when the page is updated.
     * 
     * If you want to manage watches for a page, use the following `user` methods:
     * 
     * - [Get content watch status for user](#api-user-watch-content-contentId-get)
     * - [Add content watch](#api-user-watch-content-contentId-post)
     * - [Remove content watch](#api-user-watch-content-contentId-delete)
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetWatchesForPage.PathParameters & Paths.GetWatchesForPage.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWatchesForPage.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/notification/created']: {
    /**
     * getWatchesForSpace - Get watches for space
     * 
     * Returns all space watches for the space that the content is in. A user that
     * watches a space will receive receive notifications when any content in the
     * space is updated.
     * 
     * If you want to manage watches for a space, use the following `user` methods:
     * 
     * - [Get space watch status for user](#api-user-watch-space-spaceKey-get)
     * - [Add space watch](#api-user-watch-space-spaceKey-post)
     * - [Remove space watch](#api-user-watch-space-spaceKey-delete)
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetWatchesForSpace.PathParameters & Paths.GetWatchesForSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWatchesForSpace.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/pagehierarchy/copy']: {
    /**
     * copyPageHierarchy - Copy page hierarchy
     * 
     * Copy page hierarchy allows the copying of an entire hierarchy of pages and their associated properties, permissions and attachments.
     *  The id path parameter refers to the content id of the page to copy, and the new parent of this copied page is defined using the destinationPageId in the request body.
     *  The titleOptions object defines the rules of renaming page titles during the copy;
     *  for example, search and replace can be used in conjunction to rewrite the copied page titles.
     * 
     *  Response example:
     *  <pre><code>
     *  {
     *       "id" : "1180606",
     *       "links" : {
     *            "status" : "/rest/api/longtask/1180606"
     *       }
     *  }
     *  </code></pre>
     *  Use the /longtask/<taskId> REST API to get the copy task status.
     */
    'post'(
      parameters?: Parameters<Paths.CopyPageHierarchy.PathParameters> | null,
      data?: Paths.CopyPageHierarchy.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CopyPageHierarchy.Responses.$202>
  }
  ['/wiki/rest/api/content/{id}/copy']: {
    /**
     * copyPage - Copy single page
     * 
     * Copies a single page and its associated properties, permissions, attachments, and custom contents.
     *  The `id` path parameter refers to the content ID of the page to copy. The target of the page to be copied
     *  is defined using the `destination` in the request body and can be one of the following types.
     * 
     *   - `space`: page will be copied to the specified space as a root page on the space
     *   - `parent_page`: page will be copied as a child of the specified parent page
     *   - `existing_page`: page will be copied and replace the specified page
     * 
     * By default, the following objects are expanded: `space`, `history`, `version`.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: 'Add' permission for the space that the content will be copied in and permission to update the content if copying to an `existing_page`.
     */
    'post'(
      parameters?: Parameters<Paths.CopyPage.PathParameters & Paths.CopyPage.QueryParameters> | null,
      data?: Paths.CopyPage.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CopyPage.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/permission/check']: {
    /**
     * checkContentPermission - Check content permissions
     * 
     * Check if a user or a group can perform an operation to the specified content. The `operation` to check
     * must be provided. The user’s account ID or the ID of the group can be provided in the `subject` to check
     * permissions against a specified user or group. The following permission checks are done to make sure that the
     * user or group has the proper access:
     * 
     * - site permissions
     * - space permissions
     * - content restrictions
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission) if checking permission for self,
     * otherwise 'Confluence Administrator' global permission is required.
     */
    'post'(
      parameters?: Parameters<Paths.CheckContentPermission.PathParameters> | null,
      data?: Paths.CheckContentPermission.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CheckContentPermission.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/property']: {
    /**
     * getContentProperties - Get content properties
     * 
     * Returns the properties for a piece of content. For more information
     * about content properties, see [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space, and permission to view the content if it is a page.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentProperties.PathParameters & Paths.GetContentProperties.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentProperties.Responses.$200>
    /**
     * createContentProperty - Create content property
     * 
     * Creates a property for an existing piece of content. For more information
     * about content properties, see [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * 
     * This is the same as [Create content property for key](#api-content-id-property-key-post)
     * except that the key is specified in the request body instead of as a
     * path parameter.
     * 
     * Content properties can also be added when creating a new piece of content
     * by including them in the `metadata.properties` of the request.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'post'(
      parameters?: Parameters<Paths.CreateContentProperty.PathParameters> | null,
      data?: Paths.CreateContentProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateContentProperty.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/property/{key}']: {
    /**
     * getContentProperty - Get content property
     * 
     * Returns a content property for a piece of content. For more information, see
     * [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space, and permission to view the content if it is a page.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentProperty.PathParameters & Paths.GetContentProperty.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentProperty.Responses.$200>
    /**
     * updateContentProperty - Update content property
     * 
     * Updates an existing content property. This method will also create a new
     * property for a piece of content, if the property key does not exist and
     * the property version is 1. For more information about content properties, see
     * [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateContentProperty.PathParameters> | null,
      data?: Paths.UpdateContentProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateContentProperty.Responses.$200>
    /**
     * createContentPropertyForKey - Create content property for key
     * 
     * Creates a property for an existing piece of content. For more information
     * about content properties, see [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * 
     * This is the same as [Create content property](#api-content-id-property-post)
     * except that the key is specified as a path parameter instead of in the
     * request body.
     * 
     * Content properties can also be added when creating a new piece of content
     * by including them in the `metadata.properties` of the request.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'post'(
      parameters?: Parameters<Paths.CreateContentPropertyForKey.PathParameters> | null,
      data?: Paths.CreateContentPropertyForKey.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateContentPropertyForKey.Responses.$200>
    /**
     * deleteContentProperty - Delete content property
     * 
     * Deletes a content property. For more information about content properties, see
     * [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteContentProperty.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/content/{id}/restriction']: {
    /**
     * getRestrictions - Get restrictions
     * 
     * Returns the restrictions on a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content.
     */
    'get'(
      parameters?: Parameters<Paths.GetRestrictions.PathParameters & Paths.GetRestrictions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetRestrictions.Responses.$200>
    /**
     * updateRestrictions - Update restrictions
     * 
     * Updates restrictions for a piece of content. This removes the existing
     * restrictions and replaces them with the restrictions in the request.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateRestrictions.PathParameters & Paths.UpdateRestrictions.QueryParameters> | null,
      data?: Paths.UpdateRestrictions.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateRestrictions.Responses.$200>
    /**
     * addRestrictions - Add restrictions
     * 
     * Adds restrictions to a piece of content. Note, this does not change any
     * existing restrictions on the content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'post'(
      parameters?: Parameters<Paths.AddRestrictions.PathParameters & Paths.AddRestrictions.QueryParameters> | null,
      data?: Paths.AddRestrictions.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AddRestrictions.Responses.$200>
    /**
     * deleteRestrictions - Delete restrictions
     * 
     * Removes all restrictions (read and update) on a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteRestrictions.PathParameters & Paths.DeleteRestrictions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteRestrictions.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/restriction/byOperation']: {
    /**
     * getRestrictionsByOperation - Get restrictions by operation
     * 
     * Returns restrictions on a piece of content by operation. This method is
     * similar to [Get restrictions](#api-content-id-restriction-get) except that
     * the operations are properties of the return object, rather than items in
     * a results array.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content.
     */
    'get'(
      parameters?: Parameters<Paths.GetRestrictionsByOperation.PathParameters & Paths.GetRestrictionsByOperation.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetRestrictionsByOperation.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/restriction/byOperation/{operationKey}']: {
    /**
     * getRestrictionsForOperation - Get restrictions for operation
     * 
     * Returns the restictions on a piece of content for a given operation (read
     * or update).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content.
     */
    'get'(
      parameters?: Parameters<Paths.GetRestrictionsForOperation.PathParameters & Paths.GetRestrictionsForOperation.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetRestrictionsForOperation.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/restriction/byOperation/{operationKey}/group/{groupName}']: {
    /**
     * getContentRestrictionStatusForGroup - Get content restriction status for group
     * 
     * Deprecated, use [Get content restriction status for group via groupId](https://developer.atlassian.com/cloud/confluence/rest/v1/api-group-content-restrictions/#api-wiki-rest-api-content-id-restriction-byoperation-operationkey-bygroupid-groupid-get).
     * Returns whether the specified content restriction applies to a group.
     * For example, if a page with `id=123` has a `read` restriction for the `admins` group,
     * the following request will return `true`:
     * 
     * `/wiki/rest/api/content/123/restriction/byOperation/read/group/admins`
     * 
     * Note that a response of `true` does not guarantee that the group can view the page, as it does not account for
     * account-inherited restrictions, space permissions, or even product access. For more
     * information, see [Confluence permissions](https://confluence.atlassian.com/x/_AozKw).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentRestrictionStatusForGroup.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * addGroupToContentRestriction - Add group to content restriction
     * 
     * Deprecated, use [Add group to content restriction via groupId](https://developer.atlassian.com/cloud/confluence/rest/v1/api-group-content-restrictions/#api-wiki-rest-api-content-id-restriction-byoperation-operationkey-bygroupid-groupid-put).
     * Adds a group to a content restriction. That is, grant read or update
     * permission to the group for a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'put'(
      parameters?: Parameters<Paths.AddGroupToContentRestriction.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * removeGroupFromContentRestrictionById - Remove group from content restriction
     * 
     * Deprecated, use [Remove group from content restriction by groupId](https://developer.atlassian.com/cloud/confluence/rest/v1/api-group-content-restrictions/#api-wiki-rest-api-content-id-restriction-byoperation-operationkey-user-delete).
     * Removes a group from a content restriction. That is, remove read or update
     * permission for the group for a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveGroupFromContentRestrictionById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/content/{id}/restriction/byOperation/{operationKey}/byGroupId/{groupId}']: {
    /**
     * getIndividualGroupRestrictionStatusByGroupId - Get content restriction status for group
     * 
     * Returns whether the specified content restriction applies to a group.
     * For example, if a page with `id=123` has a `read` restriction for the `123456` group id,
     * the following request will return `true`:
     * 
     * `/wiki/rest/api/content/123/restriction/byOperation/read/byGroupId/123456`
     * 
     * Note that a response of `true` does not guarantee that the group can view the page, as it does not account for
     * account-inherited restrictions, space permissions, or even product access. For more
     * information, see [Confluence permissions](https://confluence.atlassian.com/x/_AozKw).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content.
     */
    'get'(
      parameters?: Parameters<Paths.GetIndividualGroupRestrictionStatusByGroupId.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * addGroupToContentRestrictionByGroupId - Add group to content restriction
     * 
     * Adds a group to a content restriction by Group Id. That is, grant read or update
     * permission to the group for a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'put'(
      parameters?: Parameters<Paths.AddGroupToContentRestrictionByGroupId.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * removeGroupFromContentRestriction - Remove group from content restriction
     * 
     * Removes a group from a content restriction. That is, remove read or update
     * permission for the group for a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveGroupFromContentRestriction.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/content/{id}/restriction/byOperation/{operationKey}/user']: {
    /**
     * getContentRestrictionStatusForUser - Get content restriction status for user
     * 
     * Returns whether the specified content restriction applies to a user.
     * For example, if a page with `id=123` has a `read` restriction for a user with an account ID of
     * `384093:32b4d9w0-f6a5-3535-11a3-9c8c88d10192`, the following request will return `true`:
     * 
     * `/wiki/rest/api/content/123/restriction/byOperation/read/user?accountId=384093:32b4d9w0-f6a5-3535-11a3-9c8c88d10192`
     * 
     * Note that a response of `true` does not guarantee that the user can view the page, as it does not account for
     * account-inherited restrictions, space permissions, or even product access. For more
     * information, see [Confluence permissions](https://confluence.atlassian.com/x/_AozKw).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentRestrictionStatusForUser.PathParameters & Paths.GetContentRestrictionStatusForUser.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * addUserToContentRestriction - Add user to content restriction
     * 
     * Adds a user to a content restriction. That is, grant read or update
     * permission to the user for a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'put'(
      parameters?: Parameters<Paths.AddUserToContentRestriction.PathParameters & Paths.AddUserToContentRestriction.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * removeUserFromContentRestriction - Remove user from content restriction
     * 
     * Removes a group from a content restriction. That is, remove read or update
     * permission for the group for a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveUserFromContentRestriction.PathParameters & Paths.RemoveUserFromContentRestriction.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/content/{id}/state']: {
    /**
     * getContentState - Get content state
     * 
     * Gets the current content state of the draft or current version of content. To specify the draft version, set
     * the parameter status to draft, otherwise archived or current will get the relevant published state.
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentState.PathParameters & Paths.GetContentState.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentState.Responses.$200>
    /**
     * setContentState - Set the content state of a content and publishes a new version of the content.
     * 
     * Sets the content state of the content specified and creates a new version
     * (publishes the content without changing the body) of the content with the new state.
     * 
     * You may pass in either an id of a state, or the name and color of a desired new state.
     * If all 3 are passed in, id will be used.
     * If the name and color passed in already exist under the current user's existing custom states, the existing state will be reused.
     * If custom states are disabled in the space of the content (which can be determined by getting the content state space settings of the content's space)
     * then this set will fail.
     * 
     * You may not remove a content state via this PUT request. You must use the DELETE method. A specified state is required in the body of this request.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'put'(
      parameters?: Parameters<Paths.SetContentState.PathParameters & Paths.SetContentState.QueryParameters> | null,
      data?: Paths.SetContentState.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SetContentState.Responses.$200>
    /**
     * removeContentState - Removes the content state of a content and publishes a new version.
     * 
     * Removes the content state of the content specified and creates a new version
     * (publishes the content without changing the body) of the content with the new status.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveContentState.PathParameters & Paths.RemoveContentState.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.RemoveContentState.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/state/available']: {
    /**
     * getAvailableContentStates - Gets available content states for content.
     * 
     * Gets content states that are available for the content to be set as.
     * Will return all enabled Space Content States.
     * Will only return most the 3 most recently published custom content states to match UI editor list.
     * To get all custom content states, use the /content-states endpoint.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to edit the content.
     */
    'get'(
      parameters?: Parameters<Paths.GetAvailableContentStates.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAvailableContentStates.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/version']: {
    /**
     * getContentVersions - Get content versions
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns the versions for a piece of content in descending order.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content. If the content is a blog post, 'View' permission
     * for the space is required.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentVersions.PathParameters & Paths.GetContentVersions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentVersions.Responses.$200>
    /**
     * restoreContentVersion - Restore content version
     * 
     * Restores a historical version to be the latest version. That is, a new version
     * is created with the content of the historical version.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'post'(
      parameters?: Parameters<Paths.RestoreContentVersion.PathParameters & Paths.RestoreContentVersion.QueryParameters> | null,
      data?: Paths.RestoreContentVersion.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.RestoreContentVersion.Responses.$200>
  }
  ['/wiki/rest/api/content/{id}/version/{versionNumber}']: {
    /**
     * getContentVersion - Get content version
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns a version for a piece of content.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content. If the content is a blog post, 'View' permission
     * for the space is required.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentVersion.PathParameters & Paths.GetContentVersion.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentVersion.Responses.$200>
    /**
     * deleteContentVersion - Delete content version
     * 
     * Delete a historical version. This does not delete the changes made to the
     * content in that version, rather the changes for the deleted version are
     * rolled up into the next version. Note, you cannot delete the current version.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteContentVersion.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/content-states']: {
    /**
     * bulkSetContentStates - Bulk set content state of many contents
     * 
     * Creates a long running task that sets content state of draft or published versions of pages specified.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**
     * Content Edit Permission for a content to have its state set via this endpoint.
     */
    'put'(
      parameters?: Parameters<Paths.BulkSetContentStates.QueryParameters> | null,
      data?: Paths.BulkSetContentStates.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.BulkSetContentStates.Responses.$200>
    /**
     * getCustomContentStates - Get Custom Content States
     * 
     * Get custom content states that authenticated user has created.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**
     * Must have user authentication.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCustomContentStates.Responses.$200>
  }
  ['/wiki/rest/api/content-states/delete']: {
    /**
     * bulkRemoveContentStates - Bulk remove content states from content
     * 
     * Creates a long running task that Removes content state from draft or published versions of pages specified.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**
     * Content Edit Permission for a content to have its state removed via this endpoint.
     */
    'post'(
      parameters?: Parameters<Paths.BulkRemoveContentStates.QueryParameters> | null,
      data?: Paths.BulkRemoveContentStates.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.BulkRemoveContentStates.Responses.$200>
  }
  ['/wiki/rest/api/content-states/task/{taskId}']: {
    /**
     * getTaskUpdate - Get update on long running task for setting of content state.
     * 
     * Get Status of long running task that was previously created to set or remove content states from content.
     * User must first create a task by passing in details to /wiki/rest/api/content-states PUT or DELETE endpoints.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**
     * Must have created long running task
     */
    'get'(
      parameters?: Parameters<Paths.GetTaskUpdate.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetTaskUpdate.Responses.$200>
  }
  ['/wiki/rest/api/contentbody/convert/{to}']: {
    /**
     * convertContentBody - Convert content body
     * 
     * Converts a content body from one format to another format.
     * 
     * Supported conversions:
     * 
     * - storage: view, export_view, styled_view, editor
     * - editor: storage
     * - view: none
     * - export_view: none
     * - styled_view: none
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * If request specifies 'contentIdContext', 'View' permission for the space, and permission to view the content.
     */
    'post'(
      parameters?: Parameters<Paths.ConvertContentBody.PathParameters & Paths.ConvertContentBody.QueryParameters> | null,
      data?: Paths.ConvertContentBody.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.ConvertContentBody.Responses.$200>
  }
  ['/wiki/rest/api/contentbody/convert/async/{to}']: {
    /**
     * asyncConvertContentBodyRequest - Asynchronously convert content body
     * 
     * Converts a content body from one format to another format asynchronously.
     * Returns the asyncId for the asynchronous task.
     * 
     * Supported conversions:
     * 
     * - storage: export_view
     * 
     * No other conversions are supported at the moment.
     * Once a conversion is completed, it will be available for 5 minutes at the result endpoint.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * If request specifies 'contentIdContext', 'View' permission for the space, and permission to view the content.
     */
    'post'(
      parameters?: Parameters<Paths.AsyncConvertContentBodyRequest.PathParameters & Paths.AsyncConvertContentBodyRequest.QueryParameters> | null,
      data?: Paths.AsyncConvertContentBodyRequest.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AsyncConvertContentBodyRequest.Responses.$200>
  }
  ['/wiki/rest/api/contentbody/convert/async/{id}']: {
    /**
     * asyncConvertContentBodyResponse - Get asynchronously converted content body from the id or the current status of the task.
     * 
     * Returns the asynchronous content body for the corresponding id if the task is complete 
     * or returns the status of the task.
     * 
     * After the task is completed, the result can be obtained for 5 minutes, or until an identical conversion request is made again,
     * with allowCache query param set to false.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * If request specifies 'contentIdContext', 'View' permission for the space, and permission to view the content.
     */
    'get'(
      parameters?: Parameters<Paths.AsyncConvertContentBodyResponse.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AsyncConvertContentBodyResponse.Responses.$200>
  }
  ['/wiki/rest/api/inlinetasks/search']: {
    /**
     * searchTasks - Get inline tasks based on search parameters
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns inline tasks based on the search query.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission). Only tasks
     * in contents that the user has permission to view are returned.
     */
    'get'(
      parameters?: Parameters<Paths.SearchTasks.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SearchTasks.Responses.$200>
  }
  ['/wiki/rest/api/inlinetasks/{inlineTaskId}']: {
    /**
     * getTaskById - Get inline task based on global ID
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns inline task based on the global ID.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the content associated with the task.
     */
    'get'(
      parameters?: Parameters<Paths.GetTaskById.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetTaskById.Responses.$200>
    /**
     * updateTaskById - Update inline task given global ID
     * 
     * Updates an inline tasks status given its global ID
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content associated with the task.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateTaskById.PathParameters> | null,
      data?: Paths.UpdateTaskById.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateTaskById.Responses.$200>
  }
  ['/wiki/rest/api/label']: {
    /**
     * getAllLabelContent - Get label information
     * 
     * Returns label information and a list of contents associated with the label.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission). Only contents
     * that the user is permitted to view is returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetAllLabelContent.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAllLabelContent.Responses.$200>
  }
  ['/wiki/rest/api/group']: {
    /**
     * getGroups - Get groups
     * 
     * Returns all user groups. The returned groups are ordered alphabetically in
     * ascending order by group name.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetGroups.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetGroups.Responses.$200>
    /**
     * createGroup - Create new user group
     * 
     * Creates a new user group.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * User must be a site admin.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateGroup.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateGroup.Responses.$201>
    /**
     * removeGroup - Delete user group
     * 
     * Delete user group.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * User must be a site admin.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveGroup.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/group/by-name']: {
    /**
     * getGroupByQueryParam - Get group
     * 
     * Returns a user group for a given group name.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetGroupByQueryParam.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetGroupByQueryParam.Responses.$200>
  }
  ['/wiki/rest/api/group/by-id']: {
    /**
     * getGroupByGroupId - Get group
     * 
     * Returns a user group for a given group id.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetGroupByGroupId.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetGroupByGroupId.Responses.$200>
    /**
     * removeGroupById - Delete user group
     * 
     * Delete user group.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * User must be a site admin.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveGroupById.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/group/{groupName}']: {
    /**
     * getGroupByName - Get group
     * 
     * Returns a user group for a given group name.
     * 
     * Use updated Get group API
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetGroupByName.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetGroupByName.Responses.$200>
  }
  ['/wiki/rest/api/group/member']: {
    /**
     * getMembersByQueryParam - Get group members
     * 
     * Returns the users that are members of a group.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetMembersByQueryParam.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetMembersByQueryParam.Responses.$200>
  }
  ['/wiki/rest/api/group/{groupName}/member']: {
    /**
     * getGroupMembers - Get group members
     * 
     * Returns the users that are members of a group.
     * 
     * Use updated Get group API
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetGroupMembers.PathParameters & Paths.GetGroupMembers.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetGroupMembers.Responses.$200>
  }
  ['/wiki/rest/api/group/picker']: {
    /**
     * searchGroups - Search groups by partial query
     * 
     * Get search results of groups by partial query provided.
     */
    'get'(
      parameters?: Parameters<Paths.SearchGroups.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SearchGroups.Responses.$200>
  }
  ['/wiki/rest/api/group/userByGroupId']: {
    /**
     * addUserToGroupByGroupId - Add member to group by groupId
     * 
     * Adds a user as a member in a group represented by its groupId
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * User must be a site admin.
     */
    'post'(
      parameters?: Parameters<Paths.AddUserToGroupByGroupId.QueryParameters> | null,
      data?: Paths.AddUserToGroupByGroupId.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * removeMemberFromGroupByGroupId - Remove member from group using group id
     * 
     * Remove user as a member from a group.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * User must be a site admin.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveMemberFromGroupByGroupId.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/group/{groupId}/membersByGroupId']: {
    /**
     * getGroupMembersByGroupId - Get group members
     * 
     * Returns the users that are members of a group.
     * 
     * Use updated Get group API
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetGroupMembersByGroupId.PathParameters & Paths.GetGroupMembersByGroupId.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetGroupMembersByGroupId.Responses.$200>
  }
  ['/wiki/rest/api/group/user']: {
    /**
     * addUserToGroup - Add member to group
     * 
     * Adds a user as a member in a group.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * User must be a site admin.
     */
    'post'(
      parameters?: Parameters<Paths.AddUserToGroup.QueryParameters> | null,
      data?: Paths.AddUserToGroup.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * removeMemberFromGroup - Remove member from group
     * 
     * Remove user as a member from a group.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * User must be a site admin.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveMemberFromGroup.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/longtask']: {
    /**
     * getTasks - Get long-running tasks
     * 
     * Returns information about all active long-running tasks (e.g. space export),
     * such as how long each task has been running and the percentage of each task
     * that has completed.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetTasks.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetTasks.Responses.$200>
  }
  ['/wiki/rest/api/longtask/{id}']: {
    /**
     * getTask - Get long-running task
     * 
     * Returns information about an active long-running task (e.g. space export),
     * such as how long it has been running and the percentage of the task that
     * has completed.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetTask.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetTask.Responses.$200>
  }
  ['/wiki/rest/api/relation/{relationName}/from/{sourceType}/{sourceKey}/to/{targetType}']: {
    /**
     * findTargetFromSource - Find target entities related to a source entity
     * 
     * Returns all target entities that have a particular relationship to the
     * source entity. Note, relationships are one way.
     * 
     * For example, the following method finds all content that the current user
     * has an 'ignore' relationship with:
     * `GET /wiki/rest/api/relation/ignore/from/user/current/to/content`
     * Note, 'ignore' is an example custom relationship type.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view both the target entity and source entity.
     */
    'get'(
      parameters?: Parameters<Paths.FindTargetFromSource.PathParameters & Paths.FindTargetFromSource.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.FindTargetFromSource.Responses.$200>
  }
  ['/wiki/rest/api/relation/{relationName}/from/{sourceType}/{sourceKey}/to/{targetType}/{targetKey}']: {
    /**
     * getRelationship - Find relationship from source to target
     * 
     * Find whether a particular type of relationship exists from a source
     * entity to a target entity. Note, relationships are one way.
     * 
     * For example, you can use this method to find whether the current user has
     * selected a particular page as a favorite (i.e. 'save for later'):
     * `GET /wiki/rest/api/relation/favourite/from/user/current/to/content/123`
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view both the target entity and source entity.
     */
    'get'(
      parameters?: Parameters<Paths.GetRelationship.PathParameters & Paths.GetRelationship.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetRelationship.Responses.$200>
    /**
     * createRelationship - Create relationship
     * 
     * Creates a relationship between two entities (user, space, content). The
     * 'favourite' relationship is supported by default, but you can use this method
     * to create any type of relationship between two entities.
     * 
     * For example, the following method creates a 'sibling' relationship between
     * two pieces of content:
     * `GET /wiki/rest/api/relation/sibling/from/content/123/to/content/456`
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'put'(
      parameters?: Parameters<Paths.CreateRelationship.PathParameters & Paths.CreateRelationship.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateRelationship.Responses.$200>
    /**
     * deleteRelationship - Delete relationship
     * 
     * Deletes a relationship between two entities (user, space, content).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * For favourite relationships, the current user can only delete their own
     * favourite relationships. A space administrator can delete favourite
     * relationships for any user.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteRelationship.PathParameters & Paths.DeleteRelationship.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/relation/{relationName}/to/{targetType}/{targetKey}/from/{sourceType}']: {
    /**
     * findSourcesForTarget - Find source entities related to a target entity
     * 
     * Returns all target entities that have a particular relationship to the
     * source entity. Note, relationships are one way.
     * 
     * For example, the following method finds all users that have a 'collaborator'
     * relationship to a piece of content with an ID of '1234':
     * `GET /wiki/rest/api/relation/collaborator/to/content/1234/from/user`
     * Note, 'collaborator' is an example custom relationship type.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view both the target entity and source entity.
     */
    'get'(
      parameters?: Parameters<Paths.FindSourcesForTarget.PathParameters & Paths.FindSourcesForTarget.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.FindSourcesForTarget.Responses.$200>
  }
  ['/wiki/rest/api/search']: {
    /**
     * searchByCQL - Search content
     * 
     * Searches for content using the
     * [Confluence Query Language (CQL)](https://developer.atlassian.com/cloud/confluence/advanced-searching-using-cql/).
     * 
     * **Note that CQL input queries submitted through the `/wiki/rest/api/search` endpoint no longer support user-specific fields like `user`, `user.fullname`, `user.accountid`, and `user.userkey`.** 
     * See this [deprecation notice](https://developer.atlassian.com/cloud/confluence/deprecation-notice-search-api/) for more details.
     * 
     * Example initial call:
     * ```
     * /wiki/rest/api/search?cql=type=page&limit=25
     * ```
     * 
     * Example response:
     * ```
     * {
     *   "results": [
     *     { ... },
     *     { ... },
     *     ...
     *     { ... }
     *   ],
     *   "limit": 25,
     *   "size": 25,
     *   ...
     *   "_links": {
     *     "base": "<url>",
     *     "context": "<url>",
     *     "next": "/rest/api/search?cql=type=page&limit=25&cursor=raNDoMsTRiNg",
     *     "self": "<url>"
     *   }
     * }
     * ```
     * 
     * When additional results are available, returns `next` and `prev` URLs to retrieve them in subsequent calls. The URLs each contain a cursor that points to the appropriate set of results. Use `limit` to specify the number of results returned in each call.
     * 
     * Example subsequent call (taken from example response):
     * ```
     * /wiki/rest/api/search?cql=type=page&limit=25&cursor=raNDoMsTRiNg
     * ```
     * The response to this will have a `prev` URL similar to the `next` in the example response.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to view the entities. Note, only entities that the user has
     * permission to view will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.SearchByCQL.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SearchByCQL.Responses.$200>
  }
  ['/wiki/rest/api/search/user']: {
    /**
     * searchUser - Search users
     * 
     * Searches for users using user-specific queries from the
     * [Confluence Query Language (CQL)](https://developer.atlassian.com/cloud/confluence/advanced-searching-using-cql/).
     * 
     * Note that CQL input queries submitted through the `/wiki/rest/api/search/user` endpoint only support user-specific fields like `user`, `user.fullname`, `user.accountid`, and `user.userkey`.
     * 
     * Note that some user fields may be set to null depending on the user's privacy settings.
     * These are: email, profilePicture, displayName, and timeZone.
     */
    'get'(
      parameters?: Parameters<Paths.SearchUser.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SearchUser.Responses.$200>
  }
  ['/wiki/rest/api/settings/lookandfeel']: {
    /**
     * getLookAndFeelSettings - Get look and feel settings
     * 
     * Returns the look and feel settings for the site or a single space. This
     * includes attributes such as the color scheme, padding, and border radius.
     * 
     * The look and feel settings for a space can be inherited from the global
     * look and feel settings or provided by a theme.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * None
     */
    'get'(
      parameters?: Parameters<Paths.GetLookAndFeelSettings.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetLookAndFeelSettings.Responses.$200>
    /**
     * updateLookAndFeel - Select look and feel settings
     * 
     * Sets the look and feel settings to the default (global) settings, the
     * custom settings, or the current theme's settings for a space.
     * The custom and theme settings can only be selected if there is already
     * a theme set for a space. Note, the default space settings are inherited
     * from the current global settings.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.UpdateLookAndFeel.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateLookAndFeel.Responses.$200>
  }
  ['/wiki/rest/api/settings/lookandfeel/custom']: {
    /**
     * updateLookAndFeelSettings - Update look and feel settings
     * 
     * Updates the look and feel settings for the site or for a single space.
     * If custom settings exist, they are updated. If no custom settings exist,
     * then a set of custom settings is created.
     * 
     * Note, if a theme is selected for a space, the space look and feel settings
     * are provided by the theme and cannot be overridden.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'post'(
      parameters?: Parameters<Paths.UpdateLookAndFeelSettings.QueryParameters> | null,
      data?: Paths.UpdateLookAndFeelSettings.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateLookAndFeelSettings.Responses.$200>
    /**
     * resetLookAndFeelSettings - Reset look and feel settings
     * 
     * Resets the custom look and feel settings for the site or a single space.
     * This changes the values of the custom settings to be the same as the
     * default settings. It does not change which settings (default or custom)
     * are selected. Note, the default space settings are inherited from the
     * current global settings.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'delete'(
      parameters?: Parameters<Paths.ResetLookAndFeelSettings.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/settings/lookandfeel/selected']: {
    /**
     * setLookAndFeelSettings - Set look and feel settings
     * 
     * Sets the look and feel settings to either the default settings or the
     * custom settings, for the site or a single space. Note, the default
     * space settings are inherited from the current global settings.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'put'(
      parameters?: Parameters<Paths.SetLookAndFeelSettings.QueryParameters> | null,
      data?: Paths.SetLookAndFeelSettings.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SetLookAndFeelSettings.Responses.$200>
  }
  ['/wiki/rest/api/settings/systemInfo']: {
    /**
     * getSystemInfo - Get system info
     * 
     * Returns the system information for the Confluence Cloud tenant. This
     * information is used by Atlassian.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSystemInfo.Responses.$200>
  }
  ['/wiki/rest/api/settings/theme']: {
    /**
     * getThemes - Get themes
     * 
     * Returns all themes, not including the default theme.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: None
     */
    'get'(
      parameters?: Parameters<Paths.GetThemes.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetThemes.Responses.$200>
  }
  ['/wiki/rest/api/settings/theme/selected']: {
    /**
     * getGlobalTheme - Get global theme
     * 
     * Returns the globally assigned theme.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: None
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetGlobalTheme.Responses.$200>
  }
  ['/wiki/rest/api/settings/theme/{themeKey}']: {
    /**
     * getTheme - Get theme
     * 
     * Returns a theme. This includes information about the theme name,
     * description, and icon.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**: None
     */
    'get'(
      parameters?: Parameters<Paths.GetTheme.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetTheme.Responses.$200>
  }
  ['/wiki/rest/api/space']: {
    /**
     * getSpaces - Get spaces
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns all spaces. The returned spaces are ordered alphabetically in
     * ascending order by space key.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     * Note, the returned list will only contain spaces that the current user
     * has permission to view.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaces.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaces.Responses.$200>
    /**
     * createSpace - Create space
     * 
     * Creates a new space. Note, currently you cannot set space labels when
     * creating a space.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Create Space(s)' global permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateSpace.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateSpace.Responses.$200>
  }
  ['/wiki/rest/api/space/_private']: {
    /**
     * createPrivateSpace - Create private space
     * 
     * Creates a new space that is only visible to the creator. This method is
     * the same as the [Create space](#api-space-post) method with permissions
     * set to the current user only. Note, currently you cannot set space
     * labels when creating a space.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Create Space(s)' global permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreatePrivateSpace.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreatePrivateSpace.Responses.$200>
  }
  ['/wiki/rest/api/space/{spaceKey}']: {
    /**
     * getSpace - Get space
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns a space. This includes information like the name, description,
     * and permissions, but not the content in the space.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpace.PathParameters & Paths.GetSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpace.Responses.$200>
    /**
     * updateSpace - Update space
     * 
     * Updates the name, description, or homepage of a space.
     * 
     * -   For security reasons, permissions cannot be updated via the API and
     * must be changed via the user interface instead.
     * -   Currently you cannot set space labels when updating a space.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateSpace.PathParameters> | null,
      data?: Paths.UpdateSpace.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateSpace.Responses.$200>
    /**
     * deleteSpace - Delete space
     * 
     * Deletes a space. Note, the space will be deleted in a long running task.
     * Therefore, the space may not be deleted yet when this method has
     * returned. Clients should poll the status link that is returned in the
     * response until the task completes.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteSpace.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteSpace.Responses.$202>
  }
  ['/wiki/rest/api/space/{spaceKey}/content']: {
    /**
     * getContentForSpace - Get content for space
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns all content in a space. The returned content is grouped by type
     * (pages then blogposts), then ordered by content ID in ascending order.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space. Note, the returned list will only
     * contain content that the current user has permission to view.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentForSpace.PathParameters & Paths.GetContentForSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentForSpace.Responses.$200>
  }
  ['/wiki/rest/api/space/{spaceKey}/permission']: {
    /**
     * addPermissionToSpace - Add new permission to space
     * 
     * Adds new permission to space.
     * 
     * If the permission to be added is a group permission, the group can be identified
     * by its group name or group id.
     * 
     * Note: Apps cannot access this REST resource - including when utilizing user impersonation.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'post'(
      parameters?: Parameters<Paths.AddPermissionToSpace.PathParameters> | null,
      data?: Paths.AddPermissionToSpace.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AddPermissionToSpace.Responses.$200>
  }
  ['/wiki/rest/api/space/{spaceKey}/permission/custom-content']: {
    /**
     * addCustomContentPermissions - Add new custom content permission to space
     * 
     * Adds new custom content permission to space.
     * 
     * If the permission to be added is a group permission, the group can be identified
     * by its group name or group id.
     * 
     * Note: Only apps can access this REST resource and only make changes to the respective app permissions.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'post'(
      parameters?: Parameters<Paths.AddCustomContentPermissions.PathParameters> | null,
      data?: Paths.AddCustomContentPermissions.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/space/{spaceKey}/permission/{id}']: {
    /**
     * removePermission - Remove a space permission
     * 
     * Removes a space permission. Note that removing Read Space permission for a user or group will remove all
     * the space permissions for that user or group.
     * 
     * Note: Apps cannot access this REST resource - including when utilizing user impersonation.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'delete'(
      parameters?: Parameters<Paths.RemovePermission.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/space/{spaceKey}/content/{type}']: {
    /**
     * getContentByTypeForSpace - Get content by type for space
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns all content of a given type, in a space. The returned content is
     * ordered by content ID in ascending order.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space. Note, the returned list will only
     * contain content that the current user has permission to view.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentByTypeForSpace.PathParameters & Paths.GetContentByTypeForSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentByTypeForSpace.Responses.$200>
  }
  ['/wiki/rest/api/space/{spaceKey}/property']: {
    /**
     * getSpaceProperties - Get space properties
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns all properties for the given space. Space properties are a key-value storage associated with a space.
     * 
     * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**: ‘View’ permission for the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaceProperties.PathParameters & Paths.GetSpaceProperties.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaceProperties.Responses.$200>
    /**
     * createSpaceProperty - Create space property
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Creates a new space property.
     * 
     * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**:
     * ‘Admin’ permission for the space.
     */
    'post'(
      parameters?: Parameters<Paths.CreateSpaceProperty.PathParameters> | null,
      data?: Paths.CreateSpaceProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateSpaceProperty.Responses.$200>
  }
  ['/wiki/rest/api/space/{spaceKey}/property/{key}']: {
    /**
     * getSpaceProperty - Get space property
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Returns a space property.
     * 
     * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**: ‘View’ permission for the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaceProperty.PathParameters & Paths.GetSpaceProperty.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaceProperty.Responses.$200>
    /**
     * updateSpaceProperty - Update space property
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Updates a space property. Note, you cannot update the key of a space
     * property, only the value.
     * 
     * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**:
     * ‘Admin’ permission for the space.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateSpaceProperty.PathParameters> | null,
      data?: Paths.UpdateSpaceProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateSpaceProperty.Responses.$200>
    /**
     * createSpacePropertyForKey - Create space property for key
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Creates a new space property. This is the same as `POST
     * /wiki/rest/api/space/{spaceKey}/property` but the key for the property is passed as a
     * path parameter, rather than in the request body.
     * 
     * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**:
     * ‘Admin’ permission for the space.
     */
    'post'(
      parameters?: Parameters<Paths.CreateSpacePropertyForKey.PathParameters> | null,
      data?: Paths.CreateSpacePropertyForKey.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateSpacePropertyForKey.Responses.$200>
    /**
     * deleteSpaceProperty - Delete space property
     * 
     * Deprecated, use [Confluence's v2 API](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/).
     * 
     * Deletes a space property.
     * 
     * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**:
     * ‘Admin’ permission for the space.
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteSpaceProperty.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/space/{spaceKey}/settings']: {
    /**
     * getSpaceSettings - Get space settings
     * 
     * Returns the settings of a space. Currently only the
     * `routeOverrideEnabled` setting can be returned.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaceSettings.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaceSettings.Responses.$200>
    /**
     * updateSpaceSettings - Update space settings
     * 
     * Updates the settings for a space. Currently only the
     * `routeOverrideEnabled` setting can be updated.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'put'(
      parameters?: Parameters<Paths.UpdateSpaceSettings.PathParameters> | null,
      data?: Paths.UpdateSpaceSettings.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateSpaceSettings.Responses.$200>
  }
  ['/wiki/rest/api/space/{spaceKey}/state']: {
    /**
     * getSpaceContentStates - Get space suggested content states
     * 
     * Get content states that are suggested in the space.
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Space view permission
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaceContentStates.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaceContentStates.Responses.$200>
  }
  ['/wiki/rest/api/space/{spaceKey}/state/settings']: {
    /**
     * getContentStateSettings - Get content state settings for space
     * 
     * Get object describing whether content states are allowed at all, if custom content states or space content states
     * are restricted, and a list of space content states allowed for the space if they are not restricted.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Space admin permission
     */
    'get'(
      parameters?: Parameters<Paths.GetContentStateSettings.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentStateSettings.Responses.$200>
  }
  ['/wiki/rest/api/space/{spaceKey}/state/content']: {
    /**
     * getContentsWithState - Get content in space with given content state
     * 
     * Finds paginated content with 
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Space View Permission
     */
    'get'(
      parameters?: Parameters<Paths.GetContentsWithState.PathParameters & Paths.GetContentsWithState.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentsWithState.Responses.$200>
  }
  ['/wiki/rest/api/space/{spaceKey}/theme']: {
    /**
     * getSpaceTheme - Get space theme
     * 
     * Returns the theme selected for a space, if one is set. If no space
     * theme is set, this means that the space is inheriting the global look
     * and feel settings.
     * 
     * **[Permissions required](https://confluence.atlassian.com/x/_AozKw)**: ‘View’ permission for the space.
     */
    'get'(
      parameters?: Parameters<Paths.GetSpaceTheme.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSpaceTheme.Responses.$200>
    /**
     * setSpaceTheme - Set space theme
     * 
     * Sets the theme for a space. Note, if you want to reset the space theme to
     * the default Confluence theme, use the 'Reset space theme' method instead
     * of this method.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'put'(
      parameters?: Parameters<Paths.SetSpaceTheme.PathParameters> | null,
      data?: Paths.SetSpaceTheme.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.SetSpaceTheme.Responses.$200>
    /**
     * resetSpaceTheme - Reset space theme
     * 
     * Resets the space theme. This means that the space will inherit the
     * global look and feel settings
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space.
     */
    'delete'(
      parameters?: Parameters<Paths.ResetSpaceTheme.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/space/{spaceKey}/watch']: {
    /**
     * getWatchersForSpace - Get space watchers
     * 
     * Returns a list of watchers of a space
     */
    'get'(
      parameters?: Parameters<Paths.GetWatchersForSpace.PathParameters & Paths.GetWatchersForSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWatchersForSpace.Responses.$200>
  }
  ['/wiki/rest/api/space/{spaceKey}/label']: {
    /**
     * getLabelsForSpace - Get Space Labels
     * 
     * Returns a list of labels associated with a space. Can provide a prefix as well as other filters to
     * select different types of labels.
     */
    'get'(
      parameters?: Parameters<Paths.GetLabelsForSpace.PathParameters & Paths.GetLabelsForSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetLabelsForSpace.Responses.$200>
    /**
     * addLabelsToSpace - Add labels to a space
     * 
     * Adds labels to a piece of content. Does not modify the existing labels.
     * 
     * Notes:
     * 
     * - Labels can also be added when creating content ([Create content](#api-content-post)).
     * - Labels can be updated when updating content ([Update content](#api-content-id-put)).
     * This will delete the existing labels and replace them with the labels in
     * the request.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to update the content.
     */
    'post'(
      parameters?: Parameters<Paths.AddLabelsToSpace.PathParameters> | null,
      data?: Paths.AddLabelsToSpace.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.AddLabelsToSpace.Responses.$200>
    /**
     * deleteLabelFromSpace - Remove label from a space
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteLabelFromSpace.PathParameters & Paths.DeleteLabelFromSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/template']: {
    /**
     * updateContentTemplate - Update content template
     * 
     * Updates a content template. Note, blueprint templates cannot be updated
     * via the REST API.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space to update a space template or 'Confluence Administrator'
     * global permission to update a global template.
     */
    'put'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.UpdateContentTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateContentTemplate.Responses.$200>
    /**
     * createContentTemplate - Create content template
     * 
     * Creates a new content template. Note, blueprint templates cannot be created via the REST API.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Admin' permission for the space to create a space template or 'Confluence Administrator'
     * global permission to create a global template.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.CreateContentTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateContentTemplate.Responses.$200>
  }
  ['/wiki/rest/api/template/blueprint']: {
    /**
     * getBlueprintTemplates - Get blueprint templates
     * 
     * Returns all templates provided by blueprints. Use this method to retrieve
     * all global blueprint templates or all blueprint templates in a space.
     * 
     * Note, all global blueprints are inherited by each space. Space blueprints
     * can be customised without affecting the global blueprints.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space to view blueprints for the space and permission
     * to access the Confluence site ('Can use' global permission) to view global blueprints.
     */
    'get'(
      parameters?: Parameters<Paths.GetBlueprintTemplates.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBlueprintTemplates.Responses.$200>
  }
  ['/wiki/rest/api/template/page']: {
    /**
     * getContentTemplates - Get content templates
     * 
     * Returns all content templates. Use this method to retrieve all global
     * content templates or all content templates in a space.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space to view space templates and permission to
     * access the Confluence site ('Can use' global permission) to view global templates.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentTemplates.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentTemplates.Responses.$200>
  }
  ['/wiki/rest/api/template/{contentTemplateId}']: {
    /**
     * getContentTemplate - Get content template
     * 
     * Returns a content template. This includes information about template,
     * like the name, the space or blueprint that the template is in, the body
     * of the template, and more.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'View' permission for the space to view space templates and permission to
     * access the Confluence site ('Can use' global permission) to view global templates.
     */
    'get'(
      parameters?: Parameters<Paths.GetContentTemplate.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentTemplate.Responses.$200>
    /**
     * removeTemplate - Remove template
     * 
     * Deletes a template. This results in different actions depending on the
     * type of template:
     * 
     * - If the template is a content template, it is deleted.
     * - If the template is a modified space-level blueprint template, it reverts
     * to the template inherited from the global-level blueprint template.
     * - If the template is a modified global-level blueprint template, it reverts
     * to the default global-level blueprint template.
     * 
     *  Note, unmodified blueprint templates cannot be deleted.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     *         'Admin' permission for the space to delete a space template or 'Confluence Administrator'
     *         global permission to delete a global template.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveTemplate.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/user']: {
    /**
     * getUser - Get user
     * 
     * Returns a user. This includes information about the user, such as the
     * display name, account ID, profile picture, and more. The information returned may be
     * restricted by the user's profile visibility settings.
     * 
     * **Note:** to add, edit, or delete users in your organization, see the
     * [user management REST API](/cloud/admin/user-management/about/).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetUser.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetUser.Responses.$200>
  }
  ['/wiki/rest/api/user/anonymous']: {
    /**
     * getAnonymousUser - Get anonymous user
     * 
     * Returns information about how anonymous users are represented, like the
     * profile picture and display name.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetAnonymousUser.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAnonymousUser.Responses.$200>
  }
  ['/wiki/rest/api/user/current']: {
    /**
     * getCurrentUser - Get current user
     * 
     * Returns the currently logged-in user. This includes information about
     * the user, like the display name, userKey, account ID, profile picture,
     * and more.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetCurrentUser.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCurrentUser.Responses.$200>
  }
  ['/wiki/rest/api/user/memberof']: {
    /**
     * getGroupMembershipsForUser - Get group memberships for user
     * 
     * Returns the groups that a user is a member of.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetGroupMembershipsForUser.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetGroupMembershipsForUser.Responses.$200>
  }
  ['/wiki/rest/api/user/bulk']: {
    /**
     * getBulkUserLookup - Get multiple users using ids
     * 
     * Returns user details for the ids provided in request.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetBulkUserLookup.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetBulkUserLookup.Responses.$200>
  }
  ['/wiki/rest/api/user/watch/content/{contentId}']: {
    /**
     * getContentWatchStatus - Get content watch status
     * 
     * Returns whether a user is watching a piece of content. Choose the user by
     * doing one of the following:
     * 
     * - Specify a user via a query parameter: Use the `accountId` to identify the user.
     * - Do not specify a user: The currently logged-in user will be used.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission if specifying a user, otherwise
     * permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetContentWatchStatus.PathParameters & Paths.GetContentWatchStatus.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetContentWatchStatus.Responses.$200>
    /**
     * addContentWatcher - Add content watcher
     * 
     * Adds a user as a watcher to a piece of content. Choose the user by doing
     * one of the following:
     * 
     * - Specify a user via a query parameter: Use the `accountId` to identify the user.
     * - Do not specify a user: The currently logged-in user will be used.
     * 
     * Note, you must add the `X-Atlassian-Token: no-check` header when making a
     * request, as this operation has XSRF protection.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission if specifying a user, otherwise
     * permission to access the Confluence site ('Can use' global permission).
     */
    'post'(
      parameters?: Parameters<Paths.AddContentWatcher.PathParameters & Paths.AddContentWatcher.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * removeContentWatcher - Remove content watcher
     * 
     * Removes a user as a watcher from a piece of content. Choose the user by
     * doing one of the following:
     * 
     * - Specify a user via a query parameter: Use the `accountId` to identify the user.
     * - Do not specify a user: The currently logged-in user will be used.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission if specifying a user, otherwise
     * permission to access the Confluence site ('Can use' global permission).
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveContentWatcher.PathParameters & Paths.RemoveContentWatcher.QueryParameters & Paths.RemoveContentWatcher.HeaderParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/user/watch/label/{labelName}']: {
    /**
     * isWatchingLabel - Get label watch status
     * 
     * Returns whether a user is watching a label. Choose the user by doing one
     * of the following:
     * 
     * - Specify a user via a query parameter: Use the `accountId` to identify the user.
     * - Do not specify a user: The currently logged-in user will be used.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission if specifying a user, otherwise
     * permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.IsWatchingLabel.PathParameters & Paths.IsWatchingLabel.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.IsWatchingLabel.Responses.$200>
    /**
     * addLabelWatcher - Add label watcher
     * 
     * Adds a user as a watcher to a label. Choose the user by doing one of the
     * following:
     * 
     * - Specify a user via a query parameter: Use the `accountId` to identify the user.
     * - Do not specify a user: The currently logged-in user will be used.
     * 
     * Note, you must add the `X-Atlassian-Token: no-check` header when making a
     * request, as this operation has XSRF protection.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission if specifying a user, otherwise
     * permission to access the Confluence site ('Can use' global permission).
     */
    'post'(
      parameters?: Parameters<Paths.AddLabelWatcher.PathParameters & Paths.AddLabelWatcher.QueryParameters & Paths.AddLabelWatcher.HeaderParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * removeLabelWatcher - Remove label watcher
     * 
     * Removes a user as a watcher from a label. Choose the user by doing one of
     * the following:
     * 
     * - Specify a user via a query parameter: Use the `accountId` to identify the user.
     * - Do not specify a user: The currently logged-in user will be used.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission if specifying a user, otherwise
     * permission to access the Confluence site ('Can use' global permission).
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveLabelWatcher.PathParameters & Paths.RemoveLabelWatcher.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/user/watch/space/{spaceKey}']: {
    /**
     * isWatchingSpace - Get space watch status
     * 
     * Returns whether a user is watching a space. Choose the user by
     * doing one of the following:
     * 
     * - Specify a user via a query parameter: Use the `accountId` to identify the user.
     * - Do not specify a user: The currently logged-in user will be used.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission if specifying a user, otherwise
     * permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.IsWatchingSpace.PathParameters & Paths.IsWatchingSpace.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.IsWatchingSpace.Responses.$200>
    /**
     * addSpaceWatcher - Add space watcher
     * 
     * Adds a user as a watcher to a space. Choose the user by doing one of the
     * following:
     * 
     * - Specify a user via a query parameter: Use the `accountId` to identify the user.
     * - Do not specify a user: The currently logged-in user will be used.
     * 
     * Note, you must add the `X-Atlassian-Token: no-check` header when making a
     * request, as this operation has XSRF protection.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission if specifying a user, otherwise
     * permission to access the Confluence site ('Can use' global permission).
     */
    'post'(
      parameters?: Parameters<Paths.AddSpaceWatcher.PathParameters & Paths.AddSpaceWatcher.QueryParameters & Paths.AddSpaceWatcher.HeaderParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * removeSpaceWatch - Remove space watch
     * 
     * Removes a user as a watcher from a space. Choose the user by doing one of
     * the following:
     * 
     * - Specify a user via a query parameter: Use the `accountId` to identify the user.
     * - Do not specify a user: The currently logged-in user will be used.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * 'Confluence Administrator' global permission if specifying a user, otherwise
     * permission to access the Confluence site ('Can use' global permission).
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveSpaceWatch.PathParameters & Paths.RemoveSpaceWatch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/user/email']: {
    /**
     * getPrivacyUnsafeUserEmail - Get user email address
     * 
     * Returns a user's email address. This API is only available to apps approved by
     * Atlassian, according to these [guidelines](https://community.developer.atlassian.com/t/guidelines-for-requesting-access-to-email-address/27603).
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetPrivacyUnsafeUserEmail.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPrivacyUnsafeUserEmail.Responses.$200>
  }
  ['/wiki/rest/api/user/email/bulk']: {
    /**
     * getPrivacyUnsafeUserEmailBulk - Get user email addresses in batch
     * 
     * Returns user email addresses for a set of accountIds. This API is only available to apps approved by
     * Atlassian, according to these [guidelines](https://community.developer.atlassian.com/t/guidelines-for-requesting-access-to-email-address/27603).
     * 
     * Any accounts which are not available will not be included in the result.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetPrivacyUnsafeUserEmailBulk.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPrivacyUnsafeUserEmailBulk.Responses.$200>
  }
  ['/atlassian-connect/1/app/module/dynamic']: {
    /**
     * getModules - Get modules
     * 
     * Returns all modules registered dynamically by the calling app.
     * 
     * **[Permissions](#permissions) required:** Only Connect apps can make this request.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * registerModules - Register modules
     * 
     * Registers a list of modules. For the list of modules that support dynamic registration, see [Dynamic modules](https://developer.atlassian.com/cloud/confluence/dynamic-modules/).
     * 
     * **[Permissions](#permissions) required:** Only Connect apps can make this request.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
    /**
     * removeModules - Remove modules
     * 
     * Remove all or a list of modules registered by the calling app.
     * 
     * **[Permissions](#permissions) required:** Only Connect apps can make this request.
     */
    'delete'(
      parameters?: Parameters<Paths.RemoveModules.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/wiki/rest/api/analytics/content/{contentId}/views']: {
    /**
     * getViews - Get views
     * 
     * Get the total number of views a piece of content has.
     */
    'get'(
      parameters?: Parameters<Paths.GetViews.PathParameters & Paths.GetViews.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetViews.Responses.$200>
  }
  ['/wiki/rest/api/analytics/content/{contentId}/viewers']: {
    /**
     * getViewers - Get viewers
     * 
     * Get the total number of distinct viewers a piece of content has.
     */
    'get'(
      parameters?: Parameters<Paths.GetViewers.PathParameters & Paths.GetViewers.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetViewers.Responses.$200>
  }
  ['/wiki/rest/api/user/{userId}/property']: {
    /**
     * getUserProperties - Get user properties
     * 
     * Returns the properties for a user as list of property keys. For more information
     * about user properties, see [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * `Note`, these properties stored against a user are on a Confluence site level and not space/content level.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetUserProperties.PathParameters & Paths.GetUserProperties.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetUserProperties.Responses.$200>
  }
  ['/wiki/rest/api/user/{userId}/property/{key}']: {
    /**
     * getUserProperty - Get user property
     * 
     * Returns the property corresponding to `key` for a user. For more information
     * about user properties, see [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * `Note`, these properties stored against a user are on a Confluence site level and not space/content level.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'get'(
      parameters?: Parameters<Paths.GetUserProperty.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetUserProperty.Responses.$200>
    /**
     * updateUserProperty - Update user property
     * 
     * Updates a property for the given user. Note, you cannot update the key of a user property, only the value.
     * For more information about user properties, see
     * [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * `Note`, these properties stored against a user are on a Confluence site level and not space/content level.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'put'(
      parameters?: Parameters<Paths.UpdateUserProperty.PathParameters> | null,
      data?: Paths.UpdateUserProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.UpdateUserProperty.Responses.$204>
    /**
     * createUserProperty - Create user property by key
     * 
     * Creates a property for a user. For more information  about user properties, see [Confluence entity properties]
     * (https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * `Note`, these properties stored against a user are on a Confluence site level and not space/content level.
     * 
     * `Note:` the number of properties which could be created per app in a tenant for each user might be
     * restricted by fixed system limits.
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'post'(
      parameters?: Parameters<Paths.CreateUserProperty.PathParameters> | null,
      data?: Paths.CreateUserProperty.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.CreateUserProperty.Responses.$201>
    /**
     * deleteUserProperty - Delete user property
     * 
     * Deletes a property for the given user.
     * For more information about user properties, see
     * [Confluence entity properties](https://developer.atlassian.com/cloud/confluence/confluence-entity-properties/).
     * `Note`, these properties stored against a user are on a Confluence site level and not space/content level.
     * 
     * **[Permissions](https://confluence.atlassian.com/x/_AozKw) required**:
     * Permission to access the Confluence site ('Can use' global permission).
     */
    'delete'(
      parameters?: Parameters<Paths.DeleteUserProperty.PathParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.DeleteUserProperty.Responses.$204>
  }
}

export type Client = OpenAPIClient<OperationMethods, PathsDictionary>
